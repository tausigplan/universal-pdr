# encoding=utf-8
import linker
from mini_bmc import BMC
from z3 import *

from pattern.debug.profile import Stopwatch

def num_clauses(formula):
    if is_and(formula):
        return len(formula.children())
    elif formula == True or is_true(formula) or \
         formula == False or is_false(formula):
        return 0
    else:
        return 1

def num_univ_clauses(formula):
    if is_and(formula):
        fs = formula.children()
        n = 0
        for f in fs:
            if is_quantifier(f) and f.is_forall():
                n += 1
        return n
    elif is_quantifier(formula) and formula.is_forall():
        return 1
    else:
        return 0

def report_failure(pdr):
    #if pdr.counterexample:
    #    print '-' * 80
    #    display_structure(z3g.structure(pdr.counterexample), vocab)
    #    print '-' * 80
    #    return "invalid"
    #else:
    print 'Looking for concrete error trace...'
    bmc = BMC(pdr.Init, pdr.Rho, pdr.Bad, pdr.background, pdr.Locals)
    m = bmc.find_concrete_trace(pdr.N)
    if m:
        for m_i in m:
            print '-' * 80
            print m_i.get_universe(V)
            print "%s: %s" % (root, m_i.get_interp(root))
            for (c0, c) in locals:
                print "%s: %s" % (c, m_i.get_interp(c))
        print '-' * 80
        return "invalid"
    else:
        print "no concrete trace found (abstraction too coarse)"
        return "unknown"


if __name__ == '__main__':

    print "######################################################"
    import time
    print "Current time " + time.strftime("%c")
    print "Current file " + os.path.basename(__file__)
    print "######################################################"
    print

    import argparse

    a = argparse.ArgumentParser()
    #a.add_argument('filename', type=str, nargs='?')
    a.add_argument('-v', '--verbose', action='store_true')
    args = a.parse_args()
    
    ##############################################################

    V = DeclareSort('V')

    root      = Const('root', V)
    link      = Function('link', V, V, BoolSort())
    link0     = Function('link0', V, V, BoolSort())

    pending   = Function('pending', V, V, BoolSort())
    pending0  = Function('pending0', V, V, BoolSort())
    marked    = Function('marked', V, BoolSort())
    marked0   = Function('marked0', V, BoolSort())
    tree      = Function('tree', V, V, BoolSort())
    tree0     = Function('tree0', V, V, BoolSort())
    treestar  = Function('treestar', V, V, BoolSort())
    treestar0 = Function('treestar0', V, V, BoolSort())

    exit      = Const('exit', BoolSort())
    exit0     = Const('exit0', BoolSort())

    globals = [ root, link ]
    locals  = [ #(link0, link),
                (pending0, pending),
                (marked0, marked),
                (tree0, tree),
                (treestar0, treestar),
                (exit0, exit) ]

    preds = [ pending, marked, tree, treestar, link, exit ]
    
    u = Const('u', V)
    v = Const('v', V)
    w = Const('w', V)
    x = Const('x', V)

    background = \
        And([ ForAll([u], Not(link(u,u))),
              #ForAll([u], Not(link0(u,u))),
              ForAll([u,v], link(u,v) == link(v,u))
              #ForAll([u,v], link0(u,v) == link0(v,u))
        ])

    init = And([ ForAll([v],   marked(v) == (v == root)),
                 ForAll([u,v], pending(u,v) == And(u == root, link(root, v))),
                 ForAll([u,v], Not(tree(u,v))),
                 ForAll([u,v], treestar(u,v) == (u == v)),
                 exit == False ])

    cond  = Not(exit)
    cond0 = Not(exit0)
    #cond  = Exists([u,v], pending(u,v))
    #cond0 = Exists([u,v], pending0(u,v))

    a = Const('a', V)
    b = Const('b', V)
    trans = And([ pending0(a,b),
                  ForAll([u],   marked(u) == Or(marked0(u), u == b)),
                  ForAll([u,v], tree(u,v) == Or(tree0(u,v), And(u == a, v == b))),
                  ForAll([u,v], treestar(u,v) == Or(treestar0(u,v),
                                                    And(treestar0(u,a),
                                                        treestar0(b,v)))),
                  ForAll([u,v], pending(u,v) == Or(And([u == b, link(b,v), Not(marked(v))]),
                                                   And(pending0(u,v), v != b)))
                  #ForAll([u,v], link(u,v) == link0(u,v))
                ])

    rho = And(cond0, Exists([a,b], trans))

    #bad = And(Not(cond),
    #          Exists([u,v,w], And([ pending(u,v), treestar(w,u), treestar(w,v) ])))
    #bad = And(Not(cond),
    #          Exists([u,v], And([ treestar(root,u), Not(treestar(root,v)), link(u,v) ])))
    #bad = And(Not(cond),
    #          Exists([u,v,w,x],
    #              And([ treestar(u,v), treestar(u,w),
    #                    Not(treestar(v,w)), Not(treestar(w,v)), v != w, treestar(v,x), treestar(w,x) ])))
    spec = And([
                ForAll([u,v,w,x],
                    Not(And([ tree(u,v), tree(u,w), v != w, treestar(v,x), treestar(w,x) ]))),
                ForAll([u,v], Implies(tree(u,v), treestar(u,v))),
                ForAll([u,v,w], Implies(And(treestar(u,v), treestar(v,w)), treestar(u,w))),
                ForAll([u,v], Implies(And(treestar(u,v), treestar(v,u)), u == v))
            ])

    bad = And([ Not(cond), Not(spec) ])

    ##############################################################

    print "*** Using universal invariant inference"
    from mini_pdr import PDR

    print "* GLOBALS"
    print globals

    print "* LOCALS"
    print locals

    pdr = PDR(init, rho, bad, background, globals, locals, preds, universal=True)

    if args.verbose:
        pdr.verbose = True

    try:
        watch = Stopwatch()
        with watch:
            outcome = pdr.run()
        if outcome:
            status = "valid"
            if hasattr(pdr, 'verify') and not pdr.verify(outcome):
                print "incorrect!!"
                status = "incorrect"
        else:
            status = report_failure(pdr)

    except KeyboardInterrupt:
        outcome = None
        status = "timeout"
        
    fol_invariant = None
    if outcome:
        print "(%d clauses)" % num_clauses(outcome)
        print "(%d universal clauses)" % num_univ_clauses(outcome)

    print "PDR: %.2fs" % watch.clock.elapsed
    print "N =", pdr.N
    print pdr.iteration_count, "iterations"
    print pdr.sat_query_count, "calls to SAT"

