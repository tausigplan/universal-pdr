# encoding=utf-8
import linker
from mini_bmc import BMC
from z3 import *

from pattern.debug.profile import Stopwatch

def num_clauses(formula):
    if is_and(formula):
        return len(formula.children())
    elif formula == True or is_true(formula) or \
         formula == False or is_false(formula):
        return 0
    else:
        return 1

def num_univ_clauses(formula):
    if is_and(formula):
        fs = formula.children()
        n = 0
        for f in fs:
            if is_quantifier(f) and f.is_forall():
                n += 1
        return n
    elif is_quantifier(formula) and formula.is_forall():
        return 1
    else:
        return 0

def report_failure(pdr):
    #if pdr.counterexample:
    #    print '-' * 80
    #    display_structure(z3g.structure(pdr.counterexample), vocab)
    #    print '-' * 80
    #    return "invalid"
    #else:
    print 'Looking for concrete error trace...'
    bmc = BMC(pdr.Init, pdr.Rho, pdr.Bad, pdr.background, pdr.Locals)
    m = bmc.find_concrete_trace(pdr.N)
    if m:
        for m_i in m:
            print '-' * 80
            print m_i.get_universe(V)
            print "%s: %s" % (root, m_i.get_interp(root))
            for (c0, c) in locals:
                print "%s: %s" % (c, m_i.get_interp(c))
        print '-' * 80
        return "invalid"
    else:
        print "no concrete trace found (abstraction too coarse)"
        return "unknown"

def run_bmc(init, rho, bad, background, locals, depth):
    #if pdr.counterexample:
    #    print '-' * 80
    #    display_structure(z3g.structure(pdr.counterexample), vocab)
    #    print '-' * 80
    #    return "invalid"
    #else:
    print 'Looking for concrete error trace...'
    bmc = BMC(init, rho, bad, background, locals)
    m = bmc.find_concrete_trace(depth)
    if m:
        for m_i in m:
            print '-' * 80
            print m_i.get_universe(SW)
            print m_i.get_universe(PR)
            print m_i.get_universe(BUF)
            print m_i.get_universe(HO)
            print m_i.get_universe(PK)
            for c in [ port0, port1, switch0, switch1, src, dst, link ]:
                print "%s: %s" % (c, m_i.get_interp(c))
            for (c0, c) in locals:
                print "%s: %s" % (c, m_i.get_interp(c))
        print '-' * 80
        return "invalid"
    else:
        print "no concrete trace found (abstraction too coarse)"
        return "unknown"


if __name__ == '__main__':

    import argparse

    a = argparse.ArgumentParser()
    #a.add_argument('filename', type=str, nargs='?')
    a.add_argument('-v', '--verbose', action='store_true')
    args = a.parse_args()
    
    ##############################################################
    ##############       Learning switch       ###################
    ##############################################################

    #### Declarations
    SW = DeclareSort('SW')
    PR = DeclareSort('PR')
    HO = DeclareSort('HO')
    PK = DeclareSort('PK')
    BUF, (b_in, b_out) = EnumSort('BUF',('in','out'))

    Bool = BoolSort()
    #ttrue = BoolVal(True)
    #ffalse = BoolVal(False)

    #sw_port   = Function('switch_port', SW, PR, Bool)
    link      = Function('link', SW, PR, PR, SW, Bool)
    src       = Function('src', PK, HO)
    dst       = Function('dst', PK, HO)

    traffic   = Function('traffic', PK, SW, PR, BUF, SW, PR, BUF, Bool)
    traffic0  = Function('traffic0', PK, SW, PR, BUF, SW, PR, BUF, Bool)
    rt        = Function('rt', SW, HO, PR, Bool)
    rt0       = Function('rt0', SW, HO, PR, Bool)
    sent      = Function('sent', SW, PK, PR, PR, Bool)
    sent0     = Function('sent0', SW, PK, PR, PR, Bool)

    exit      = Const('exit', Bool)
    exit0     = Const('exit0', Bool)

    #### Axioms
    h = Const('h', HO)

    s = Const('s', SW)
    t = Const('t', SW)
    t1 = Const('t1', SW)
    t2 = Const('t2', SW)

    u = Const('u', PR)
    v = Const('v', PR)
    w = Const('w', PR)
    u_buf = Const('u_buf', BUF)
    v_buf = Const('v_buf', BUF)
    w_buf = Const('w_buf', BUF)

    port0 = Const('port0', PR)
    port1 = Const('port1', PR)
    #port2 = Const('port2', PR)

    switch0 = Const('switch0', SW)
    switch1 = Const('switch1', SW)
    #switch2 = Const('switch2', SW)

    p = Const('p', PK)

    axioms_switch = [
            #ForAll([s], Or([ s == switch0, s == switch1, s == switch2 ]))
            ForAll([s], Or([ s == switch0, s == switch1 ]))
        ]

    axioms_link = [
            ForAll([s,t,u,v], link(s,u,v,t) == link(t,v,u,s)),
            ForAll([s,t1,t2,u,v,w],
                Implies(And(link(s,u,v,t1), link(s,u,w,t2)),
                        And(t1 == t2, v == w))),
            ForAll([s,u,v], Not(link(s,u,v,s)))
        ] 

    axioms_port = [
            #ForAll([u], Or([ u == port0, u == port1, u == port2 ]))
            ForAll([u], Or([ u == port0, u == port1 ])),
            port0 != port1
        ]

    axioms_sent = [
            #ForAll([p], src(p) != dst(p))
            #BoolVal(True),
            ForAll([p,s,t,u,v,w],
                Implies(
                    And([ sent0(s,p,u,v), link(s,u,w,t) ]),
                    Or([
                        sent0(t, p, port0, w),
                        sent0(t, p, port1, w)
                    ])
                )
            ),
            ForAll([p,s,t,u,v,w],
                Implies(
                    And([ sent(s,p,u,v), link(s,u,w,t) ]),
                    Or([
                        sent(t, p, port0, w),
                        sent(t, p, port1, w)
                    ])
                )
            )
        ]

    assumption_topo0 = \
        lambda s,p,i: \
            And([
                ForAll([t,u],
                    Implies(link(s,i,u,t),
                        Or([
                            sent0(t,p,port0,u),
                            sent0(t,p,port1,u)
                        ])
                    )
                )
            ])

    ### Initial state
    init = And([
            ForAll([p,s,u,u_buf,t,v,v_buf],
                traffic(p, s, u, u_buf, t, v, v_buf) ==
                Or([ And([ s == t, u == v, u_buf == v_buf ]),
                     And([ link(s,u,v,t), u_buf == b_out, v_buf == b_in ])
                ])),
            ForAll([s,h,u], Not(rt(s,h,u))),
            ForAll([s,p,u,v], Not(sent(s,p,u,v))),
            Not(exit)
        ])

    #### Event parameters
    sw        = Const('sw', SW)
    sw0       = Const('sw0', SW)
    pkt       = Const('pkt', PK)
    pkt0      = Const('pkt0', PK)
    inport    = Const('inport', PR)
    inport0   = Const('inport0', PR)

    #### HACK: TODO: ask Nikolaj aboul model completion and sorts
    #s_hack    = Const('s_hack', SW)
    p_hack    = Const('p_hack', PK)
    h_hack    = Const('h_hack', HO)

    nonempty  = [ #Exists([s_hack], s_hack == s_hack),
                  Exists([p_hack, h_hack], src(p_hack) == h_hack) ]

    #### UPDR inputs
    #globals = [ port0, port1, port2, src, dst ]
    #globals = [ port0, port1, switch0, switch1, switch2, src, dst ]
    globals = [ port0, port1, switch0, switch1, src, dst ]
    locals  = [ (traffic0, traffic),
                (rt0, rt),
                (sent0, sent),
                (exit0, exit),
                (sw0, sw),
                (pkt0, pkt),
                (inport0, inport)
              ]

    preds = [ link, traffic, exit, sent, rt ]
    
    background = And(axioms_link + axioms_switch + axioms_port + axioms_sent + nonempty)

    cond  = Not(exit)
    cond0 = Not(exit0)

    trans = And([
        assumption_topo0(sw0, pkt0, inport0),
        # 1. The packet src is already learned.
        Implies(
            Exists([u], rt0(sw0, src(pkt0), u)),
            And([
                # Then rt and traffic are not changed
                ForAll([s,h,u], rt(s,h,u) == rt0(s,h,u)),
                ForAll([p,s,u,u_buf,t,v,v_buf],
                    traffic (p,s,u,u_buf,t,v,v_buf) ==
                    traffic0(p,s,u,u_buf,t,v,v_buf))
            ]) 
        ),
        # 2. The packet src is not learned yet
        Implies(
            Not(Exists([u], rt0(sw0, src(pkt0), u))),
            And([
                # Learn the source of the packet..
                ForAll([s,h,u],
                    rt(s,h,u) ==
                        Or(rt0(s,h,u),
                           And([ s == sw0, h == src(pkt0), u == inport0 ]))
                ),
                # ..and update the traffic relation
                ForAll([p, s, u, u_buf, t, v, v_buf],
                    traffic(p, s, u, u_buf, t, v, v_buf) ==
                    And([
                        Implies(
                            dst(p) == src(pkt0),
                            Or([
                                traffic0(p, s, u, u_buf, t, v, v_buf),
                                And([
                                    traffic0(p, s, u, u_buf, sw0, port0, b_in),
                                    traffic0(p, sw0, inport0, b_out, t, v, v_buf)
                                ]),
                                And([
                                    traffic0(p, s, u, u_buf, sw0, port1, b_in),
                                    traffic0(p, sw0, inport0, b_out, t, v, v_buf)
                                ])
                                #And([
                                #    traffic0(p, s, u, u_buf, sw0, port2, b_in),
                                #    traffic0(p, sw0, inport0, b_out, t, v, v_buf)
                                #])
                            ])
                        ),
                        Implies(dst(p) != src(pkt0), traffic0(p, s, u, u_buf, t, v, v_buf))
                    ])
                )
            ]) 
        ),
        And([
            # Regular forwarding
            Implies(
                Exists([u], rt(sw0, dst(pkt0), u)),
                ForAll([s,p,u,v,w],
                    Implies(
                        rt(sw0, dst(pkt0), w),
                        sent(s,p,u,v) ==
                            Or(sent0(s,p,u,v),
                               And([ s == sw0, p == pkt0, u == inport0, v == w ]))
                    )
                )
            ),
            # Flooding
            Implies(
                Not(Exists([u], rt(sw0, dst(pkt0), u))),
                ForAll([s,p,u,v],
                    sent(s,p,u,v) ==
                        Or(sent0(s,p,u,v),
                           And([ s == sw0, p == pkt0, u == inport0, v != inport0 ])))
            )
        ])
    ])

    rho = And(cond0, trans)

    #bad = And([
    #        Not(cond),
    #        Exists([p,u,v,v_buf],
    #            And([
    #                u != v,
    #                traffic(p, u, b_in, v, v_buf),
    #                traffic(p, v, v_buf, u, b_in)
    #            ])
    #        )
    #    ])
    bad = And([
            Not(cond),
            #Exists([p,s,t,u,v,v_buf],
            Exists([p,s,t,u,v],
                And([
                    s != t,
                    traffic(p, s, u, b_in, t, v, b_in),
                    traffic(p, t, v, b_in, s, u, b_in)
                ])
            )
        ])

    ##############################################################

    print "*** Using universal invariant inference"
    from mini_pdr import PDR

    print "* GLOBALS"
    print globals

    print "* LOCALS"
    print locals

    pdr = PDR(init, rho, bad, background, globals, locals, preds, universal=True, enumsorts=[BUF])

    if args.verbose:
        pdr.verbose = True

    if True:
        try:
            watch = Stopwatch()
            with watch:
                outcome = pdr.run()
            if outcome:
                status = "valid"
                if hasattr(pdr, 'verify') and not pdr.verify(outcome):
                    print "incorrect!!"
                    status = "incorrect"
            else:
                status = report_failure(pdr)
        except KeyboardInterrupt:
            outcome = None
            status = "timeout"
    else:
        depth = 3 
        status = run_bmc(init, rho, bad, background, locals, depth)

    fol_invariant = None
    if outcome:
        print "(%d clauses)" % num_clauses(outcome)
        print "(%d universal clauses)" % num_univ_clauses(outcome)

    print "PDR: %.2fs" % watch.clock.elapsed
    print "N =", pdr.N
    print pdr.iteration_count, "iterations"
    print pdr.sat_query_count, "calls to SAT"

