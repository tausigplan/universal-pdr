# encoding=utf-8
import linker
from mini_bmc import BMC
from z3 import *

from pattern.debug.profile import Stopwatch

def num_clauses(formula):
    if is_and(formula):
        return len(formula.children())
    elif formula == True or is_true(formula) or \
         formula == False or is_false(formula):
        return 0
    else:
        return 1

def num_univ_clauses(formula):
    if is_and(formula):
        fs = formula.children()
        n = 0
        for f in fs:
            if is_quantifier(f) and f.is_forall():
                n += 1
        return n
    elif is_quantifier(formula) and formula.is_forall():
        return 1
    else:
        return 0

def report_failure(pdr):
    #if pdr.counterexample:
    #    print '-' * 80
    #    display_structure(z3g.structure(pdr.counterexample), vocab)
    #    print '-' * 80
    #    return "invalid"
    #else:
    print 'Looking for concrete error trace...'
    bmc = BMC(pdr.Init, pdr.Rho, pdr.Bad, pdr.background, pdr.Locals)
    m = bmc.find_concrete_trace(pdr.N)
    if m:
        for m_i in m:
            print '-' * 80
            print m_i.get_universe(V)
            print "%s: %s" % (root, m_i.get_interp(root))
            for (c0, c) in locals:
                print "%s: %s" % (c, m_i.get_interp(c))
        print '-' * 80
        return "invalid"
    else:
        print "no concrete trace found (abstraction too coarse)"
        return "unknown"


if __name__ == '__main__':

    print "######################################################"
    import time
    print "Current time " + time.strftime("%c")
    print "Current file " + os.path.basename(__file__)
    print "######################################################"
    print

    import argparse

    a = argparse.ArgumentParser()
    #a.add_argument('filename', type=str, nargs='?')
    a.add_argument('-v', '--verbose', action='store_true')
    args = a.parse_args()
    
    ##############################################################

    Bool = BoolSort()

    SW = DeclareSort('SW')
    HO = DeclareSort('HO')
    LOC, (loc1, loc2) = EnumSort('LOC', ('loc1','loc2'))

    root = Const('root', SW)

    link      = Function('link', SW, SW, Bool)

    pending   = Function('pending', HO, HO, SW, SW, Bool)
    pending0  = Function('pending0', HO, HO, SW, SW, Bool)
    marked    = Function('marked', HO, SW, Bool)
    marked0   = Function('marked0', HO, SW, Bool)
    routing   = Function('routing', HO, SW, SW, Bool)
    routing0  = Function('routing0', HO, SW, SW, Bool)
    routingstar  = Function('routingstar', HO, SW, SW, Bool)
    routingstar0 = Function('routingstar0', HO, SW, SW, Bool)

    #is_client = Function('is_client', SW, Bool)
    #connected = Function('connected', HO, SW, Bool)

    exit      = Const('exit', BoolSort())
    exit0     = Const('exit0', BoolSort())
    event     = Const('event', LOC)
    event0    = Const('event0', LOC)
    case      = Const('case', LOC)
    case0     = Const('case0', LOC)

    #one_src  = Const('one_src', HO)
    #one_dst  = Const('one_dst', HO)

    #globals = [ root, link, is_client, connected ]
    #globals = [ root, link, one_src, one_dst ]
    globals = [ root, link ]

    sw1  = Const('sw1', SW)
    sw10 = Const('sw10', SW)
    sw2  = Const('sw2', SW)
    sw20 = Const('sw20', SW)
    sw3  = Const('sw3', SW)
    sw30 = Const('sw30', SW)
    src  = Const('src', HO)
    src0 = Const('src0', HO)
    dst  = Const('dst', HO)
    dst0 = Const('dst0', HO)

    locals  = [ (pending0, pending),
                (marked0, marked),
                (routing0, routing),
                (routingstar0, routingstar),
                (event0, event),
                (case0, case),
                (sw10, sw1),
                (sw20, sw2),
                (sw30, sw3),
                (src0, src),
                (dst0, dst),
                (exit0, exit)
              ]

    preds = [ pending, marked, routing, routingstar, link, exit ]
    #preds = [ pending, marked, routing, routingstar, link, is_client, connected, exit ]
    
    g = Const('g', HO)
    h = Const('h', HO)
    s = Const('s', SW)
    t = Const('t', SW)
    u = Const('u', SW)
    v = Const('v', SW)
    w = Const('w', SW)
    x = Const('x', SW)

    axioms_link = [
            ForAll([u], Not(link(u,u))),
            ForAll([u,v], link(u,v) == link(v,u)),
            ForAll([u], Not(link(root, u)))
        ]

    axioms_host = [
            #ForAll([u,v],   Implies(And(is_client(u), is_client(v)), Not(link(u,v)))),
            #ForAll([s,u,v],
            #    Implies(And([ is_client(s), link(s,u), link(s,v) ]), u == v)),
            #ForAll([h,u],   Implies(connected(h,u), is_client(u))),
            #ForAll([h,u,v], Implies(And(connected(h,u), connected(h,v)), u == v))
        ]

    background = And(axioms_link + axioms_host)

    init = And([ ForAll([h,u,v], Not(routing(h,u,v))),
                 ForAll([h,u], Not(marked(h,u))),
                 ForAll([h,u,v], routingstar(h,u,v) == And([u == v])),
                 ForAll([g,h,u,v], Not(pending(g,h,u,v))),
                 Not(exit)
           ])

    cond  = Not(exit)
    cond0 = Not(exit0)

    trans_pgen = And([
        sw20 != root,
        src0 != dst0,
        ForAll([g,h,u,v],
            pending(g,h,u,v) == Or(
                pending0(g,h,u,v),
                And([ g == src0, h == dst0, u == root, v == sw20 ])
                #And([ g == one_src, h == one_dst, u == root, v == sw20 ])
                #And([ g == src0, h == dst0, u == sw10, v == sw20,
                #      src0 != dst0,
                #      connected(g,sw10), is_client(sw10), link(sw10, sw20) ])
            )
        ),
        ForAll([g,u,v], routing(g,u,v) == routing0(g,u,v)),
        ForAll([h,u,v], routingstar(h,u,v) == routingstar0(h,u,v)),
        ForAll([h,u], marked(h,u) == marked0(h,u))
    ])

    trans_ph = And([
        pending0(src0, dst0, sw10, sw20),
        Or([
            And([
                Not(marked0(src0, sw20)),
                #ForAll([u], Not(routing0(src0, sw20, u))),
                ForAll([h,u],
                    marked(h,u) == Or(
                      marked0(h,u),
                      And([ h == src0, u == sw20 ])
                    )
                ),
                ForAll([h,u,v],
                    routing(h,u,v) == Or(
                      routing0(h,u,v),
                      And([ h == src0, u == sw20, v == sw10 ])
                    )
                ),
                ForAll([h,u,v],
                    routingstar(h,u,v) == Or(
                        routingstar0(h,u,v),
                        And([ h == src0,
                              routingstar0(h, u, sw20),
                              routingstar0(h, sw10, v)
                        ])
                    )
                )
            ]),
            And([
                marked0(src0, sw20),
                ForAll([h,u], marked(h,u) == marked0(h,u)),
                ForAll([h,u,v], routing(h,u,v) == routing0(h,u,v)),
                ForAll([h,u,v], routingstar(h,u,v) == routingstar0(h,u,v))
            ])
        ]),
        Or([
            # 20, assume_('[[routing(dst0,sw20,sw30)]]'),
            And([
                #Or( routing0(dst0,sw20,sw30),
                #    And([ dst0 == src0, sw20 == sw20, sw30 == sw10 ])
                #),
                case0 == loc1,
                marked(dst0,sw20),
                routing(dst0,sw20,sw30),
                # 6, remove_('pending(src0,dst0,sw10,sw20)'),
                # 21, insert_('pending(src0,dst0,sw20,sw30)'),
                ForAll([g,h,u,v],
                    pending(g,h,u,v) == Or(
                        And( pending0(g,h,u,v), 
                             Not(And([ g == src0, h == dst0, u == sw10, v == sw20 ]))
                        )
                        #And([ g == src0, h == dst0, u == sw20, v == sw30,
                        #      link(sw20, sw30),
                        #      sw30 != root ])
                    )
                )
            ]),
            # 20, assume_('[[~routing(dst0,sw20,V)]]'), 30,
            And([
                case0 == loc2,
                Not(marked(dst0,sw20)),
                ForAll([v], 
                     Not(routing(dst0,sw20,v)),
                     #Not(Or( routing0(dst0,sw20,v),
                     #        And([ dst0 == src0, sw20 == sw20, v == sw10 ])
                     #      )
                     #)
                ),
                # 6, remove_('pending(src0,dst0,sw10,sw20)'),
                # 30, insert_('pending(src0,dst0,sw20,V)', '[link(sw20,V), ~=(V,sw10)]'),
                ForAll([g,h,u,v],
                    pending(g,h,u,v) == Or([
                        And( pending0(g,h,u,v), 
                             Not(And([ g == src0, h == dst0, u == sw10, v == sw20 ]))
                        ),
                        #And([ g == src0, h == dst0, u == sw20, link(sw20, v), v != sw10 ])
                        And([ g == src0, h == dst0, u == sw20, link(sw20,v), v != sw10,
                              v != root ])
                    ])
                )
            ])
        ])
    ])

    trans = Or(And(event0 == loc1, trans_pgen), And(event0 == loc2, trans_ph))

    rho = And(cond0, trans)

    #bad = And(Not(cond),
    #          Exists([u,v,w], And([ pending(u,v), routingstar(w,u), routingstar(w,v) ])))
    #bad = And(Not(cond),
    #          Exists([u,v], And([ routingstar(root,u), Not(routingstar(root,v)), link(u,v) ])))
    #bad = And(Not(cond),
    #          Exists([u,v,w,x], And([ routingstar(u,v), routingstar(u,w), Not(routingstar(v,w)), Not(routingstar(w,v)), v != w, routingstar(v,x), routingstar(w,x) ])))

    spec = And([
                #ForAll([h,u], Implies(marked(h,u), h == one_src)),
                #ForAll([h,u,v], Implies(routing(h,u,v), h == one_src)),

                ForAll([h,u,v,w],
                    Implies(And(routing(h,u,v), routing(h,u,w)), v == w)
                ),

                ForAll([h,u,v], Implies(routing(h,u,v), routingstar(h,u,v))),
                ForAll([h,u,v,w],
                    Implies(And(routingstar(h,u,v), routingstar(h,v,w)), routingstar(h,u,w))
                ),
                ForAll([h,u,v],
                    Implies(And(routingstar(h,u,v), routingstar(h,v,u)), u == v)
                ),
                ForAll([h,u,v,w,x],
                    Not(And([ routing(h,v,u), routing(h,w,u), v != w,
                        routingstar(h,x,v), routingstar(h,x,w) ]))),
                ForAll([h,u,v,w],
                    Implies(And(routingstar(h,u,v), routingstar(h,u,w)),
                            Or(routingstar(h,v,w), routingstar(h,w,v)))
                ),

                # extra stuff
                ForAll([h,u,v], Not(pending(h,h,u,v))),
                ForAll([g,h,u], Not(pending(g,h,u,root))),
                #ForAll([h,u,v,w],
                #    Implies(And(routingstar(h,u,v), routing(h,v,w)),
                #            routingstar(h,u,w))
                #),
                ForAll([h,u,v],
                    Implies(And(routingstar(h,u,v), u != v), marked(h,u))
                )
                #ForAll([g,h,u,v],
                #    Implies(And(pending(g,h,u,v), marked(g,u)),
                #            link(u,v))
                #)
                #ForAll([h,u,v],
                #    Not(And(routingstar(h,u,v), pending(h,h,u,v)))
                #)

           ])

    bad = And([ Not(cond),
                Not(spec) ])

    ##############################################################

    print "*** Using universal invariant inference"
    from mini_pdr import PDR

    print "* GLOBALS"
    print globals

    print "* LOCALS"
    print locals

    pdr = PDR(init, rho, bad, background, globals, locals, preds,
            universal=True, enumsorts=[LOC], gen_enums=False)

    if args.verbose:
        pdr.verbose = True

    try:
        watch = Stopwatch()
        with watch:
            outcome = pdr.run()
        if outcome:
            status = "valid"
            if hasattr(pdr, 'verify') and not pdr.verify(outcome):
                print "incorrect!!"
                status = "incorrect"
        else:
            status = report_failure(pdr)

    except KeyboardInterrupt:
        outcome = None
        status = "timeout"
        
    fol_invariant = None
    if outcome:
        print "(%d clauses)" % num_clauses(outcome)
        print "(%d universal clauses)" % num_univ_clauses(outcome)

    print "PDR: %.2fs" % watch.clock.elapsed
    print "N =", pdr.N
    print pdr.iteration_count, "iterations"
    print pdr.sat_query_count, "calls to SAT"

