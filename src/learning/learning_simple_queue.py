# encoding=utf-8
import linker
from mini_bmc import BMC
from z3 import *

from pattern.debug.profile import Stopwatch

def num_clauses(formula):
    if is_and(formula):
        return len(formula.children())
    elif formula == True or is_true(formula) or \
         formula == False or is_false(formula):
        return 0
    else:
        return 1

def num_univ_clauses(formula):
    if is_and(formula):
        fs = formula.children()
        n = 0
        for f in fs:
            if is_quantifier(f) and f.is_forall():
                n += 1
        return n
    elif is_quantifier(formula) and formula.is_forall():
        return 1
    else:
        return 0

def report_failure(pdr):
    #if pdr.counterexample:
    #    print '-' * 80
    #    display_structure(z3g.structure(pdr.counterexample), vocab)
    #    print '-' * 80
    #    return "invalid"
    #else:
    print 'Looking for concrete error trace...'
    bmc = BMC(pdr.Init, pdr.Rho, pdr.Bad, pdr.background, pdr.Locals)
    m = bmc.find_concrete_trace(pdr.N)
    if m:
        for m_i in m:
            print '-' * 80
            print m_i.get_universe(V)
            print "%s: %s" % (root, m_i.get_interp(root))
            for (c0, c) in locals:
                print "%s: %s" % (c, m_i.get_interp(c))
        print '-' * 80
        return "invalid"
    else:
        print "no concrete trace found (abstraction too coarse)"
        return "unknown"


if __name__ == '__main__':

    import argparse

    a = argparse.ArgumentParser()
    #a.add_argument('filename', type=str, nargs='?')
    a.add_argument('-v', '--verbose', action='store_true')
    args = a.parse_args()
    
    ##############################################################
    ##############       Learning switch       ###################
    ##############################################################

    #### Declarations
    SW = DeclareSort('SW')

    num_ports = 3
    #PR = DeclareSort('PR')
    PR, ports = EnumSort('PR', [ 'port%d' % d for d in range(num_ports) ])

    HO = DeclareSort('HO')
    PK = DeclareSort('PK')
    BUF, (b_in, b_out) = EnumSort('BUF',('in','out'))

    Bool = BoolSort()
    #ttrue = BoolVal(True)
    #ffalse = BoolVal(False)

    #sw_port   = Function('switch_port', SW, PR, Bool)
    link      = Function('link', SW, PR, PR, SW, Bool)
    #src       = Function('src', PK, HO)
    #dst       = Function('dst', PK, HO)

    traffic   = Function('traffic', PK, SW, PR, BUF, SW, PR, BUF, Bool)
    traffic0  = Function('traffic0', PK, SW, PR, BUF, SW, PR, BUF, Bool)
    #traffic   = Function('traffic', PK, PR, BUF, PR, BUF, Bool)
    #traffic0  = Function('traffic0', PK, PR, BUF, PR, BUF, Bool)
    #rt        = Function('rt', SW, HO, PR, Bool)
    #rt0       = Function('rt0', SW, HO, PR, Bool)
    fwd       = Function('fwd', PK, SW, PR, Bool)
    fwd0      = Function('fwd0', PK, SW, PR, Bool)
    queue     = Function('queue', PK, SW, PR, Bool)
    queue0    = Function('queue0', PK, SW, PR, Bool)

    exit      = Const('exit', Bool)
    exit0     = Const('exit0', Bool)
    
    PC, (cmd1, cmd2) = EnumSort('PC',('cmd1','cmd2'))
    pc        = Const('pc', PC)

    #### Axioms
    #h = Const('h', HO)

    s = Const('s', SW)
    t = Const('t', SW)
    t1 = Const('t1', SW)
    t2 = Const('t2', SW)

    u = Const('u', PR)
    v = Const('v', PR)
    w = Const('w', PR)
    u_buf = Const('u_buf', BUF)
    v_buf = Const('v_buf', BUF)
    w_buf = Const('w_buf', BUF)

    p = Const('p', PK)

    #port0 = Const('port0', PR)
    #port1 = Const('port1', PR)
    #port2 = Const('port2', PR)

    switch0 = Const('switch0', SW)
    switch1 = Const('switch1', SW)
    #switch2 = Const('switch2', SW)

    axioms_switch = [
            #ForAll([s,t,u], Implies(And(sw_port(s,u), sw_port(t,u)), s == t)),
            #ForAll([s], Or([ s == switch0, s == switch1 ]))
            #ForAll([s], Or([ s == switch0, s == switch1, s == switch2 ]))
        ]

    axioms_link = [
            ForAll([s,u,v], Not(link(s,u,v,s))),
            ForAll([s,t,u,v], link(s,u,v,t) == link(t,v,u,s)),
            ForAll([s,t1,t2,u,v,w],
                Implies(And(link(s,u,v,t1), link(s,u,w,t2)), And(t1 == t2, v == w)))
            #ForAll([u,v,s],
            #    Implies(And(sw_port(s,u), sw_port(s,v)), Not(link(u,v))))
        ] 

    axioms_port = [
            #ForAll([u], Or([ u == port0, u == port1, u == port2 ]))
            #ForAll([u], Or([ u == port0, u == port1 ]))
        ]

    axioms_packet = [
            #ForAll([p], src(p) != dst(p))
        ]

    assumption_topo0 = \
        lambda p_cur,s_cur,i_cur: \
            And([
                #sw_port(s,i),
                #ForAll([t,u],
                #    Implies(link(t,u,i_cur,s_cur), fwd0(p_cur,t,u)))
                    #Implies(And(link(t,u,i_cur,s_cur), sw_port(t,u)), fwd0(p,t,u)))
                queue0(p_cur,s_cur,i_cur)
            ])

    assumptions_traffic = \
        [
            ForAll([p,s,u,u_buf],
                traffic(p, s, u, u_buf, s, u, u_buf)),

            ForAll([p,s,t,t1,u,u_buf,v,v_buf,w,w_buf],
                Implies(
                    And(traffic(p, s, u, u_buf, t1, v, v_buf),
                        traffic(p, t1, v, v_buf, t2, w, w_buf)),
                    traffic(p, s, u, u_buf, t2, w, w_buf))
            ),

            ForAll([p,s,t,t1,u,u_buf,v,v_buf,w,w_buf],
                Implies(
                    And(traffic(p, s, u, u_buf, t1, v, v_buf),
                        traffic(p, s, u, u_buf, t2, w, w_buf)),
                    Or(traffic(p, t1, v, v_buf, t2, w, w_buf),
                       traffic(p, t2, w, w_buf, t1, v, v_buf)))
            ),

            ForAll([p,s,u,u_buf],
                traffic0(p, s, u, u_buf, s, u, u_buf)),

            ForAll([p,s,t,t1,u,u_buf,v,v_buf,w,w_buf],
                Implies(
                    And(traffic0(p, s, u, u_buf, t1, v, v_buf),
                        traffic0(p, t1, v, v_buf, t2, w, w_buf)),
                    traffic0(p, s, u, u_buf, t2, w, w_buf))
            ),

            ForAll([p,s,t,t1,u,u_buf,v,v_buf,w,w_buf],
                Implies(
                    And(traffic0(p, s, u, u_buf, t1, v, v_buf),
                        traffic0(p, s, u, u_buf, t2, w, w_buf)),
                    Or(traffic0(p, t1, v, v_buf, t2, w, w_buf),
                       traffic0(p, t2, w, w_buf, t1, v, v_buf)))
            )
        ]

    ### Initial state
    init = And([
            ForAll([p,s,t,u,u_buf,v,v_buf],
                traffic(p, s, u, u_buf, t, v, v_buf) ==
                Or([ And([ s == t, u == v, u_buf == v_buf ]),
                     And([ link(s,u,v,t), u_buf == b_out, v_buf == b_in ])
                ])
            ),
            #ForAll([s,h,u], Not(rt(s,h,u))),
            #ForAll([p,s,u], Not(fwd(p,s,u))),
            ForAll([p,s,u], Not(queue(p,s,u))),
            Not(exit)
        ])

    #### Event parameters
    i_sw      = Const('i_sw', SW)
    i_pkt     = Const('i_pkt', PK)
    i_inport  = Const('i_inport', PR)
    #sw        = Const('sw', SW)
    #sw0       = Const('sw0', SW)
    #pkt       = Const('pkt', PK)
    #pkt0      = Const('pkt0', PK)
    #inport    = Const('inport', PR)
    #inport0   = Const('inport0', PR)

    #### HACK: TODO: ask Nikolaj aboul model completion and sorts
    #s_hack    = Const('s_hack', SW)
    p_hack    = Const('p_hack', PK)
    h_hack    = Const('h_hack', HO)

    #nonempty  = [ Exists([p_hack, h_hack], src(p_hack) == h_hack) ]
    nonempty   = []

    #### UPDR inputs
    globals = []
    #globals = [ src, dst ]
    globals += [ switch0, switch1 ]
    locals  = [ (traffic0, traffic),
                #(rt0, rt),
                #(fwd0, fwd),
                (queue0, queue),
                (exit0, exit)
                #(sw0, sw),
                #(pkt0, pkt),
                #(inport0, inport)
              ]

    #preds = [ sw_port, link, traffic, exit, fwd, rt ]
    #preds = [ link, traffic, fwd, queue, exit ]
    preds = [ link, traffic, queue, exit ]
    
    background = And(axioms_link + axioms_switch + axioms_port + axioms_packet + nonempty) #+ assumptions_traffic)

    cond  = Not(exit)
    cond0 = Not(exit0)

    trans_gen_pkt = And([
                ForAll([p,s,t,u,u_buf,v,v_buf],
                    traffic (p,s,u,u_buf,t,v,v_buf) ==
                    traffic0(p,s,u,u_buf,t,v,v_buf)),
                ForAll([p,s,u,t,v],
                    And([
                        Not(link(i_sw, i_inport, v, t)),
                        queue (p,s,u) ==
                        Or(
                            queue0(p,s,u),
                            And([ p == i_pkt, s == i_sw, u == i_inport ])
                        )
                    ])
                )
            ])

    trans_pp = And([
        assumption_topo0(i_pkt, i_sw, i_inport),
        # 0. Assume that ft entry is not installed for the i_pkt
        # TODO: use immediate succesor
        ForAll([u], Not(traffic0(i_pkt, i_sw, i_inport, b_in, i_sw, u, b_out))),
        # 1. The packet i_pkt is already learned on i_sw on some port (maybe different from i_inport)
        Implies(
            Exists([u,v], traffic0(i_pkt, i_sw, u, b_in, i_sw, v, b_out)),
            # ... then we do nothing
            And([
                # .. which means that traffic is not changed
                #ForAll([s,h,u], rt(s,h,u) == rt0(s,h,u)),
                ForAll([p,s,t,u,u_buf,v,v_buf],
                    traffic (p,s,u,u_buf,t,v,v_buf) ==
                    traffic0(p,s,u,u_buf,t,v,v_buf))
                #ForAll([p,s,u],
                #    fwd(p,s,u) == fwd0(p,s,u))
            ]) 
        ),
        # 2. The packet i_pkt is not yet learned on i_sw
        Implies(
            Not(Exists([u,v], traffic0(i_pkt, i_sw, u, b_in, i_sw, v, b_out))),
            And([
                # Learn the source of the packet..
                #ForAll([s,h,u],
                #    rt(s,h,u) ==
                #        Or(rt0(s,h,u),
                #           And([ s == i_sw, h == src(i_pkt), u == i_inport ]))
                #),
                # ..and update the traffic relation
                ForAll([p, s, u, u_buf, t, v, v_buf],
                    traffic(p, s, u, u_buf, t, v, v_buf) ==
                    And([
                        Or([
                            traffic0(p, s, u, u_buf, t, v, v_buf),
                            And(
                                p == i_pkt,
                                Or([
                                    And([
                                        #w != i_inport,
                                        #sw_port(i_sw, w),
                                        traffic0(p, s, u, u_buf, i_sw, w, b_in),
                                        traffic0(p, i_sw, i_inport, b_out, t, v, v_buf)
                                    ])
                                    for w in ports
                                ])
                            )
                        ])
                        #Implies(dst(p) != src(i_pkt), traffic0(p, u, u_buf, v, v_buf))
                        #Implies(p != i_pkt, traffic0(p, s, u, u_buf, t, v, v_buf))
                    ])
                )
            ]) 
        ),
        And([
            # Flooding
            ForAll([p,s,u],
                queue(p,s,u) ==
                    Or(
                        And(queue0(p,s,u),
                            Not(And([ p == i_pkt, s == i_sw, u == i_inport ]))
                        ),
                        #And([ s == i_sw, p == i_pkt, u != i_inport ])
                        And([ Exists(v, link(i_sw, v, u, s)), p == i_pkt ])
                    )
            )
            #ForAll([p,s,u],
            #    fwd(p,s,u) ==
            #        Or(fwd0(p,s,u),
            #           #And([ s == i_sw, p == i_pkt, u != i_inport ])
            #           And([ s == i_sw, p == i_pkt ])
            #        )
            #)
        ])
    ])

    trans = Or([ And(pc == cmd1, trans_gen_pkt), And(pc == cmd2, trans_pp) ])

    rho = And(cond0, trans)

    bad = And([
            Not(cond),
            Exists([s,t,p,u,v,v_buf],
                And([
                    s != t,
                    #u != v,
                    #sw_port(s,u),
                    #sw_port(t,v),
                    traffic(p, s, u, b_in, t, v, v_buf),
                    traffic(p, t, v, v_buf, s, u, b_in)
                ])
            )
        ])

    ##############################################################

    print "*** Using universal invariant inference"
    from mini_pdr import PDR

    print "* GLOBALS"
    print globals

    print "* LOCALS"
    print locals

    pdr = PDR(init, rho, bad, background, globals, locals, preds,
            universal=True, enumsorts=[BUF, PR], gen_enums=True)

    if args.verbose:
        pdr.verbose = True

    try:
        watch = Stopwatch()
        with watch:
            outcome = pdr.run()
        if outcome:
            status = "valid"
            if hasattr(pdr, 'verify') and not pdr.verify(outcome):
                print "incorrect!!"
                status = "incorrect"
        else:
            status = report_failure(pdr)

    except KeyboardInterrupt:
        outcome = None
        status = "timeout"
        
    fol_invariant = None
    if outcome:
        print "(%d clauses)" % num_clauses(outcome)
        print "(%d universal clauses)" % num_univ_clauses(outcome)

    print "PDR: %.2fs" % watch.clock.elapsed
    print "N =", pdr.N
    print pdr.iteration_count, "iterations"
    print pdr.sat_query_count, "calls to SAT"

