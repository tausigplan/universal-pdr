# encoding=utf-8
import linker
from mini_bmc import BMC
from z3 import *

from pattern.debug.profile import Stopwatch

def num_clauses(formula):
    if is_and(formula):
        return len(formula.children())
    elif formula == True or is_true(formula) or \
         formula == False or is_false(formula):
        return 0
    else:
        return 1

def num_univ_clauses(formula):
    if is_and(formula):
        fs = formula.children()
        n = 0
        for f in fs:
            if is_quantifier(f) and f.is_forall():
                n += 1
        return n
    elif is_quantifier(formula) and formula.is_forall():
        return 1
    else:
        return 0

def report_failure(pdr):
    #if pdr.counterexample:
    #    print '-' * 80
    #    display_structure(z3g.structure(pdr.counterexample), vocab)
    #    print '-' * 80
    #    return "invalid"
    #else:
    print 'Looking for concrete error trace...'
    bmc = BMC(pdr.Init, pdr.Rho, pdr.Bad, pdr.background, pdr.Locals)
    m = bmc.find_concrete_trace(pdr.N)
    if m:
        for m_i in m:
            print '-' * 80
            print m_i.get_universe(V)
            print "%s: %s" % (root, m_i.get_interp(root))
            for (c0, c) in locals:
                print "%s: %s" % (c, m_i.get_interp(c))
        print '-' * 80
        return "invalid"
    else:
        print "no concrete trace found (abstraction too coarse)"
        return "unknown"


if __name__ == '__main__':

    import argparse

    a = argparse.ArgumentParser()
    #a.add_argument('filename', type=str, nargs='?')
    a.add_argument('-v', '--verbose', action='store_true')
    args = a.parse_args()
    
    ##############################################################

    Bool = BoolSort()

    SW = DeclareSort('SW')
    HO = DeclareSort('HO')
    LOC, (loc1, loc2) = EnumSort('LOC', ('loc1','loc2'))

    #origin_sw = Const('origin', SW)

    link      = Function('link', SW, SW, Bool)

    pending   = Function('pending', HO, HO, SW, SW, Bool)
    pending0  = Function('pending0', HO, HO, SW, SW, Bool)
    routing   = Function('routing', HO, SW, SW, Bool)
    routing0  = Function('routing0', HO, SW, SW, Bool)
    routingstar  = Function('routingstar', HO, SW, SW, Bool)
    routingstar0 = Function('routingstar0', HO, SW, SW, Bool)

    exit      = Const('exit', BoolSort())
    exit0     = Const('exit0', BoolSort())
    pc        = Const('pc', LOC)
    #pc0       = Const('pc0', LOC)

    #globals = [ origin_sw, link ]
    globals = [ link ]
    locals  = [ (pending0, pending),
                (routing0, routing),
                (routingstar0, routingstar),
                #(pc0, pc),
                (exit0, exit)
              ]

    preds = [ pending, routing, routingstar, link, exit ]
    
    g = Const('g', HO)
    h = Const('h', HO)
    s = Const('s', SW)
    t = Const('t', SW)
    u = Const('u', SW)
    v = Const('v', SW)
    src = Const('src', HO)
    dst = Const('dst', HO)

    background = \
        And(ForAll([u], Not(link(u,u))),
            ForAll([u,v], link(u,v) == link(v,u))
        )

    init = And([ ForAll([h,u,v], Not(routing(h,u,v))),
                 ForAll([h,u,v], routingstar(h,u,v) == And([u == v])),
                 ForAll([g,h,u,v], Not(pending(g,h,u,v))),
                 Not(exit) ])

    cond  = Not(exit)
    cond0 = Not(exit0)

    sw1 = Const('sw1', SW)
    #sw10 = Const('sw10', SW)
    sw2 = Const('sw2', SW)
    #sw20 = Const('sw20', SW)
    sw3 = Const('sw3', SW)

    trans_pgen = And([
        ForAll([g,h,u,v],
            pending(g,h,u,v) == Or(
                pending0(g,h,u,v),
                And([ g == src, h == dst, u == sw1, v == sw1 ])
                #And([ g == src, h == dst, u == origin_sw, v == sw1,
                #      v != origin_sw ])
            )
        ),
        ForAll([g,u,v], routing(g,u,v) == routing0(g,u,v)),
        ForAll([h,u,v], routingstar(h,u,v) == routingstar0(h,u,v))
    ])

    trans_ph = And([
        pending0(src, dst, sw1, sw2),
        And([
            ForAll([u], Not(routing0(src, sw2, u))),
            ForAll([h,u,v],
                routing(h,u,v) == Or(
                  routing0(h,u,v),
                  And([ h == src, u == sw2, v == sw1 ])
                )
            ),
            ForAll([h,u,v],
                routingstar(h,u,v) == Or(
                    routingstar0(h,u,v),
                    And([ h == src,
                          routingstar0 (h, u, sw2),
                          routingstar0 (h, sw1, v)
                    ])
                )
            )
        ]),
        Or([
            # 20, assume_('[[routing(dst,sw2,sw3)]]'),
            And([
                #Or( routing0(dst,sw2,sw3),
                #    And([ dst == src, sw2 == sw2, sw3 == sw1 ])
                #),
                routing(dst,sw2,sw3),
                # 6, remove_('pending(src,dst,sw1,sw2)'),
                # 21, insert_('pending(src,dst,sw2,sw3)'),
                ForAll([g,h,u,v],
                    pending(g,h,u,v) == Or(
                        And( pending0(g,h,u,v), 
                             Not(And([ g == src, h == dst, u == sw1, v == sw2 ]))
                        ),
                        And([ g == src, h == dst, u == sw2, v == sw3 ])
                        #And([ g == src, h == dst, u == sw2, v == sw3,
                        #      sw3 != origin_sw ])
                    )
                )
            ]),
            # 20, assume_('[[~routing(dst,sw2,V)]]'), 30,
            And([
                ForAll([v], 
                     Not(routing(dst,sw2,v))
                     #Not(Or( routing0(dst,sw2,v),
                     #        And([ dst == src, sw2 == sw2, v == sw1 ])
                     #      )
                     #)
                ),
                # 6, remove_('pending(src,dst,sw1,sw2)'),
                # 30, insert_('pending(src,dst,sw2,V)', '[link(sw2,V), ~=(V,sw1)]'),
                ForAll([g,h,u,v],
                    pending(g,h,u,v) == Or([
                        And( pending0(g,h,u,v), 
                             Not(And([ g == src, h == dst, u == sw1, v == sw2 ]))
                        ),
                        And([ g == src, h == dst, u == sw2, link(sw2, v), v != sw1 ])
                        #And([ g == src, h == dst, u == sw2, link(sw2,v), v != sw1,
                        #      v != origin_sw ])
                    ])
                )
            ])
        ])
    ])

    trans = Or(And(pc == loc1, trans_pgen), And(pc == loc2, trans_ph))

    rho = And(cond0, trans)

    #bad = And(Not(cond),
    #          Exists([u,v,w], And([ pending(u,v), routingstar(w,u), routingstar(w,v) ])))
    #bad = And(Not(cond),
    #          Exists([u,v], And([ routingstar(root,u), Not(routingstar(root,v)), link(u,v) ])))
    #bad = And(Not(cond),
    #          Exists([u,v,w,x], And([ routingstar(u,v), routingstar(u,w), Not(routingstar(v,w)), Not(routingstar(w,v)), v != w, routingstar(v,x), routingstar(w,x) ])))
    bad = And([ Not(cond),
                Exists([h,u,v],
                    And([ u != v, #u != origin_sw, v != origin_sw,
                          routingstar(h,u,v),
                          routingstar(h,v,u) ]))
              ])

    ##############################################################

    print "*** Using universal invariant inference"
    from mini_pdr import PDR

    print "* GLOBALS"
    print globals

    print "* LOCALS"
    print locals

    pdr = PDR(init, rho, bad, background, globals, locals, preds,
            universal=True, enumsorts=[LOC], gen_enums=False)

    if args.verbose:
        pdr.verbose = True

    try:
        watch = Stopwatch()
        with watch:
            outcome = pdr.run()
        if outcome:
            status = "valid"
            if hasattr(pdr, 'verify') and not pdr.verify(outcome):
                print "incorrect!!"
                status = "incorrect"
        else:
            status = report_failure(pdr)

    except KeyboardInterrupt:
        outcome = None
        status = "timeout"
        
    fol_invariant = None
    if outcome:
        print "(%d clauses)" % num_clauses(outcome)
        print "(%d universal clauses)" % num_univ_clauses(outcome)

    print "PDR: %.2fs" % watch.clock.elapsed
    print "N =", pdr.N
    print pdr.iteration_count, "iterations"
    print pdr.sat_query_count, "calls to SAT"

