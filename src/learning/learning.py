# encoding=utf-8
import linker
from mini_bmc import BMC
from z3 import *

from pattern.debug.profile import Stopwatch

def num_clauses(formula):
    if is_and(formula):
        return len(formula.children())
    elif formula == True or is_true(formula) or \
         formula == False or is_false(formula):
        return 0
    else:
        return 1

def num_univ_clauses(formula):
    if is_and(formula):
        fs = formula.children()
        n = 0
        for f in fs:
            if is_quantifier(f) and f.is_forall():
                n += 1
        return n
    elif is_quantifier(formula) and formula.is_forall():
        return 1
    else:
        return 0

def report_failure(pdr):
    #if pdr.counterexample:
    #    print '-' * 80
    #    display_structure(z3g.structure(pdr.counterexample), vocab)
    #    print '-' * 80
    #    return "invalid"
    #else:
    print 'Looking for concrete error trace...'
    bmc = BMC(pdr.Init, pdr.Rho, pdr.Bad, pdr.background, pdr.Locals)
    m = bmc.find_concrete_trace(pdr.N)
    if m:
        for m_i in m:
            print '-' * 80
            print m_i.get_universe(V)
            print "%s: %s" % (root, m_i.get_interp(root))
            for (c0, c) in locals:
                print "%s: %s" % (c, m_i.get_interp(c))
        print '-' * 80
        return "invalid"
    else:
        print "no concrete trace found (abstraction too coarse)"
        return "unknown"


if __name__ == '__main__':

    import argparse

    a = argparse.ArgumentParser()
    #a.add_argument('filename', type=str, nargs='?')
    a.add_argument('-v', '--verbose', action='store_true')
    args = a.parse_args()
    
    ##############################################################
    ##############       Learning switch       ###################
    ##############################################################

    #### Declarations
    SW = DeclareSort('SW')
    PR = DeclareSort('PR')
    HO = DeclareSort('HO')
    PK = DeclareSort('PK')
    BUF, (b_in, b_out) = EnumSort('BUF',('in','out'))

    Bool = BoolSort()
    ttrue = BoolVal(True)
    ffalse = BoolVal(False)

    sw_port   = Function('switch_port', SW, PR, Bool)
    link      = Function('link', PR, PR, Bool)
    src       = Function('src', PK, HO)
    dst       = Function('dst', PK, HO)

    traffic   = Function('traffic', PK, PR, BUF, PR, BUF, Bool)
    traffic0  = Function('traffic0', PK, PR, BUF, PR, BUF, Bool)
    rt        = Function('rt', SW, HO, PR, Bool)
    rt0       = Function('rt0', SW, HO, PR, Bool)
    fwd       = Function('fwd', SW, PK, PR, Bool)
    fwd0      = Function('fwd0', SW, PK, PR, Bool)

    exit      = Const('exit', Bool)
    exit0     = Const('exit0', Bool)

    #### Axioms
    h = Const('h', HO)

    s = Const('s', SW)
    t = Const('t', SW)

    u = Const('u', PR)
    v = Const('v', PR)
    w = Const('w', PR)

    u_buf = Const('u_buf', BUF)
    v_buf = Const('v_buf', BUF)
    w_buf = Const('w_buf', BUF)

    p = Const('p', PK)

    axioms_switch = [
            ForAll([s,t,u], Implies(And(sw_port(s,u), sw_port(t,u)), s == t))
            #ForAll([u], Exists([s], sw_port(s,u)))
            #ForAll(u, switch(u,u)),
            #ForAll([u,v], Implies(switch(u,v), switch(v,u))),
            #ForAll([u,v,w], Implies(
            #    And(switch(u,v), switch(v,w)),
            #    switch(u,w)))
        ]

    axioms_link = [
            ForAll([u], Not(link(u,u))),
            ForAll([u,v], link(u,v) == link(v,u)),
            ForAll([u,v,w],
                Implies(And(link(u,v), link(u,w)), v == w)),
            ForAll([u,v,s],
                Implies(And(sw_port(s,u), sw_port(s,v)), Not(link(u,v))))
        ] 

    #assumption_topo = \
    #    lambda s,p,i: \
    #        And([
    #            ForAll([t,u],
    #                Implies(And(link(u,i), sw_port(t,u)), fwd(t,p,u)))
    #            ])

    assumption_topo0 = \
        lambda s,p,i: \
            And([
                sw_port(s,i),
                ForAll([t,u],
                    Implies(And(link(u,i), sw_port(t,u)), fwd0(t,p,u)))
            ])

    ### Initial state
    init = And([
            ForAll([p,u,u_buf,v,v_buf],
                traffic(p, u, u_buf, v, v_buf) ==
                Or([ And([ u == v, u_buf == v_buf ]),
                     And([ link(u,v), u_buf == b_out, v_buf == b_in ])
                ])
            ),
            ForAll([s,h,u], Not(rt(s,h,u))),
            ForAll([s,p,u], Not(fwd(s,p,u))),
            Not(exit)
        ])

    #### Event parameters
    sw        = Const('sw', SW)
    sw0       = Const('sw0', SW)
    pkt       = Const('pkt', PK)
    pkt0      = Const('pkt0', PK)
    inport    = Const('inport', PR)
    inport0   = Const('inport0', PR)

    #### HACK: TODO: ask Nikolaj aboul model completion and sorts
    #s_hack    = Const('s_hack', SW)
    p_hack    = Const('p_hack', PK)
    h_hack    = Const('h_hack', HO)

    nonempty  = [ #Exists([s_hack], s_hack == s_hack),
                  Exists([p_hack, h_hack], src(p_hack) == h_hack) ]

    #### UPDR inputs
    globals = [ src, dst ]
    locals  = [ (traffic0, traffic),
                (rt0, rt),
                (fwd0, fwd),
                (exit0, exit),
                (sw0, sw),
                (pkt0, pkt),
                (inport0, inport)
              ]

    preds = [ sw_port, link, traffic, exit, fwd, rt ]
    
    background = And(axioms_link + axioms_switch + nonempty)

    cond  = Not(exit)
    cond0 = Not(exit0)

    trans = And([
        sw_port(sw0, inport0),
        #assumption_topo0(sw0, pkt0, inport0),
        # 1. The packet src is already learned.
        Implies(
            Exists([u], rt0(sw0, src(pkt0), u)),
            And([
                # Then rt and traffic are not changed
                ForAll([s,h,u], rt(s,h,u) == rt0(s,h,u)),
                ForAll([p,u,u_buf,v,v_buf],
                    traffic (p,u,u_buf,v,v_buf) ==
                    traffic0(p,u,u_buf,v,v_buf))
            ]) 
        ),
        # 2. The packet src is not learned yet
        Implies(
            Not(Exists([u], rt0(sw0, src(pkt0), u))),
            And([
                # Learn the source of the packet..
                ForAll([s,h,u],
                    rt(s,h,u) ==
                        Or(rt0(s,h,u),
                           And([ s == sw0, h == src(pkt0), u == inport0 ]))
                ),
                # ..and update the traffic relation
                ForAll([p, u, u_buf, v, v_buf, w, w_buf],
                    traffic(p, u, u_buf, v, v_buf) ==
                    And([
                        Implies(
                            #p == pkt0,
                            dst(p) == src(pkt0),
                            Or([
                                traffic0(p, u, u_buf, v, v_buf),
                                And([
                                    sw_port(sw0, w),
                                    traffic0(p, u, u_buf, w, b_in),
                                    traffic0(p, inport0, b_out, v, v_buf)
                                ])
                            ])
                        ),
                        #Implies(p != pkt0, traffic0(p, u, u_buf, v, v_buf))
                        Implies(dst(p) != src(pkt0), traffic0(p, u, u_buf, v, v_buf))
                    ])
                )
            ]) 
        ),
        And([
            # Regular forwarding
            Implies(
                Exists([u], rt(sw0, dst(pkt0), u)),
                ForAll([s,p,u,v],
                    Implies(
                        rt(sw0, dst(pkt0), v),
                        fwd(s,p,u) ==
                            Or(fwd0(s,p,u),
                               And([ s == sw0, p == pkt0, u == v ]))
                    )
                )
            ),
            # Flooding
            Implies(
                Not(Exists([u], rt(sw0, dst(pkt0), u))),
                ForAll([s,p,u],
                    fwd(s,p,u) ==
                        Or(fwd0(s,p,u),
                           And([ s == sw0, p == pkt0, u != inport0 ])))
            )
        ])
    ])

    rho = And(cond0, trans)

    #bad = And([
    #        Not(cond),
    #        Exists([p,u,v,v_buf],
    #            And([
    #                u != v,
    #                traffic(p, u, b_in, v, v_buf),
    #                traffic(p, v, v_buf, u, b_in)
    #            ])
    #        )
    #    ])
    bad = And([
            Not(cond),
            Exists([s,t,p,u,v,v_buf],
                And([
                    u != v,
                    s != t,
                    sw_port(s,u),
                    sw_port(t,v),
                    traffic(p, u, b_in, v, v_buf),
                    traffic(p, v, v_buf, u, b_in)
                ])
            )
        ])

    ##############################################################

    print "*** Using universal invariant inference"
    from mini_pdr import PDR

    print "* GLOBALS"
    print globals

    print "* LOCALS"
    print locals

    pdr = PDR(init, rho, bad, background, globals, locals, preds, universal=True, enumsorts=[BUF])

    if args.verbose:
        pdr.verbose = True

    try:
        watch = Stopwatch()
        with watch:
            outcome = pdr.run()
        if outcome:
            status = "valid"
            if hasattr(pdr, 'verify') and not pdr.verify(outcome):
                print "incorrect!!"
                status = "incorrect"
        else:
            status = report_failure(pdr)

    except KeyboardInterrupt:
        outcome = None
        status = "timeout"
        
    fol_invariant = None
    if outcome:
        print "(%d clauses)" % num_clauses(outcome)
        print "(%d universal clauses)" % num_univ_clauses(outcome)

    print "PDR: %.2fs" % watch.clock.elapsed
    print "N =", pdr.N
    print pdr.iteration_count, "iterations"
    print pdr.sat_query_count, "calls to SAT"

