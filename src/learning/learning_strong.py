# encoding=utf-8
import linker
from mini_bmc import BMC
from z3 import *

from pattern.debug.profile import Stopwatch

def num_clauses(formula):
    if is_and(formula):
        return len(formula.children())
    elif formula == True or is_true(formula) or \
         formula == False or is_false(formula):
        return 0
    else:
        return 1

def num_univ_clauses(formula):
    if is_and(formula):
        fs = formula.children()
        n = 0
        for f in fs:
            if is_quantifier(f) and f.is_forall():
                n += 1
        return n
    elif is_quantifier(formula) and formula.is_forall():
        return 1
    else:
        return 0

def report_failure(pdr):
    #if pdr.counterexample:
    #    print '-' * 80
    #    display_structure(z3g.structure(pdr.counterexample), vocab)
    #    print '-' * 80
    #    return "invalid"
    #else:
    print 'Looking for concrete error trace...'
    bmc = BMC(pdr.Init, pdr.Rho, pdr.Bad, pdr.background, pdr.Locals)
    m = bmc.find_concrete_trace(pdr.N)
    if m:
        for m_i in m:
            print '-' * 80
            print m_i.get_universe(V)
            print "%s: %s" % (root, m_i.get_interp(root))
            for (c0, c) in locals:
                print "%s: %s" % (c, m_i.get_interp(c))
        print '-' * 80
        return "invalid"
    else:
        print "no concrete trace found (abstraction too coarse)"
        return "unknown"


if __name__ == '__main__':

    import argparse

    a = argparse.ArgumentParser()
    #a.add_argument('filename', type=str, nargs='?')
    a.add_argument('-v', '--verbose', action='store_true')
    args = a.parse_args()
    
    ##############################################################
    ##############       Learning switch       ###################
    ##############################################################

    #### Declarations
    SW = DeclareSort('SW')
    PR = DeclareSort('PR')
    HO = DeclareSort('HO')
    PK = DeclareSort('PK')

    Bool = BoolSort()

    link      = Function('link', SW, PR, PR, SW, Bool)
    src       = Function('src', PK, HO)
    dst       = Function('dst', PK, HO)

    traffic   = Function('traffic', PK, SW, PR, SW, PR, Bool)
    traffic0  = Function('traffic0', PK, SW, PR, SW, PR, Bool)
    rt        = Function('rt', SW, HO, PR, Bool)
    rt0       = Function('rt0', SW, HO, PR, Bool)
    fwd       = Function('fwd', SW, PK, PR, Bool)
    fwd0      = Function('fwd0', SW, PK, PR, Bool)

    exit      = Const('exit', Bool)
    exit0     = Const('exit0', Bool)

    #### Axioms
    h = Const('h', HO)

    s = Const('s', SW)
    t = Const('t', SW)
    s_bad = Const('s_bad', SW)
    t_bad = Const('t_bad', SW)
    t1 = Const('t1', SW)
    t2 = Const('t2', SW)

    u = Const('u', PR)
    v = Const('v', PR)
    w = Const('w', PR)
    u_bad = Const('u_bad', PR)
    v_bad = Const('v_bad', PR)
    w_bad = Const('w_bad', PR)

    h_bad = Const('h_bad', HO)

    port0 = Const('port0', PR)
    port1 = Const('port1', PR)
    port2 = Const('port2', PR)

    p = Const('p', PK)
    p_bad = Const('p_bad', PK)

    axioms_switch = [
            #ForAll([s,t,u], Implies(And(sw_port(s,u), sw_port(t,u)), s == t))
        ]

    axioms_link = [
            ForAll([s,t,u,v], link(s,u,v,t) == link(t,v,u,s)),
            ForAll([s,t1,t2,u,v,w],
                Implies(And(link(s,u,v,t1), link(s,u,w,t2)),
                        And(t1 == t2, v == w))),
            ForAll([s,u,v], Not(link(s,u,v,s)))
        ] 

    axioms_port = [
            ForAll([u], Or([ u == port0, u == port1, u == port2 ]))
        ]

    assumption_topo0 = \
        lambda s,p,i: \
            And([
                ForAll([t,u],
                    Implies(link(s,i,u,t), fwd0(t,p,u)))
            ])

    ### Initial state
    init = And([
            ForAll([p,s,u,t,v],
                traffic(p, s, u, t, v) ==
                Or([ And([ s == t, u == v ]),
                     And([ link(s,u,v,t) ])
                ])),
            ForAll([s,h,u], Not(rt(s,h,u))),
            ForAll([s,p,u], Not(fwd(s,p,u))),
            Not(exit)
        ])

    #### Event parameters
    sw        = Const('sw', SW)
    sw0       = Const('sw0', SW)
    pkt       = Const('pkt', PK)
    pkt0      = Const('pkt0', PK)
    inport    = Const('inport', PR)
    inport0   = Const('inport0', PR)

    #### HACK: TODO: ask Nikolaj aboul model completion and sorts
    #s_hack    = Const('s_hack', SW)
    p_hack    = Const('p_hack', PK)
    h_hack    = Const('h_hack', HO)

    nonempty  = [ #Exists([s_hack], s_hack == s_hack),
                  Exists([p_hack, h_hack], src(p_hack) == h_hack) ]

    #### UPDR inputs
    globals = [ port0, port1, port2, src, dst ]
    locals  = [ (traffic0, traffic),
                (rt0, rt),
                (fwd0, fwd),
                (exit0, exit),
                (sw0, sw),
                (pkt0, pkt),
                (inport0, inport)
              ]

    preds = [ link, traffic, exit, fwd, rt ]
    
    background = And(axioms_link + axioms_switch + axioms_port + nonempty)

    cond  = Not(exit)
    cond0 = Not(exit0)

    trans = And([
        assumption_topo0(sw0, pkt0, inport0),
        # 1. The packet src is already learned.
        Implies(
            Exists([u], rt0(sw0, src(pkt0), u)),
            And([
                # Then rt and traffic are not changed
                ForAll([s,h,u], rt(s,h,u) == rt0(s,h,u)),
                ForAll([p,s,u,t,v],
                    traffic (p,s,u,t,v) ==
                    traffic0(p,s,u,t,v))
            ]) 
        ),
        # 2. The packet src is not learned yet
        Implies(
            Not(Exists([u], rt0(sw0, src(pkt0), u))),
            And([
                # Learn the source of the packet..
                ForAll([s,h,u],
                    rt(s,h,u) ==
                        Or(rt0(s,h,u),
                           And([ s == sw0, h == src(pkt0), u == inport0 ]))
                ),
                # ..and update the traffic relation
                ForAll([p, s, u, t, v],
                    traffic(p, s, u, t, v) ==
                    And([
                        Implies(
                            dst(p) == src(pkt0),
                            Or([
                                traffic0(p, s, u, t, v),
                                And([
                                    traffic0(p, s, u, sw0, port0),
                                    traffic0(p, sw0, inport0, t, v)
                                ]),
                                And([
                                    traffic0(p, s, u, sw0, port1),
                                    traffic0(p, sw0, inport0, t, v)
                                ]),
                                And([
                                    traffic0(p, s, u, sw0, port2),
                                    traffic0(p, sw0, inport0, t, v)
                                ]),
                            ])
                        ),
                        Implies(dst(p) != src(pkt0), traffic0(p, s, u, t, v))
                    ])
                )
            ]) 
        ),
        And([
            # Regular forwarding
            Implies(
                Exists([u], rt(sw0, dst(pkt0), u)),
                ForAll([s,p,u,v],
                    Implies(
                        rt(sw0, dst(pkt0), v),
                        fwd(s,p,u) ==
                            Or(fwd0(s,p,u),
                               And([ s == sw0, p == pkt0, u == v ]))
                    )
                )
            ),
            # Flooding
            Implies(
                Not(Exists([u], rt(sw0, dst(pkt0), u))),
                ForAll([s,p,u],
                    fwd(s,p,u) ==
                        Or(fwd0(s,p,u),
                           And([ s == sw0, p == pkt0, u != inport0 ])))
            )
        ])
    ])

    rho = And(cond0, trans)

    bad = And([
            Not(cond),
            Exists([h_bad,p_bad,s_bad,t_bad,u_bad,v_bad,w_bad],
                And([
                    s_bad != t_bad,
                    w_bad != v_bad,
                    dst(p_bad) == h_bad,
                    traffic(p_bad, s_bad, u_bad, t_bad, v_bad),
                    rt(t_bad, h_bad, w_bad),
                    traffic(p_bad, t_bad, w_bad, s_bad, u_bad)
                ])
            )
        ])

    ##############################################################

    print "*** Using universal invariant inference"
    from mini_pdr import PDR

    print "* GLOBALS"
    print globals

    print "* LOCALS"
    print locals

    pdr = PDR(init, rho, bad, background, globals, locals, preds, universal=True)

    if args.verbose:
        pdr.verbose = True

    try:
        watch = Stopwatch()
        with watch:
            outcome = pdr.run()
        if outcome:
            status = "valid"
            if hasattr(pdr, 'verify') and not pdr.verify(outcome):
                print "incorrect!!"
                status = "incorrect"
        else:
            status = report_failure(pdr)

    except KeyboardInterrupt:
        outcome = None
        status = "timeout"
        
    fol_invariant = None
    if outcome:
        print "(%d clauses)" % num_clauses(outcome)
        print "(%d universal clauses)" % num_univ_clauses(outcome)

    print "PDR: %.2fs" % watch.clock.elapsed
    print "N =", pdr.N
    print pdr.iteration_count, "iterations"
    print pdr.sat_query_count, "calls to SAT"

