%!TEX root = ./paper.tex

\newcommand{\bound}{\textit{bound}}

\section{Sufficient Conditions for Termination}\label{Se:Termination}

%\todo[inline]{Make it more general than EPR}

The $\UPDR$ algorithm is in general not guaranteed to terminate: it does not restrict the number
of distinct existentially quantified variables in diagrams. This comes as a blessing for finding
invariants with an arbitrary number of quantified variables, but is a curse when it comes
to bounding the search space. There are however non-trivial classes of programs where it terminates.
In particular, we show that $\UPDR$ terminates for programs that manipulate singly linked lists.
Inferring universal invariants for this class of programs was shown to be \revUpdate{decidable~\cite[Theorem 4.2]{Padon:POPL16}
using the na{\"i}ve backwards reachability algorithm}{} presented in~\Cref{alg:backward-simple}.
The main result in this section bridges backwards reachability with $\UPDR$, here formulated
for a transition system $\TS = (\minit,\rho)$ and a property $\Prop$, with $\mbad = \neg \Prop$:

\begin{proposition}\label{prop:main-termination-result}
If backwards reachability, given by~\Cref{alg:backward-simple}, terminates on input transition system $\TS$ and property $\Prop$,
then $\UPDR$ terminates as well.
\end{proposition}

In order to establish this result, we introduce a notion of \emph{effective} encodings of the (backwards) reachable states, $\backR$
(defined in \Cref{sec:frame-properties}).
In a nutshell, a set of (backwards) reachable states $\backR$ is effective for a class of transition systems and properties if
the set of reachable states can be defined using a finitary existential formula, i.e., a formula that uses a finite number
of quantified variables and contains a finite number of sub-formulae.
This allows us to connect backwards reachability with $\UPDR$ using the two lemmas:
\begin{lemma}\label[lemma]{lemma:back-to-effective}
If \Cref{alg:backward-simple} terminates on $\TS$ and $\Prop$ then $\backR$ is effective.
\end{lemma}
and
\begin{lemma}\label[lemma]{lemma:effective-to-updr}
If $\backR$ is effective for $\TS$ and $\Prop$ then $\UPDR$ is guaranteed to terminate.
%if the following assumptions hold:
%\begin{enumerate}
%%\item \label{asmp:universal} $\minit, \mbad$ and $\rho$ are universal relational formulae,
%%\item \label{asmp:min} For every EPR formula $\psi$, $\SAT{\psi}$ returns a model of size at most $n_{\psi}$, and
%\item \label{asmp:logic} all satisfiability queries are in $\logic$ which is effective, and
%\item \label{asmp:effective}  $\backR$ is effective.
%\end{enumerate}
\end{lemma}

We start with the description of \Cref{alg:backward-simple}.
It computes the complement of the least fixpoint of diagrams that can
reach (via a relaxed trace) the bad states $\mbad$.
%\alex{Why is it a least fixpoint? Don't we need for that some sort
%  of ``monotonicity'' of $\Trans$? SH: why? we have monotonicity since we use disjunction.}
If the states represented by the resulting formula do not properly contain the initial
states $\minit$, then there are no universal inductive invariants, otherwise there is one.
\begin{algorithm}
\setcounter{AlgoLine}{0}
\caption{Na{\"i}ve Backward Reachability}
\label{alg:backward-simple}
%\footnotesize
\DontPrintSemicolon
$I$ := $\mtrue$ \;
%\While{$I$ is not an inductive invariant for $(\TS,\Prop)$}{
\While{$\SAT{I \wedge \mbad}$ \textbf{or} $\SAT{I \wedge \Trans \wedge (\neg I)'}$}{
    $(\state,\state') \assign \findModel{(I \land \mbad) \lor (I \wedge \Trans \wedge (\neg I)')}$ \;
    $I$ := $I \wedge \neg\kDiag(\state)$ \; \label{line:strengthen}
}
% \lIf{$\SAT{\minit \wedge \neg I}$\label{line:if-init}} {
%          \Return{no universal inductive invariant} \label{line:no-ind} %\;
% }
% \lElse{\Return{$I$ is a universal inductive invariant}}
\If {$\SAT{\minit \wedge \neg I}$}{\label{line:if-init}
  \Return{no universal inductive invariant} \label{line:no-ind} %\;
}
\Else{\Return{$I$ is a universal inductive invariant}}
\end{algorithm}


Next, let us define what we mean by a set of backward reachable states being \emph{effective}.
Recall that we use $\backR_i$ to denote
the set of states that can reach a bad state via a relaxed trace of $\TS$ in at most $i$ steps, and $\backR$ to denote
the set of states that can reach a bad state via a relaxed trace in
some number of steps.
Thus, $\backR = \bigcup_{i \geq 0} \backR_i$ (see \Cref{sec:frame-properties}).
\begin{definition}
We say that $\backR$ is \emph{effective} if
%there exists $k$ such that $\backR = \backR_k$, and in addition $\backR$
it can be described by an existential formula. That is, there exists an existential formula $\psi_{\backR}$ such that
$\backR = \{ \state \mid \state \models \psi_{\backR}\}$.
\end{definition}

\Cref{lemma:back-to-effective} follows from the description of the na{\"i}ve algorithm and the definition of effective states:

\begin{proof}[of~\Cref{lemma:back-to-effective}]
Suppose \Cref{alg:backward-simple} terminates, and let $\neg I$ be the negation of the obtained formula, $I$.
Then $\backR = \{\cmodel \mid \cmodel \models \neg I\}$. Further, since $\neg I$ is equivalent to a disjunction of diagrams, it is an existential formula.
This ensures effectiveness.
%This ensures the second effectiveness condition. As for the first, let $k$ be the number of loop iterations executed in \Cref{alg:backward-simple}.
%Then $\backR_k = \backR$.
\end{proof}

Establishing \Cref{lemma:effective-to-updr} requires a bit more context.
Recall that we require that every satisfiable formula in $\logic$ has a finite model, and
assume to have a decision procedure $\SAT{\psi}$, which checks if a formula
$\psi$ in $\logic$ is satisfiable, and a function $\findModelKw(\psi)$, which
returns a \emph{finite} model $\cmodel$ of $\psi$ if such a model exists and $\NoneKw$  otherwise.
For the termination argument, we place a stronger requirement: that finite model sizes are functions of the number of
constants and existentially quantified variables.
% is needed. Namely, we require
In other words, we require the existence of a function $\bound: \Nat \to \Nat$ that
given a formula $\psi$ in $\logic$, with at most $n_{\psi}$ existentially quantified variables and constants,
$\psi$ has a finite model if and only if
%
it has a model which contains no more than  $\bound(n_{\psi})$ elements.
%the number of elements of it is bounded by $\bound(n_{\psi})$.
%
Note that such as bound only depends on the existential quantifiers
and constants, and not, e.g., on the number of universal quantifiers.
\revUpdate{We note that {\EPR} has such a bound; see \Cref{Se:TerminationEPR}.}
%and the function is necessarily monotone in $n_{\psi}$.

%\alex{It is not necessarily monotone in $n_{\psi}$. If a bounding
%funcion exists, one can construct infinitely many bounding functions,
%non-monotonic ones too.}

Further, we also assume that $\findModelKw(\psi)$ always returns a model of size at most $\bound(n_{\psi})$.
%For simplicity, we assume that $\bound$ is monotonically increasing.
%The latter requirement ensures that the size of the obtained model does not depend on the size of $\psi$, b
%
Assuming that $\logic$ satisfies the additional requirement, and that all
satisfiability checks performed by $\UPDR$ are in $\logic$,
we can prove \Cref{lemma:effective-to-updr}.

%
%%%%%
%% NSB: TODO ----------------------
%%%%
%





%if it can be
%described by an existential formula, and in addition there exists $k$ such that $\backR = \backR_k$. \sharon{is the latter implied by the former?}
%%Note that for every for every $i$, $\backR_i \implies \backR$. Therefore, if $\backR$ can be described by an existential formula



\begin{proof}[of~\Cref{lemma:effective-to-updr}]
The proof consists of two main arguments. First, we show that there exists a window of frames of a fixed length in which $\UPDR$ generates new clauses.
Then, we show that the size of generated clauses is bounded by a function that depends on their distance from the last frame. As the latter is bounded by the length of the window,
we obtain a bound on the size of clauses, which ensures termination.
%
% NSB: above is a good summary.
%
%In the proof we use sets of states and their representation as formulae interchangeably.


We first note that since $\backR$ is effective, there exists $k$ such that $\backR = \backR_k$.
This holds because  every existential formula can be written as a finite disjunction of
diagrams.
%(By case splitting on whether the existentially quantified
%variables are distinct or not and on whether the missing instances of
%constants and relations appear negatively or positively.)
Indeed, this can be achieved by converting the formula to DNF and performing for
every existential
cube a case splitting on whether the existentially quantified
variables are distinct or not and on whether the missing instances of
constants and relations appear negatively or positively.
Therefore, effectiveness of $\backR$ implies that there exists a finite set of states $\state_1,\ldots,\state_m$
such that $\backR = \{\state \mid \state \models \bigvee_{i=1}^m \kDiag(\state_i) \}$.
For each $i =1,\ldots,m$, let $k_i$ be the length of a shortest relaxed trace leading from
$\state_i$ to a bad state, and let $k  = \max_{i=1}^m k_i$.
Since every state in $\backR$ is a model of $\kDiag(\state_i)$ for at least one of the states $\state_i$, every such
state starts a relaxed trace of length at most $k_i$ to a bad state.
Therefore, $\backR = \backR_k$.
Hence for every $i > k$, $\backR_i = \backR_k$ as well.

We now turn to show that in each step, $\UPDR$ only generates new clauses in the last $k$ frames.
%Moreover, $J =\neg \backR$ is a universal inductive invariant.
%
Consider a fixed $N$.
By \Cref{lem:frame-disjoint-from-backr},
%It can be shown by a simple induction that
%$\UPDR$, similarly to traditional $\PDR$, has the property that
it holds that for every $j < N$, if $\state \models \aframe{j}$ then $\state \not \in \backR_{N-1-j}$.
%(i.e., $\aframe{j}$ does not include any state that reaches a bad state via a relaxed trace in $N-1-j$ steps or less).
By the choice of $k$, if
%$N-1-j > k$
$N-1-j \geq k$
then $\backR_{N-1-j} = \backR$.
This means that for every
%$j < N-1-k$,
$j \leq N-1-k$,
if $\state \models \aframe{j}$ then $\state \not \in \backR$.
%$\aframe{j} \cap \backR = \emptyset$.
On the other hand, by \Cref{lem:block-in-backr}, every state that
$\UPDR$ attempts to block in frame $\aframe{j}$ is a state in $\backR$.
%it can also be proven by induction
%(similarly to the the proof of \Cref{Pr:AbstracCEX}) that every state that
%$\UPDR$ attempts to block in frame $\aframe{j}$ is a state in $\backR$.
As no such state exists for
%$j < N-1-k$,
$j \leq N-1-k$,
we conclude that no state is blocked at $\aframe{j}$ for
%$j < N-1-k$.
$j \leq N-1-k$.
Therefore, new clauses, which result from blocked states, are only generated in the last $k+1$ frames
%$\aframe{N-1-k}$ to $\aframe{N}$.
$\aframe{N-k}$ to $\aframe{N}$.
(They are of course pushed backwards once generated.)

%As explained above, assumption~\ref{asmp:universal} ensures that all satisfiability
%queries performed by $\UPDR$ consist of EPR formulae.
We now show that we can bound the size of models obtained by $\UPDR$ in its backward traversal, and hence can bound the size of generated clauses.
Let $n_c$ denote the number of constants in $\vocabulary$, $n_{\mbad}$ denote the number of existential quantifiers in $\mbad$ and $n_{\rho}$
denote the number of existential quantifiers in $\rho$.
We show by induction on $j$ that we can bound the size of models obtained by $\UPDR$ in frame $N-j$ by a function of $n_c$, $n_{\mbad}$ and $n_{\rho}$
(i.e., the bound does not depend on additional parameters such as $N$).
For the base case ($j=0$) recall that the backward traversal starts from a state $\state \models \aframe{N} \land \mbad$. Since $\aframe{N}$ is a universal formula,
the properties of $\logic$ ensure that the size of the obtained model is bounded by $\bound(n_c + n_{\mbad})$.
For the induction step, when $\UPDR$ makes a step backward from a diagram of a state $\state$ using $\rho$, it uses the formula $\aframe{j-1} \land \rho \land (\varphi)'$,
where $\varphi = \kDiag(\state)$.
The only existential quantifiers in this formula result from $\rho$ and $\varphi$, where the number of the latter is equal to the size of the domain of $\state$
(by the construction of the diagram).
Our assumptions on $\logic$ and on $\findModelKw(\psi)$ therefore ensure that
the size of  the domain of the obtained model is bounded by
\revUpdate{%
%$\bound(n_c + n_{\rho} + n)$
\begin{equation}
  \label{eq:bound}\tag{*}
  \bound(n_c + n_{\rho} + n)
\end{equation}
}
where $n$ is the size of $\state$.
By the induction hypothesis, $n$ is bounded by a function of $n_c$, $n_{\mbad}$ and $n_{\rho}$ only, hence the claim follows.

We denote by $f_j$ the function of $n_c$, $n_{\mbad}$ and $n_{\rho}$ that provides a bound on the size of models obtained by $\UPDR$ in frame $N-j$.
Therefore, when $\UPDR$ makes at most
%$k+1$
$k$
backward steps from $\mbad$, the number of elements in the obtained
models is bounded by
\revUpdate{
\begin{equation*}
  \mmax = \max_{j=0}^{k} f_{j} (n_c, n_{\mbad}, n_{\rho})\,.
\end{equation*}
}
%$max = \max_{j=0}^{k} f_{j} (n_c, n_{\mbad}, n_{\rho})$.
This provides a bound on the number of elements in the models that $\UPDR$ tries to block.
%in frames $\aframe_{N-k}$ to $\aframe_N$.


%As explained above, assumption~\ref{asmp:universal} ensures that all satisfiability
%queries performed by $\UPDR$ consist of EPR formulae.
%Now we can use assumption~\ref{asmp:min} to ensure that whenever $\UPDR$ makes a step backwards
%from a diagram of a state $\state'$ using $\rho$, then the size of the obtained model $\state$
%grows by at most a fixed number $m$ of elements compared to $\state'$.
%$m$ is determined by the number of constants in the vocabulary.
%(This is because the number of existential quantifiers in the query is equal
%to the size of the domain of $\state'$.)
%Further, by the same assumption, the number of elements in a bad state returned by $\SAT{\aframe{N} \land \mbad}$
%is also bounded by $m$.
%Therefore, when $\UPDR$ makes at most $k$ backward steps from $\mbad$, the number of elements in the obtained models is bounded by $m*(k+1)$.
%This provides a bound on the number of elements in the models that $\UPDR$ tries to block.
%%in frames $\aframe_{N-k}$ to $\aframe_N$.

Altogether, we conclude that the clauses generated by $\UPDR$ (blocked diagrams) have at most $\mmax$ quantifiers,
which makes the potential number of clauses finite and hence ensures termination of $\UPDR$.
\end{proof}



\subsection{Termination when Reasoning with Effectively Propositional Logic}\label{Se:TerminationEPR}

If $\minit$ and $\Trans$ are $\EPR$ formulae and $\Prop$ is a universal formula, then all the satisfiability queries made by $\UPDR$ are of $\EPR$ formulae.
$\EPR$ has the finite model property, and its satisfiability is decidable. Further, $\EPR$ meets the additional requirement needed for termination,
as an $\EPR$ formula $\psi$ is satisfiable if and only if it has a satisfying model whose size is bounded by $n_{\psi}$,
where $n_{\psi}$ is the number of constants and existentially quantified variables in $\psi$.
That is, for every $\EPR$ formula $\psi$,  $\bound(n_{\psi}) = n_{\psi}$.
\revUpdate{The existence of this bound is a well-known property of the Bernays-Sch{\"o}nfinkel-Ramsey class of first-order formulae~\cite{BGG}.}

Note that in this case, the bound on the number of quantifiers in clauses generated by $\UPDR$ in the last $k+1$ frames
can be explicitly expressed as $(k+1)\cdot n_c + k \cdot n_{\rho} + n_{\mbad}$,
where $n_c$ denotes the number of constants in $\vocabulary$, $n_{\mbad}$ denotes the number of existential quantifiers in $\mbad$ and $n_{\rho}$
denotes the number of existential quantifiers in $\rho$.
\revUpdate{This can be proven by induction on $k$ using the bound (\ref{eq:bound}).}

In~\cite{Padon:POPL16}, it is shown that \Cref{alg:backward-simple} is guaranteed to terminate
\revUpdate{in the case where the substructure relation is a well-quasi-order on the set of states%
\footnote{%
\revUpdate{%
A \emph{well-quasi-ordering} $\sqsubseteq$ on a set $X$ is a preorder (i.e., a
reflexive, transitive binary relation) such that any infinite sequence of
elements $x_0,x_1,\ldots$ from $X$ contains an increasing pair $x_i \sqsubseteq
x_j$ with $i<j$.}
}.
This is the case for}{} programs manipulating singly linked lists
which are modeled in $\EPR$ using a single transitive reflexive binary relation for $n^*$ (axiomatized as in \cite{CAV:IBINS13}),
any number of constants and any number of unary relations (but no additional
binary or higher-arity relations)~\cite{Padon:POPL16}.
We therefore conclude that $\UPDR$ also terminates on such programs.



%We consider the case where $\minit, \mbad$ and $\rho$ are universal formulae.
%This ensures that the frames computed by $\UPDR$ are also universal formulae.
%In addition, it ensures that all satisfiability queries $\SAT{\psi}$ consist of EPR formulae.
%EPR formulae have the property that a formula $\psi$ is satisfiable if and only if it has
%a satisfying model whose domain is bounded by the number of constants and existentially quantified variables in $\psi$.
%We denote the latter number by $n_{\psi}$, i.e. $n_{\psi}$ is the number of
%constants and existentially quantified variables in $\psi$.
%In this section, we assume that $\SAT{\psi}$ returns a model of size at most $n_{\psi}$.
%This is required for the termination guarantees.


\done{Add that the conditions hold for EPR/linked list}

%\todo[inline]{explain that for singly linked lists, modeled by $n^*$ with no additional binary relation,
%all assumptions hold and therefore termination is guaranteed.
%Assumption~\ref{asmp:effective} follows from the fact that the substructure relation is a well quasi order in this case.}


%We consider the case where $\minit, \mbad$ and $\rho$ are universal formulae.
%This ensures that the frames computed by $\UPDR$ are also universal formulae.
%In addition, it ensures that all satisfiability queries $\SAT{\psi}$ consist of EPR formulae.
%EPR formulae have the property that a formula $\psi$ is satisfiable if and only if it has
%a satisfying model whose domain is bounded by the number of constants and existentially quantified variables in $\psi$.
%We denote the latter number by $n_{\psi}$, i.e. $n_{\psi}$ is the number of
%constants and existentially quantified variables in $\psi$.
%In this section, we assume that $\SAT{\psi}$ returns a model of size at most $n_{\psi}$.
%This is required for the termination guarantees.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
