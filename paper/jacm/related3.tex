%!TEX root = ./paper.tex

%\vspacex{-1.5ex}
\section{Related Work}
\label{Se:RelatedWork}
%\vspacex{-1.5ex}
%In this section, we briefly summarize the extensive volume of related work.
Synthesizing quantified invariants has received significant attention.
Several works have considered discovery of quantified predicates, e.g.,
based on counterexamples~\cite{DBLP:conf/fmcad/DasD02} or
by extension of predicate abstraction to support free variables~\cite{POPL:FQ02,Lahiri:2007:PAI:1297658.1297662}.
Our inferred invariants are comprised of universally quantified predicates,
but unlike these approaches, our computation of the predicates is property directed and does \emph{not} employ predicate abstraction.
Additional works for generation of quantified invariants
include using abstract domains of \emph{quantified data automata}~\cite{CAV:GLMN13,SAS:GMP13} or ones  tailored to Presburger arithmetic with arrays~\cite{DBLP:conf/oopsla/DilligDA10},
instantiating quantifier
templates~\cite{DBLP:conf/sas/BjornerMR13,PLDI:SrivastavaG09}, or
applying symbolic proof techniques~\cite{DBLP:conf/tacas/HoderKV11}.
% ,
% or using abstractions based on separation logic~\cite{TACAS:DOY06,CAV:BCCDOWY07}.

%\vspacex{-0.1ex}
Other works aim to identify loop invariants \emph{given} a set of predicates as
candidate ingredients. Houdini \cite{FME:FL01} is the first such algorithm of which we are aware.
Santini \cite{ENTCS:TLLR13,UW-TR-1790:TLLR13} is a recent algorithm
which is based on full predicate abstraction.
In the context of IC3, predicate abstraction was used in
~\cite{DBLP:conf/cav/BirgmeierBW14,DBLP:conf/tacas/CimattiGMT14,CAV14:Itzhaky},
the last of which specifically targeting shape analysis.
% In contrast to previous work, our algorithm
% does not require a pre-defined set of predicates, and is therefore more automatic:
The above require a a predefined set of predicates, and are therefore less
automatic than our approach, since
the diagrams provide an ``on-the-fly'' abstraction mechanism.
\revUpdate{
A CEGAR-based  approach for inferring predicates, is shown in~\cite{Podelski:2010}.
They go beyond the usual state predicates by introducing unary predicates that range over heap
locations.
This allows to infer quantified properties and permits lazy refinement to
improve performance, but does not provide completeness guarantees.
In addition, the inferred invariants have quantifier-nesting depth of one,
whereas \UPDR\ allows for arbitrary nesting depth.}

%\vspacex{-0.1ex}
PDR has been shown to work extremely well
in other domains, such as hardware verification
\cite{VMCAI:Bradley11,FMCAD:EMB11}.
Subsequently, it was generalized to software model checking
for program models that use linear real arithmetic.
The approach in~\cite{SAT:HB12} uses conflict-based projection for linear real arithmetic,
while~\cite{CAV:CG12}
employs a quantifier-elimination procedure for linear
real arithmetic to provide an approximate pre-image operation.
Finally,~\cite{DBLP:conf/cav/KomuravelliGC14}
uses model-based projection to extract under-approximations of pre-images.
In contrast, our use of diagrams allows us to obtain a natural approximation
which is precise for programs that can be verified using universal invariants.


%\vspacex{-0.1ex}
The reduction we use into EPR creates a parametrized array-based system
(where the range of the arrays are Booleans).
A number of tools have been developed for general array-based systems.
The SAFARI~\cite{DBLP:conf/cav/AlbertiBGRS12} system is relevant.
It is related to MCMT and
Cubicle~\cite{DBLP:conf/cade/GhilardiR10,DBLP:journals/corr/abs-1010-1872,DBLP:conf/fmcad/ConchonGKMZ13,DBLP:conf/cav/ConchonGKMZ12}.
SAFARI uses symbolic
preconditions to propagate symbolic states in the form of cubes that
are conjunctions of literals over array constraints, and uses interpolants to synthesize universal invariants.
Our method for propagating and inductively generalizing diagrams differs by being based on PDR.
\revUpdate{More generally, our use of diagrams lazily produces finite state abstractions of array-based systems.
Lazy abstraction was applied in~\cite{DBLP:conf/popl/HenzingerJMS02}, and shown to terminate when the
abstraction structures satisfy the ascending-chain condition, e.g., that every chain of abstractions is well-founded.
In~\cite{DBLP:conf/tacas/JhalaM06}, various abstraction domains are considered based on interpolation where termination
of the interpolation process can be enforced by limiting the creation of atomic formulae to use only existing sub-terms.
%forcing interpolation procedures to only create atomic formulae that use existing sub-terms.
%These conditions pertains to
In contrast, our termination argument is based on well-quasi-orders.
}

%\vspacex{-0.1ex}
% The logic used by our implementation
% has limited capabilities to express properties  of list segments
% that are not pointed to by  variables~\cite{CAV14:Itzhaky}.
% This is similar to the self-imposed limitations on
% expressibility used in a number of shape analysis algorithms~\cite{TOPLAS:SRW02,VMCAI:MYRS05,CAV:LIS06,TACAS:DOY06,CAV:BCCDOWY07,TACAS:YRS04,Podelski:2010}.
% Past experience, as well as our own,
% has shown that despite these limitations it is still possible
% to successfully analyze a rich set of programs
% and properties.

%
% In this paper, our abstract domain is based on formulae expressed
% in $\AF$, which has very limited capabilities to express properties
% of stretches of data structures that are not pointed to by a program
% variable.

The logic used by our implementation
has limited capabilities to express properties  of list segments
that are not pointed to by  variables~\cite{CAV14:Itzhaky}.
This is similar to the self-imposed limitations on
expressibility used in a number of past approaches, including
\begin{inparaenum}[(a)]
\item canonical abstraction~\cite{TOPLAS:SRW02};
\item a prior method for applying predicate abstraction to linked lists~\cite{VMCAI:MYRS05};
\item an abstraction method based on ``must-paths'' between nodes that
  are either pointed to by variables or are list-merge points~\cite{CAV:LIS06}; and
\item domains based on separation logic's list-segment
  primitive~\cite{TACAS:DOY06,CAV:BCCDOWY07}
  (i.e., ``$\textrm{ls}[x,y]$'' asserts the existence of a possibly empty list
  segment running from the node pointed to by $x$ to the node pointed to by $y$).
\end{inparaenum}
Decision procedures have been used in previous work to compute the
best transformer for individual statements that manipulate linked
lists~\cite{TACAS:YRS04,Podelski:2010}.

% Maybe it is a hint that this is a good place to pay homage to UIUC work and Reachability predicates. This also applies to the comment above: our related work section seems not to cover as much as the reviewer desires.
% The usual suspects are: Madhusudan’s work, Shaz/Lahiri, Thomas Wies, (Andrey Rybalchenko), the separation logic gang: Smallfoot, Honseok Yang, the Facebook group, NUS (two groups, Joxan Jaffar on heap logic and Wei-Ngan Chin on separation logic), Jhala & McMillan & Henzinger on arrays/separation logic.

%\notepr[SH]{does this make sense?}
%our use of a predicate-abstraction domain allows us to obtain an
%approximate pre-image as the unsat core of a single
%call to an SMT solver.

\sharon{Discuss about Petri nets: Incremental inductive coverability}

\Omit{
\subsubsubsection{Reasoning about linked-lists}

In this paper, our abstract domain is based on formulae expressed
in $\AF$, which has very limited capabilities to express properties
of stretches of data structures that are not pointed to by a program
variable.
This feature is similar to the self-imposed limitations on
expressibility used in a number of past approaches, including
(a) canonical abstraction \cite{TOPLAS:SRW02};
(b) a prior method for applying predicate abstraction to linked lists
\cite{VMCAI:MYRS05};
(c) an abstraction method based on ``must-paths'' between nodes that
are either pointed to by variables or are list-merge points
\cite{CAV:LIS06}; and
(d) domains based on separation logic's list-segment primitive
\cite{TACAS:DOY06,CAV:BCCDOWY07} (i.e., ``$\textrm{ls}[x,y]$'' asserts
the existence of a possibly empty list segment running from the node
pointed to by $x$ to the node pointed to by $y$).
Decision procedures have been used in previous work to compute the
best transformer for individual statements that manipulate linked
lists~\cite{TACAS:YRS04,POPL:PodelskiW10}.

\subsubsubsection{Abstractions.}
A number of abstraction techniques have been developed to verify invariants, including
for array based systems where inductive invariants most typically require universal quantification.
Predicate abstraction provides a means to encode a controlled vocabulary of universally quantified
formulae, and in the IC3 setting predicate abstraction was used in
~\cite{DBLP:conf/cav/BirgmeierBW14},~\cite{DBLP:conf/tacas/CimattiGMT14} and~\cite{DBLP:conf/cav/ItzhakyBRST14}.
In the last case for shape analysis.

\subsubsubsection{IC3/PDR.}
}

\revUpdate{
%\textbf{DAEMON START HERE}
% There is a great deal of literature pertaining to logics used for heap verification.
% Below, we mention some closely related works.
%\begin{paragraph}{Other decidable fragments of logic}
Several logics used for heap verification have decision procedures obtained by reduction to traditional logics.
STRAND~\cite{Madhusudan:SAS2011,Madhusudan:POPL2011}   has the ability to reason about heap data structures and the data they contain, using
MSO-defined relations over trees to describe heaps.
It has a decidable fragment inspired by EPR where universally quantified variables can be used only in so-called \emph{elastic relation}. Essentially, elasticity is a generalization of transitive closure.
STRAND is similar to the earlier PALE~\cite{PLDI:MS01}, which also translates reachability
properties to MSO, and is based on \emph{graph types}~\cite{Cook:1975}.
STRAND does not have an invariant inference mechanism, thus it can be interesting to use STRAND as the logic $\logic$ in $\UPDR$.
The logic CSL~\cite{DBLP:conf/concur/BouajjaniDES09} has a similar flavor to STRAND, with similar sort restrictions on the syntax, but generalizes to handle doubly linked lists, and allows size constraints on structures. Its decidability is obtained by proving a small model property and via a reduction to first order logic.

A logic for reasoning about (cyclic) singly-linked lists is proposed by~\cite{Rakamaric:2007}. 
The logic contains transitive closure of a single link function symbol, and has a decision procedure based on custom inference rules.
% , and can reason about singly-linked
% lists, both acyclic and cyclic (but not doubly-linked lists or trees).
% Its main drawback is that
% abstraction predicates have to be provided manually.
%
A logic for reasoning about list segments and data is given in \cite{Lahiri:2008}.
The logic, LISBQ, provides a ternary primitive $\cdot\to\cdot\to\cdot$ that corresponds to heap paths
through \emph{three} nodes. This allows reasoning about pointer cycles.
The decision procedure is based on a custom proof system, and its termination relies on
stratification of the sorts --- a semantic property of the formulae.
A previous work by the same authors \cite{Lahiri:2006} proposes a translation to first-order logic,
but requires manual instantiations of quantifiers.
%\end{paragraph}


%\begin{paragraph}{Separation logic and its derivatives}
Separation logic~\cite{LICS:Reynolds02} uses inductive predicates conjoined with the $*$-operator to describe unbounded heaps.
It has been used as a basis for static shape analyses, e.g.,~\cite{TACAS:DOY06}, and also has some decidable fragments, e.g.,~\cite{Berdine:2004}.
The latter allows to describe heaps %as for predicates describing linked list segments which allows to describe heaps similar to the ones we can represent in $\EAR$,
as a collection of separated list segments between program variables or existentially quantified ones. Our logic uses similar segments, however it does not express separation explicitly, but rather as a collection of properties that hold at all the heaps.
TREX~\cite{Wies2011} and GRIT~\cite{Piskac2014,Piskac:GRASSHOPER2014} are essentially similar decidable fragments of separation logic.
The former has a decision procedure based on reduction to first order logic, and the latter was recently shown to be reducible to EPR.
GRIT also allows to reason on numerical data stored in heap cells.
%\end{paragraph}


%\begin{paragraph}{Abstraction-based}
% An approach based on abstract interpretation is shown by~\cite{Rakamaric:2007}. Their logic contains
% transitive closure of a single link function symbol.
% Like LISBQ, it is using a decision procedure based on custom inference rules, and can reason about singly-linked
% lists, both acyclic and cyclic (but not doubly-linked lists or trees). Its main drawback is that
% abstraction predicates have to be provided manually.
% An approach that also employs CEGAR, and therefore can infer predicates, is shown by~\cite{Podelski:2010}.
% They go beyond the usual state predicates by introducing unary predicates that range over heap
% locations. This allows to infer quantified properties and permits lazy refinement to improve
% performance, but does not provide completeness guarantees.
% \end{paragraph}

%\smallskip

% Of the above-mentioned, those that use abstract interpretation admit
% automatic discovery of loop invariants by fixed-point computation.
% When predicates are given manually, the domain has finite height, albeit exponential,
% therefore termination is guaranteed. When using CEGAR, there is generally
% no bound and the analysis may diverge. Of the other two categories,
% we know only of~\cite{TACAS:DOY06} to offer invariant inference,
% by also computing a fixed point similarly to abstract interpretation.
% As mentioned above, decidable fragments are natural candidates for
% IC3, provided that they exhibit a small model property as described
% in this paper.

% %\cite{Rakamaric:2007}
%
% % \cite{Moller:2001} -- added in the same paragraph as STRAND
% \cite{monamanual2001} -- why was this even here??
%
% \cite{Chu:PLDI15}
%
% \cite{Chin:2012}
%
% %\cite{Bouajjani2009}
%
% %\cite{Podelski:2010}
}


%  a generalization  where formulae has an EPR-like form,
% essentially, properties of nodes not pointed to
% ,
% STRAND is a prominent example for heap verification logics based on the use of ~\cite{}.
% is an example for a logic which allows to reason about heap shapes and data
%
% Most other known decidable logics for reasoning about trees rely on monadic second- order logic (MSOL) [22, 37]. However, the high complexity of MSOL over trees lim- its its usefulness in verification. There exist some other expressive logics that support reachability in trees with lower complexity [8,13,17,41]. All these logics are still at least in EXPTIME, and their decision procedures are given in terms of automata-theoretic techniques, tableaux procedures, or small model properties. These can be difficult to combine efficiently with SMT solvers. One exception is the STRAND logic [26], which combines MSOL over trees with a data logic. There exists an implementation of a deci- sion procedure for a decidable fragment of STRAND, w
%
%
% provides a description of the entire heap using reachability formulae involving similar variables.
%
%     in a more local a kind of reachability or transitive closure). Decidability is first proved by semantic means: by showing a small model property that bounds the size of potential countermodels that must be checked. We then provide a complete proof system for the fragment, the termination of which furnishes a second decision procedure.


% \todo{I think this mostly repeats what was written earlier.}
% {
%\color{gray}
% In this paper, our abstract domain is based on formulae expressed
% in $\AF$, which has very limited capabilities to express properties
% of stretches of data structures that are not pointed to by a program
% variable.
% This feature is similar to the self-imposed limitations on
% expressibility used in a number of past approaches, including
% (a) canonical abstraction \cite{TOPLAS:SRW02};
% (b) a prior method for applying predicate abstraction to linked lists
% \cite{VMCAI:MYRS05};
% (c) an abstraction method based on ``must-paths'' between nodes that
% are either pointed to by variables or are list-merge points
% \cite{CAV:LIS06}; and
% (d) domains based on separation logic's list-segment primitive
% \cite{TACAS:DOY06,CAV:BCCDOWY07} (i.e., ``$\textrm{ls}[x,y]$'' asserts
% the existence of a possibly empty list segment running from the node
% pointed to by $x$ to the node pointed to by $y$).
% Decision procedures have been used in previous work to compute the
% best transformer for individual statements that manipulate linked
% lists~\cite{TACAS:YRS04,Podelski:2010}.
%}

% Back to the future
% TREX
% Jahob - STRAND / MSOL
% Separation logic

 


%Perhaps we can say that other works that handle linked lists use specialized logics.
% The logic used by our implementation
% has limited capabilities to express properties  of list segments
% that are not pointed to by  variables~\cite{CAV14:Itzhaky}.
% This is similar to the self-imposed limitations on
% expressibility used in a number of shape analysis algorithms~\cite{TOPLAS:SRW02,VMCAI:MYRS05,CAV:LIS06,TACAS:DOY06,CAV:BCCDOWY07,TACAS:YRS04,Podelski:2010}.
% Past experience, as well as our own,
% has shown that despite these limitations it is still possible
% to successfully analyze a rich set of programs.

%}%
%
% \papercomment{
% \vspacex{-1.0ex}
% The literature on program analysis is vast, and the subject
% of shape analysis alone has an extensive literature.
% Thus, in this section we are only able to touch on a few pieces of
% prior work that relate to the ideas used in this paper.
%
% \subsubsubsection{Predicate abstraction.}
% Houdini \cite{FME:FL01} is the first algorithm of which we are aware
% that aims to identify a loop invariant, given a set of predicates as
% candidate ingredients.
% However, Houdini only infers \emph{conjunctive} invariants from a
% given set of predicates.
% Santini \cite{ENTCS:TLLR13,UW-TR-1790:TLLR13} is a recent algorithm
% for discovering invariants expressed in terms of a set of candidate
% predicates.
% Like our algorithm, Santini is based on full predicate abstraction
% (i.e., it uses arbitrary Boolean combinations of a set of predicates),
% and thus is strictly more powerful than Houdini.
% Santini could make use of the predicates and abstract domain described
% in this paper;
% however, unlike our algorithm, Santini would not be able to report
% counterexamples when verification fails.
% Other work infers quantified
% invariants~\cite{PLDI:SrivastavaG09,DBLP:conf/tacas/HoderKV11} but
% does not support the reporting of counterexamples.
% Templates are used in many tools to define the abstract domains used
% to represent sets of states, by fixing the form of the constraints
% permitted.
% Template Constraint Matrices \cite{DBLP:conf/vmcai/SankaranarayananSM05}
% are based on inequalities in linear real arithmetic (i.e., polyhedra),
% but leave the linear coefficients as symbolic inputs to the analysis.
% The values of the coefficients are derived in the course of running
% the analysis.
% In comparison, a coefficient in our use of $\EAR$ corresponds to one
% of the finitely many constants that appear in the program, and we
% instantiated our templates prior to using PDR.
%
% As mentioned in \secref{Introduction}, PDR meshes well with full predicate
% abstraction: in effect, the analysis obtains the benefit of the
% precision of the abstract transformers for full predicate abstraction,
% without ever constructing the abstract transformers explicitly.
% PDR also allows a predicate-abstraction-based tool to create
% concrete counterexamples when verification fails.
%
% \Omit{
%
% Compared to past work, the advantages of the approach described in
% this paper are three-fold:
% (i)~the method is property-directed,
% (ii)~it precisely models the loop body and does not lose precision
% when it combines several statements,
% (iii)~it is capable of reporting concrete counterexamples when
% verification fails.
% More work is needed to make the method comparable in speed to
% the approaches mentioned above.}
%
% \subsubsubsection{STRAND and elastic quantified data automata.}
% Recently, Garg et al.\ developed methods for obtaining
% quantified invariants for programs that manipulate linked lists
% via an abstract domain of \emph{quantified data automata}
% \cite{CAV:GLMN13,SAS:GMP13}.
% \Omit{
% In addition to heap properties, their methods are designed to
% recover invariants about numeric values stored in a list's elements.
% In essence, the automata are equipped with multiple registers
% that read numeric values from an input word (or tree) that encodes a
% heap configuration.
% The acceptance condition of an automaton's final state requires that
% the numeric state must satisfy a given formula.
% }%end Omit
% To create an abstract domain with the right properties, they use a
% weakened form of automaton---so-called \emph{elastic} quantified data
% automata---that is unable to observe the details of stretches of data
% structures that are not pointed to by a program variable.
% (Thus, an elastic automaton has some of the characteristics
% of the work based on linked-list segments described above.)
% An elastic automaton can be converted to a formula in the
% decidable fragment of $\STRAND$ over lists \cite{SAS:MQ11}.
% }
% %%%%
% % Uncomment the following if we get rid of conclusions.tex
% %%%%
% % The simplified PDR algorithm given in
% % \secref{PropertyDirectedReachability} (\algref{PDR} and
% % \algref{Refine}) is really an analysis \emph{framework} that is
% % parameterized on an abstract domain and an associated decidable logic.
% % In this paper, we have used the predicate-abstraction domain defined
% % by \tabref{Pred} and \tabref{PredFormulae} and the logic $\AF$
% % \cite{CAV:IBINS13}.
% % However, the abstract domain of elastic quantified data automata, together
% % with the decidable fragment of $\STRAND$ over lists, represents an
% % alternative abstract-domain/decidable-logic pair that could be used to
% % replace predicate abstraction and $\AF$ in \algref{PDR} and
% \algref{Refine}.
% One advantage that this replacement instantiation would have is
% that elastic quantified data automata are equipped with a numeric
% abstract domain that abstracts the data state, and thus it would be
% possible to obtain invariants that incorporate properties of a list's
% data values.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
