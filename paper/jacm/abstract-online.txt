Property-Directed Inference of Universal Invariants or Proving Their Absence

We present <i>Universal Property Directed Reachability</i> (<b>PDR</b><sup>∀</sup>), a property-directed semi-algorithm for automatic inference of invariants in a universal fragment of first-order logic. <b>PDR</b><sup>∀</sup> is an extension of Bradley's <b>PDR</b>/<b>IC3</b> algorithm for inference of propositional invariants. <b>PDR</b><sup>∀</sup> terminates when it either discovers a concrete counterexample, infers an inductive universal invariant strong enough to establish the desired safety property, or finds a <i>proof that such an invariant does not exist</i>. <b>PDR</b><sup>∀</sup> is not guaranteed to terminate. However, we prove that under certain conditions, e.g., when reasoning about programs manipulating singly-linked lists, it does.

We implemented an analyzer based on <b>PDR</b><sup>∀</sup>, and applied it to a collection of list-manipulating programs. Our analyzer was able to automatically infer universal invariants strong enough to establish memory safety and certain functional correctness properties, show the absence of such invariants for certain natural programs and specifications, and detect bugs. All this, without the need for user-supplied abstraction predicates.

Rajeev Alur
CAV 2015
