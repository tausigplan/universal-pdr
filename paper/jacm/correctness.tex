%!TEX root = ./paper.tex

\section{Correctness}\label{Se:Correctness}
In this section we formalize the correctness guarantees of $\UPDR$.
We start by formalizing the invariants of the sequence of frames maintained by $\UPDR$.
We then prove that the output of $\UPDR$ is correct.

\subsection{Properties of the Frames Computed by $\UPDR$} \label{sec:frame-properties}

The following lemma summarizes the invariants of the sequence of frames computed by $\UPDR$.
It follows from a simple induction on the steps of $\UPDR$. These invariants are adopted from traditional $\PDR$.

\begin{lemma} \label[lemma]{lem:frames-ars}
Let $\TS = (\Init, \Trans)$ be a transition system and $\Prop$ a safety
property. For every $N \geq 1$, the sequence $\langle F_0,
F_1,\ldots,F_{N}\rangle$ obtained by $\UPDR$ right before $N$
is increased in \cref{Li:UPDR:incN}, is an approximate reachability sequence
for $\TS$ and $\Prop$ (Def.~\ref{def:ars}). Further, in every other step of the
while loop (\Cref{Li:UPDR:beginWhile}), the sequence $\langle F_0,
F_1,\ldots,F_{N}\rangle$ satisfies all the requirements of \Cref{def:ars},
except for, possibly, requirement~(\ref{item:bad}) for $i = N$.
\end{lemma}

In order to provide a slightly tighter characterization of the frames computed by $\UPDR$,
which, unlike \Cref{lem:frames-ars}, is specific to $\UPDR$, we need the following definition.
\begin{definition}[Relaxed Traces]
Given a transition system $\TS = (\Init, \Trans)$, we say that a sequence
of states $\state_0,\ldots,\state_n$ is a \emph{relaxed trace} of $\TS$ if for every $0\leq i\leq n-1$,
there exists $\state_{i+1}'$ such that $(\state_i,\state_{i+1}') \models \Trans$
and $\state_{i+1}' \models (\kDiag(\state_{i+1}))'$.
%and $\state_{i+1}' \models \kDiag(\state_{i+1})$.
\end{definition}
%
For a transition system $\TS$ and a property $\Prop$, with $\mbad = \neg \Prop$,
we denote by $\backR_i$ the set of states that can reach a bad state in at most $i$ steps via a relaxed trace.
That is, $\state \in \backR_i$ if and only if there exists a relaxed trace $\state_0,\ldots,\state_n$
such that $\state_0 = \state$, $\state_n \models \mbad$, and $n \leq i$.
In particular, $\backR_0 = \{\state \mid \state \models \mbad\}$.
We denote by $\backR$ the set of states that can reach a bad state in
some number of steps via a relaxed trace, i.e.  $\backR = \bigcup_{i \geq 0} \backR_i$.
%We say that $\backR$ is \emph{effective} if there exists $k$ such that $\backR = \backR_k$, and
%in addition $\backR$ can be described by an existential formula. That is, there exists an existential formula $\psi_{\backR}$ such that
%$\backR = \{ \state \mid \state \models \psi_{\backR}\}$.


The following lemma states that the frames computed by $\UPDR$ do not contain
states that lead to bad states via relaxed traces, where the length of the considered traces
depends on the frame index.
\begin{lemma} \label[lemma]{lem:frame-disjoint-from-backr}
Let $\TS = (\Init, \Trans)$ be a transition system and $\Prop$ a safety
property. For every $N \geq 1$ and $0 \leq j \leq N-1$, if $\cmodel \models F_j$ then $\cmodel \not\in \backR_{N-1-j}$.
\end{lemma}
That is, $\aframe{j}$ does not include any state that reaches a bad state via a relaxed trace in $N-1-j$ steps or less.
Note that this lemma holds in particular when $N$ is increased to $N+1$ (\cref{Li:UPDR:incN}), in which case
it implies that for every $0 \leq j \leq N$,
%if $\cmodel \in \backR_{N-j}$ then $\cmodel \not\models F_j$.
if $\cmodel \models F_j$ then $\cmodel \not\in \backR_{N-j}$.
The proof follows a simple induction on $j$ that shows that if $\cmodel \in \backR_{N-1-j}$,
then it is excluded from $F_j$.


On the other hand, it is easy to prove by induction that every state that
$\UPDR$ attempts to block in frame $\aframe{j}$ is a state in $\backR$:
\begin{lemma} \label[lemma]{lem:block-in-backr}
Let $\TS = (\Init, \Trans)$ be a transition system and $\Prop$ a safety
property. For every $N \geq 1$ and $0 \leq j \leq N$, if $\block(j,\cmodel)$ is called, then
$\cmodel \in \backR_{N-j}$.
\end{lemma}

We can now return to the proof of \Cref{lem:blocked-before}.
\begin{proof}[of \Cref{lem:blocked-before}]
Suppose $\cmodel \models \aframe{j}$ is discovered in the backward traversal
from $\aframe{N}$. By Lemma~\ref{lem:block-in-backr}, $\cmodel \in \backR_{N-j}$. Therefore,
by Lemma~\ref{lem:frame-disjoint-from-backr}, $\cmodel \not\models \aframe{j-1}$.
To complete the proof, recall that we consider $j >1$. Therefore $j-1 >0$, hence $\aframe{j-1}$ is a
universal formula. Therefore, by \Cref{lem:diagram}, $\kDiag(\cmodel) \implies \neg \aframe{j-1}$,
or equivalently, $\aframe{j-1} \implies \neg \kDiag(\cmodel)$.
Since $\aframe{i} \implies \aframe{j-1}$ for every $i \leq j-1$, we
conclude that $\aframe{i} \implies \neg \kDiag(\cmodel)$ for every $i \leq j-1$.
\end{proof}
Note that if $\Init$ is a universal formula, then the claim formulated by Lemma~\ref{lem:blocked-before}
holds for $j=1$ as well. That is, when $\block(1,\cmodel)$ is called,
$\aframe{0} \implies \neg \kDiag(\cmodel)$.
In this case, the additional check of $(j=1 \land \SAT{\varphi \land \minit})$ performed
in \cref{Li:block:shorterCex} of $\block (j, \cmodel)$ is always false, and hence not needed.



\subsection{Correctness of the Outcome of $\UPDR$}
We recall that if $\UPDR$ terminates it reports that either the program is safe, the program is not safe, providing a counterexample, or the program cannot be verified using a universal inductive invariant.
\begin{lemma}\label[lemma]{Le:Soundness}
Let $\TS = (\Init, \Trans)$ be a transition system and let $\Prop$ be a safety property.
If $\UPDR$ returns $\validKw$ then $\TS$ satisfies $\Prop$.
Further, if $\UPDR$ returns a counterexample, then $\TS$ does not satisfy $\Prop$.
\end{lemma}
\ifproofsA
\begin{proof}
$\UPDR$ returns $\validKw$ if there exists $i$ such that $F_{i+1}\implies F_i$.
Therefore, $F_i \wedge \Trans \implies (F_{i+1})' \implies (F_i)'$. Recall that,
by the properties of an approximate reachability sequence, $\Init \implies F_0 \implies F_i$ and $F_i \implies \Prop$.
Therefore, $F_i$ is an inductive invariant, which ensures that $\TS$
satisfies $\Prop$.
The second part of the claim follows immediately from the definition of a counterexample.
%
% \Omit{
% $\UPDR$ returns  \code{Valid}\  if there exists $i$ such that $F_{i+1}\implies F_i$.
% Therefore, $F_i \wedge \Trans \implies (F_{i+1})' \implies (F_i)'$. By induction
% this proves that for every $j \geq i$, the set $\Reach_j$ of states reachable
% from $\Init$ in $j$ steps is a subset of the set of states that satisfy
% $F_i$. This means that the set $\Reach$ of states reachable from $\Init$ in
% any number of steps is a subset of the states that satisfy $F_i$. Since $F_i
% \implies \neg \Bad$, we conclude that every state in $\Reach$ satisfies $\neg
% \Bad$.}
\qed
\end{proof}
Note that the proof of \Cref{Le:Soundness} also implies that when $\UPDR$ determines that
$\TS$ satisfies $\Prop$, it also obtains a \emph{universal} inductive invariant. Namely,
the frame $F_i$ in which a fixpoint was identified comprises such an invariant.
(Recall that each frame is a universal formula as it is obtained as a conjunction of negated diagrams.)
\fi
%
% \sharon{need to rewrite this lemma to capture the bug fix: now it refers only to cex of length exactly $N$.
% Possibly: remove $\langle \phi_0 \til \phi_N\rangle$ from the proposition, and in the proof use $\langle \phi_j \til \phi_N\rangle$ to indicate that the
% cex can be shorter than $N$}
\begin{proposition}\label[proposition]{Pr:AbstracCEX}
Let $\TS = (\Init, \Trans)$ be a transition system and let $\Prop$ be a
safety property. If $\UPDR$ obtains a spurious counterexample $\langle \phi_j%0
\til \phi_N\rangle$
then there exists no universal safety inductive invariant $\IndInv$ for $\TS$ and $\Prop$
over the given vocabulary.
\end{proposition}
%
In order to prove \Cref{Pr:AbstracCEX}, we first show that if a universal inductive invariant exists
for $\TS$ and $\Prop$, 
then no state that reaches a bad state via a relaxed trace satisfies the invariant:
%
\begin{proposition}
\label[proposition]{Le:BadInv}
Let $\TS = (\Init, \Trans)$ be a transition system and $\Prop$ a safety
property, and let $\backR$ be the set of states that
reach a bad state via a relaxed trace (see \Cref{sec:frame-properties}). 
Let $\IndInv$ be a universal inductive safety invariant for $\Prop$.
For any $\cmodel \in \backR$, we have $\cmodel \models \neg\IndInv$.
%(and hence $\cmodel \not\models \Init$).
%For every $N \geq 1$ and $0 \leq j \leq N-1$, if $\cmodel \models
%F_j$ then $\cmodel \not\in \backR_{N-1-j}$.
\end{proposition}
%
\begin{proof}
We show by induction on $i$ that
$\cmodel \in \backR_i$ implies
$\cmodel \models \neg\IndInv$.
For the base case, $\cmodel \in \backR_0$, we have $\cmodel \models \Bad$.
Since $\Bad = \neg \Prop$ and $\IndInv \implies \Prop$,
we conclude that $\cmodel \models \neg\IndInv$.

For the inductive step, let $\cmodel \in \backR_{i+1}$.
By definition of $\backR_{i+1}$, there exist models
$\cmodel_i$ and $\cmodel_i'$ such that
%$(\cmodel,\cmodel_i') \models \Trans$,
%$\cmodel_i' \models (\Diag(\cmodel_i))'$,
$(\cmodel,\cmodel_i') \models \Trans \land (\Diag(\cmodel_i))'$.
and
$\cmodel_i \in \backR_i$.
%The first two conditions imply that
%$(\cmodel,\cmodel_i') \models \Trans \land (\Diag(\cmodel_i))'$.
Moreover, by the induction hypothesis, $\cmodel_i \models \neg \IndInv$.
Since $\neg \IndInv$ is an existential formula, this means by
\Cref{lem:diagram} that $\Diag(\cmodel_{i}) \implies \neg \IndInv$.
We conclude that
$\Trans \land (\Diag(\cmodel_{i+1}))' \implies
 \Trans \land (\neg\IndInv)'$.
Therefore, $(\cmodel,\cmodel_i')$ is also a model of the formula
$\Trans \land (\neg\IndInv)'$.

If we assume that $\cmodel \models \IndInv$, we would get that
$\IndInv \wedge \Trans \wedge (\neg\IndInv)'$ is satisfiable,
in contradiction $\IndInv$ being inductive.
Hence, $\cmodel \models \neg \IndInv$.
\end{proof}
%
\ifproofsB
\begin{proof}[of \Cref{Pr:AbstracCEX}]
Assume that there exists a universal safety inductive invariant $\IndInv$ over
$\vocabulary$.
By \Cref{lem:block-in-backr}, for every state $\cmodel_i$ generated by
$\UPDR$ at frame $F_i$, $\cmodel_i\in \backR$.
Hence by \Cref{Le:BadInv}, $\cmodel_i\models \neg\IndInv$.
This implies, by \Cref{lem:diagram}, that every diagram $\phi_i$ generated by $\UPDR$ at frame $F_i$ is such that
$\phi_i \implies \neg \IndInv$, and hence $\phi_i \implies \neg \Init$.
(Recall that by definition $\Init \implies \IndInv$, i.e.,
$\neg \IndInv \implies \neg \Init$).
This contradicts the existence of a spurious counterexample, where
$\phi_j \land \Init$ is satisfiable.
\end{proof}
% \begin{proof}
% Assume that there exists a universal safety inductive invariant $\IndInv$ over
% $\vocabulary$. We show by induction on the distance
% %$i=0 \til N$, of $F_{N-i}$ from $F_N$
% $N-i=0 \til N$, of $F_{i}$ from $F_N$
% that every state $\cmodel_i$ generated by $\UPDR$ at frame $F_i$ is such that
% $\cmodel_i \models \neg \IndInv$.
% %Because $F_0 \equiv  \Init$ and by definition $\Init \implies \IndInv$,
% This implies, by \Cref{lem:diagram}, that every diagram $\phi_i$ generated by $\UPDR$ at frame $F_i$ is such that
% $\phi_i \implies \neg \IndInv$, and hence $\phi_i \implies \neg \Init$.
% (Recall that by definition $\Init \implies \IndInv$, i.e., $\neg \IndInv \implies \neg \Init$). This contradicts
% the existence of a spurious counterexample, where $\phi_j \land \Init$ is satisfiable.
%
% The base case of the induction pertains to $\aframe{N}$.
% It follows immediately from the property that a
% state $\cmodel_N$ generated at frame $F_N$ is a model of the formula $F_N
% \land \Bad$, and in particular is a model of $\Bad = \neg \Prop$, i.e.,
% $\cmodel_N \models \neg\Prop$. Since $\IndInv \implies \Prop$, or equivalently
% $\neg \Prop \implies \neg \IndInv$, we conclude that $\cmodel_N \models \neg
% \IndInv$.
%
% Consider a state generated at frame $F_i$.
% Then %$\cmodel_i = \reduct{\cmodel}{\vocabulary}$ is the reduct of a model of
% %the formula $F_i \wedge \Trans \wedge (\Diag(\cmodel_{i+1}))'$
% $\cmodel_i$ is the reduct of a model of
% the formula $F_i \wedge \Trans \wedge (\Diag(\cmodel_{i+1}))'$ to $\vocabulary$.
% %$((\Init)' \lor (F_i \wedge \Trans)) \wedge (\Diag(\cmodel_{i+1}))'$.
% Moreover, by the induction hypothesis, $\cmodel_{i+1} \models \neg \IndInv$.
% Since $\neg \IndInv$ is an existential formula, this means by
% \Cref{lem:diagram} that $\Diag(\cmodel_{i+1}) \implies \neg \IndInv$.
% %Since $\Init \implies \IndInv$, we conclude that $\cmodel_i$ is (a reduct of) a
% %model of $F_i \wedge \Trans \wedge (\Diag(\cmodel_{i+1}))'$.
% We conclude that
% $F_i \land \Trans \land (\Diag(\cmodel_{i+1}))' \implies
%  F_i \land \Trans \land  (\neg\IndInv)'$.
% Therefore, $\cmodel_{i}$ is also (a reduct of) a model of the formula
% $F_i  \land \Trans \land (\neg\IndInv)'$.
% If we assume that $\cmodel_i \models \IndInv$, we would get that
% $\IndInv \wedge \Trans \wedge (\neg\IndInv)'$ is satisfiable, in contradiction
%  $\IndInv$ being inductive.
% Hence, $\cmodel_i \models \neg \IndInv$.
% \qed
%\end{proof}
\else
\begin{proof}[sketch]
Assume that there exists a universal inductive invariant $\IndInv$.
% over $\vocabulary$.
Based on \Cref{Def:Invariant} and \Cref{lem:diagram},
it can be shown by induction on the distance %$i=0 \til N$, of $F_{N-i}$ from $F_N$
$N-i=0 \til N$, of $F_{i}$ from $F_N$
that
every state $\cmodel_i$ generated by $\UPDR$ at frame $F_i$ is such that
$\cmodel_i \models \neg \IndInv$, hence
every diagram $\phi_i$ generated at $F_i$ is such that
$\phi_i \implies \neg \IndInv$, and therefore $\phi_i \implies \neg \Init$.
(Recall that by definition $\Init \implies \IndInv$, i.e., $\neg \IndInv \implies \neg \Init$). This contradicts
the existence of a spurious counterexample, where $\phi_j \land \Init$ is satisfiable.
\qed
\end{proof}
\fi
%
% Partial models
%
% \begin{remark} \notepr[SH]{if partial models is incorrect, remove this}
% The description of $\UPDR$ as well as the proof above assume that every model
% extracted for some formula $\phi$ over $\vocabulary$ is complete (i.e.,
% provides an interpretation for all constant and relation symbols of
% $\vocabulary$).
% However, it is typically sufficient to provide a \emph{partial} model that
% interprets only the symbols that appear in $\phi$ (possibly leaving some symbols in $\vocabulary$ uninterpreted).
% If a partial model $\cmodel$ describing a bad or adverse state is obtained, correctness is maintained even if the diagram $\Diag(\cmodel)$ encodes only the interpreted
% symbols in $\cmodel$.
% This results from the property that the interpreted symbols suffice to satisfy
% $\phi$, and the uninterpreted symbols do not affect the satisfaction of
% $\phi$.
% \end{remark}
\begin{example}\label[example]{Ex:SharedTail}
%\sharon{the edges are messed up. We want some of the nodes to be equal to null, not pointing to it}
Procedure \code{traverseTwo()}, presented in Figure~\ref{Fi:SharedTail} together with its pre- and post-condition,
traverses two lists until it finds their last elements.
%\code{g}\ and \code{h} using pointers \code{p,i}\ and \code{g,j}, respectively,
If the lists have a shared tail then \code{p}\ and \code{q}\ should point to the same element when the traversal terminates.
The program indeed satisfies this property.
However, this cannot be proven correct using an inductive universal invariant:
Take, as usual, $\minit$ to be the procedure's precondition and
$\Prop$ to be the safety property whose negation is
%$\mbad$
%to be $\Prop = (\mnull \land j = \mnull) \to \mathit{post}$, where $\emph{post}$ is the procedure's postcondition.
$\mbad = (i = \mnull \land j = \mnull) \land \neg \Post$, where $\Post$ is the procedure's postcondition.
Consider the state $\cmodel_0$ depicted in Figure~\ref{Fi:SharedTail:models}.
Clearly, this model satisfies $\Init$.
Therefore, if $\IndInv$ exists, $\cmodel_0 \models \IndInv$.
$\cmodel_0$ is a predecessor of $\cmodel^t_1$ and hence it should be the case that $\cmodel^t_1 \models \IndInv$.
Now consider  $\cmodel_1$, which is a submodel of $\cmodel^t_1$ and interprets all constants as in
$\cmodel_1$. If $\IndInv$ is universal, then $\cmodel_1 \models \IndInv$ as
well.
The model $\cmodel_2$ is a successor of $\cmodel_1$ and hence
$\cmodel_2 \models \IndInv$.
However, $\cmodel_2 \not\models \Prop$, in contradiction to the
property of a safety invariant.
Indeed, when using {\UPDR}, the spurious counterexample
$\B{\cmodel_0,\cmodel_1,\cmodel_2}$ presented in
\Cref{Fi:SharedTail:models} is obtained.
This indicates that no universal invariant for $\Prop$ exists.
Note that state  $\cmodel_1$ is a predecessor of $\cmodel_2$ and recall that
$\cmodel_0$ is a predecessor of $\cmodel^t_1$.
The spurious counterexample was obtained because $\cmodel^t_1$
satisfies the diagram of state $\cmodel_1$.
\end{example}

\input{shared-tail}

%\begin{figure}[t]
% \centering
% \begin{minipage}{1.2\textwidth}
% \begin{lstlisting}[mathescape]
% $\fname{pre}\colon p = \mnull \land q = \mnull \land i = g \land g \neq \mnull \land j = h \land h \neq \mnull \land \mbox{}$
%           $\exists v. n^*(g,v) \land n^*(h,v) \land v \neq \mnull$
% $\fname{post}\colon p = q \land p \neq \mnull \land  i = \mnull \land j = \mnull$
% void traverseTwo(List g, List h) {
%     while (i $\neq$ null $\lor$ j $\neq$ null){
%         if i $\neq$ null then p := i; i := i.n;
%         if j $\neq$ null then q := j; j := j.n;
%     }
% }
% \end{lstlisting}
% \end{minipage}
% %\vspace{-3ex}\vspaceall\caption{\label{Fi:SharedTail}%
%   A procedure that finds the last elements of two non empty acyclic lists.
% }}
% \end{figure}
% %
%
% %\def\arraystretch{1.5}%  1 is the default, change whatever you need
% \begin{figure}[t]
% \centering
% \small
% \resizebox{1.0\textwidth}{!}{
% \begin{tabular}{|c|c|c|c|c|c|c|}
% \hhline{|-|~|-|~|-|~|-|}
% %($\cmodel_0$)
% %&
% \mbox{\xymatrix @-1pc {
% g,i\ar[d] &  \mnull\ar[d] & h,j\ar[d]  \\
% *++[o][F-]{v_g} \ar[d]^{n*}  &  *++[o][F-]{v_0} & *++[o][F-]{v_h}\ar[d]^{n*}\\
% *++[o][F-]{v_1} \ar[r]^{n*} &  *++[o][F-]{v_t}      & *++[o][F-]{v_2}\ar[l]_{n*} \\
% }}
% & &
% \mbox{\xymatrix @-1pc {
% & g,p\ar[d] &  \mnull\ar[d] & h,q\ar[d]  \\
% & *++[o][F-]{v_g} \ar[d]^{n*}  &  *++[o][F-]{v_0} & *++[o][F-]{v_h}\ar[d]^{n*}\\
% i \ar[r] & *++[o][F-]{v_1} \ar[r]^{n*} &  *++[o][F-]{v_t}      & *++[o][F-]{v_2}\ar[l]_{n*} & j\ar[l]\\
% }}
% & &
% % \mbox{\xymatrix @-1pc {
% % g\ar[d] &  \mnull\ar[d] & h\ar[d]  \\
% % *++[o][F-]{v_g} \ar[d]^{n*}  &  *++[o][F-]{v_0} & *++[o][F-]{v_h}\ar[d]^{n*}\\
% % *++[o][F-]{v_1}   & \ar[l]{i~~~~j}\ar[r] & *++[o][F-]{v_2}  \\
% % }}
% % & &
% \mbox{\xymatrix @-1pc {
% g,p\ar[d] &  \mnull\ar[d] & h,q\ar[d]  \\
% *++[o][F-]{v_g} \ar[d]^{n*}  &  *++[o][F-]{v_0} & *++[o][F-]{v_h}\ar[d]^{n*}\\
% *++[o][F-]{v_1}   & \ar[l]{i~~~j}\ar[r] & *++[o][F-]{v_2}  \\
% }}
% & &
% \mbox{\xymatrix @-1pc {
% g\ar[d] &  \mnull\ar[d] & h\ar[d]  \\
% *++[o][F-]{v_g} \ar[d]^{n*}  &  *++[o][F-]{v_0} & *++[o][F-]{v_h}\ar[d]^{n*}\\
% *++[o][F-]{v_1}   & \ar[l]{p~~~i,j~~~q}\ar[u]\ar[r] & *++[o][F-]{v_2}  \\
% }}
% \\
% $(\cmodel_0)$ && $(\cmodel^t_1)$ && $(\cmodel_1)$ && $(\cmodel_2)$\\[1pt]
% \hhline{|-|~|-|~|-|~|-|}
% \end{tabular}
% } % end resizebox
% {\small\caption{\label{Fi:SharedTail:models}A spurious counterexample found for procedure \code{traverseTwo()}, shown in \Cref{Fi:SharedTail}.}}
% \end{figure}

% \Omit{
%
% \subsection{Termination}
%
% \sharon{this part requires verification!}
%
% Below we provide a sufficient condition for termination of $\UPDR$.
%
% \begin{definition}
% A formula $\phi$ is \emph{$n$-finitely generated} if there exists
% a finite set $A$ of diagrams over $\vocabulary$ such that $\phi \equiv \bigvee A$.
% A set $S$ of states (models) is \emph{$n$-finitely generated} if there exists
% a finite set $A$ of diagrams over $\vocabulary$, each with at most $n$
% existential quantifiers, such that the set of states that satisfy $\bigvee A$ is exactly $S$.
% %$\Semantics{\bigvee A}{\vocabulary} = S$.
% \end{definition}
%
% \begin{lemma}[Termination] \label{lem:termination}
% Let $\TS$ be a transition system and let $\Bad$ be a formula describing the
% set of bad states. Suppose that \begin{inparaenum}[(i)]
% \item $\Bad$ is $n$-finitely generated, and that \item there exists a
% bound $k \geq n$ such that for every diagram with at most $k$ existential
% quantifiers, the set of predecessors of the states that satisfy the diagram
% %its preimage \sharon{define preimage of diagram/formula}
% is $k$-finitely generated. \end{inparaenum} If, in addition, $\findModelKw(\psi)$ always returns a \emph{minimal}
% model when $\psi$ is satisfiable, then $\UPDR$ terminates.
% \end{lemma}
%
% \begin{proof}
% Under these assumptions, it can be shown by induction on the runtime of
% $\UPDR$ that all the models generated by $\UPDR$ consist of at most $k$
% individuals \sharon{not sure this is correct, since the preimage of a diagram is conjoined with the previous frame.
% This can affect the size of models}, and accordingly the diagrams have at most $k$ existential
% quantifiers. Therefore, the universal clauses added to frames all have at
% most $k$ universal quantifiers. Termination follows since the set of
% universal clauses over a given vocabulary ($\vocabulary$) with at most $k$
% quantifiers is finite (up to variable renaming).
% \qed
% \end{proof}
%
% Note that the formula $\Bad$ is $n$-finitely generated for some $n$ if and only if $\Bad$ is an existential formula.
% As for the second condition of \cref{lem:termination}, if, for example, the transition relation is universal, and the unprimed constants
% are all related to primed constants using equalities, then this condition holds (in fact, in this case,
% the set of predecessors of the states that satisfy a diagram is expressible using a diagram with the same number of quantifiers).
% }

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
