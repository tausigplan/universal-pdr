%!TEX root=./paper.tex

\section{Reasoning about Heap-Manipulating Programs using Effectively Propositional Logic}\label{Se:EPR}

%In this section, we present the $\EPR$ fragment of first-order logic
%and exemplify how we model heap-manipulating programs in $\EPR$.
In this section we exemplify how we represent heap-manipulating programs,
such as the ones used in our running examples and experiments,
as well as the corresponding verification problems in first-order logic.

We start by defining the
fragment of logic used, and continue to describe the construction of the formulae $\rho$, $\minit$ and $\mbad$ for a program.
First, we present the construction of the formulae for programs that manipulate \emph{acyclic} data structures, as developed in~\cite{CAV:IBINS13}.
Next, we develop a novel construction that also handles restricted \emph{cyclic} data structures.


\paragraph{$\EPR$ and $\EAR$} %
Effectively-Propositional logic ($\EPR$), also known as the Bernays-Sch\"onfinkel-Ramsey class,
is a fragment of first-order logic which allows for relational  first-order formulae
with a quantifier prefix of the form $\exists^*\forall^*$, but forbids functional symbols.
Satisfiability of $\EPR$ is decidable.  $\EPR$ enjoys the \emph{small model property}: every
satisfiable formula in $\EPR$ is guaranteed to have a finite model~\cite{LEWIS1980317}.

In our running examples and experiments, we represent programs
and the corresponding verification problems using $\EAR$~\cite{CAV:IBINS13}, an auxiliary logic built on top of $\EPR$,
which enables natural reasoning about programs manipulating linked-data structures.
$\EAR$ extends $\EPR$ by allowing a deterministic transitive-closure
operator $^*$ over acyclic relations.
Satisfiability of $\EAR$ is reducible to that of $\EPR$, and enjoys the same properties.
Technically, the reduction introduces first-order axioms ($\EPR$ formulae)
that provide a complete characterization of $^*$. These axioms are given in \tabref{axioms-linord}.

\begin{table}
\[
\renewcommand\arraystretch{1.3}
\begin{array}{|rc@{~}clr|}\hline
\Tlinord &~\eqdef& &
    \forall \alpha,\beta \ldotp \nrtc\alpha\beta \land \nrtc\beta\alpha \leftrightarrow
                     \alpha = \beta & \text{reflexivity + acyclicity} \\
 && \land & \forall \alpha, \beta, \gamma \ldotp \nrtc\alpha\beta \land \nrtc\beta\gamma \to
                          \nrtc\alpha\gamma & \text{transitivity} \\
 && \land & \forall \alpha, \beta, \gamma \ldotp \nrtc\alpha\beta \land \nrtc\alpha\gamma \to
                          \nrtc\beta\gamma \lor \nrtc\gamma\beta & \text{semi-linearity} \\
\hline
\end{array}
\]
\caption{\tablabel{axioms-linord}%
  Effectively propositional axiomatization of deterministic reflexive-transitive closure.}
\end{table}
%

\para{Programs manipulating linked-data structures as transition systems}
To represent memory states of list manipulating programs,
we fix an infinite countable
%{}\alex{Should we say 'countable'?}
universe $\universe$ whose individuals represent dynamically allocated objects.
Recall that a state is represented by a finite first-order model
$\cmodel = (\domain,\interpretation)$ with
$\domain \subseteq \universe$.
%{}\alex{Where do we say that we consider/allow only finite models as states?}

We use a vocabulary $\vocabulary$ which associates every program variable $\mathtt{x}$ with a constant $x$,
contains a designated constant $\mnull$ to denote the $\NULL$ value,
and contains the special binary predicate symbol $\nrtcSym(\cdot\,,\cdot)$ which defines reachability
over every pointer field \code{n}, e.g., in \Cref{Ex:Split,Ex:Filter}.
Notice that \code{n} itself is not part of the vocabulary,
but it is definable using the (open) formula $\Pnext$ in \Cref{Ta:Wprules}.
We use $\ntc\alpha\beta$ as a shorthand for $\nrtc\alpha\beta \land \alpha \neq \beta$.
Clearly, these definitions for $\Pnext$ and $\ntcSym$ rely on acyclicity of \code{n}:
if there was a cycle, then for every node $u$ on the cycle we would have $\ntc u u$,
and also for any two nodes $u,v$ we would have $\nrtc u v$, so there would not be
enough information in $\nrtcSym$ to define $\Pnext$ based on it. In particular,
the order of the node in the cycle is not encoded in $\nrtcSym$.

In addition, we represent a Boolean function $\isOK$ with a unary predicate
$\isOK(\cdot)$,
and an order relation (e.g. for sorting) with a binary predicate $R(\cdot\,,\cdot)$.

We depict memory states $\cmodel= (\domain, \interpretation)$ as directed
graphs (e.g., \Cref{Fi:diag:models}).
Individuals in $\domain$, representing heap locations, are depicted as circles labeled by their name.
We draw an edge from the name of constant $x$ or a unary predicate $\isOK$ to an individual $v$
if $\cmodel \models x=v$ or $\cmodel \models \isOK(v)$, respectively.
For clarity, we do not directly depict the interpretation of the $n^*$ relation. Instead,
we use a more compact drawing scheme where we draw an $n$-annotated edge between $v$ and $u$ if $\cmodel\models \Pnext(v,u)$.
The interpretation of $n^*$ can be inferred from the $n$-annotated edges by
\begin{inparaenum}[(i)]
\item omitting the incoming edges of the element that corresponds to the $\mnull$ constant, and
\item considering the reflexive transitive closure of the remaining edges.
\end{inparaenum}


%We draw an $n^*$-annotated edge between $v$ and $u$
%if $\cmodel\models n^*(v,u)$.
%For clarity, we do not show edges that can be inferred from the reflexive and transitive
%nature of $n^*$.

\para{Transition relation}
We express the semantics of loop-free code as a transition relation $\Trans$ over the above vocabulary
by defining a \emph{weakest liberal precondition} predicate
transformer, $\wlpSym\semp{-}$, for each command type.
We do this in a simple language IMP$^R$, which is an extension of IMP~\cite{Winskel:1993:FSP}
with heap-related commands.
The rules for $\wlpSym$ are shown in \Cref{Ta:Wprules}.
The notation $\substitute{Q}{x}{t}$
is used to denote substitution of all the occurrences of the constant $x$ in $Q$ with the term $t$.
The notation $\substitute{Q}{\nrtc\alpha\beta}{\phi}$ denotes substitution of any atom of the form
$\nrtc{\cdot\,}{\cdot}$ in $Q$ with the formula $\phi$, where $\alpha$ and $\beta$ may occur as term
placeholders in $\phi$ and are filled in with the arguments of $\nrtcSym$. $\Pvar$ is the set of
(constant symbols pertaining to) variables used in the program.
As shown in~\cite{CAV14:Itzhaky}, the rules for $\wlpSym$ are sound
and complete.
\done{We should explain that the rule for x.n:=y assumes that x.n=null prior, as in TVLA}
\done{Shachar, can you add a footnote explaining the subtle issue of $\wlp$ used for assertions}

\revUpdate{
The encoding of lists using $\nrtcSym$ and the corresponding update rules $\wlpSym$
may seem confounding at first, but follow a fairly simple intuition:
when removing a pointer link, all paths that go through the changed node are disconnected;
when adding a link, all paths into the source get connected to the (single) path from the target.
This is expressed, respectively, by the formulae
\begin{equation*}
  \nrtc\alpha\beta \land (\lnot \nrtc\alpha x \lor \nrtc\beta x)
  \quad
  \text{(for $x.\nextf := \NULL$)}
\end{equation*}
%$\nrtc\alpha\beta \land (\lnot \nrtc\alpha x \lor \nrtc\beta x)$ (for $x.\nextf := \NULL$)
and
\begin{equation*}
  \nrtc\alpha\beta \lor (y\neq\mnull \land \nrtc\alpha x \land \nrtc y\beta)
  \quad
  \text{(for $x.\nextf := y$)} \,.
\end{equation*}
%$\nrtc\alpha\beta \lor (y\neq\mnull \land \nrtc\alpha x \land \nrtc y\beta)$ (for $x.\nextf := y$).
The former describes all $n$-paths except those that go through $x$, and the latter describes
all $n$-paths with the addition of those that were connected by the new edge from $x$ to $y$.
Here, $\alpha$ and $\beta$ denote aribitrary heap locations.
When traversing a pointer ($x := y.\nextf$), the successor can be expressed by its transitive closure
$\nrtcSym$ using the formula $\Pnext$: for any two locations $s$ and $t$ (which are not $\mnull$),
the successor of $s$ is $t$ iff $t$ is on the path starting at $s$ (but not $s$ itself), and
no other node lies between $s$ and $t$. The successor of $s$ is $\mnull$ iff the path starting
at $s$ is empty. Our ability to recover the successor from $\nrtcSym$ is key to having a complete
encoding of heap structures using transitive closure.
}

\begin{table}
\[
\renewcommand\arraystretch{1.3}
\begin{array}{|@{}c@{}|}
 \hline
 \begin{array}{r@{~~~}c@{~~~}l}
 \wlp{\SKIP}Q &\eqdef& Q\\
 \wlp{x := y}Q &\eqdef& \substitute{Q}{x}{y}\\
\wlp{\kCmd_1 ~; ~ \kCmd_2}Q &\eqdef& \wlp{\kCmd_1}{\big(\wlp{\kCmd_2}Q\big)}\\
\wlp{\IF ~ B ~ \THEN~ \kCmd_1  ~ \ELSE~ \kCmd_2}Q &\eqdef&
\semp{B}\land \wlp{\kCmd_1}Q \lor \lnot\semp{B}\land\wlp{\kCmd_2}Q \\
\wlp{\ASSERT ~ B}Q &\eqdef& \semp{B}\land Q \\
\end{array} \\
\hline
\multicolumn{1}{c}{} \\
\hline
%
\begin{array}{r@{~~~}c@{~~~}l}
%\hline
% Destructive Updates
  \wlp{x.\nextf := \NULL}Q &\eqdef&
      \substitute{Q}{\nrtc\alpha\beta}{\nrtc\alpha\beta \land (\lnot \nrtc\alpha x \lor \nrtc\beta x)} \\
  \wlp{x.\nextf := y}Q &\eqdef& \lnot \nrtc yx \land %{} \\
    %&& \quad
       \substitute{Q}{\nrtc\alpha\beta}{\nrtc\alpha\beta \lor (y\neq\mnull \land \nrtc\alpha x \land \nrtc y\beta)} \\
% Traversal
  \wlp{x := y.\nextf}Q &\eqdef&
      \forall \alpha \ldotp \Pnext(y,\alpha) \to \substitute{Q}{x}{\alpha}\\
  \textit{where ~}
  \Pnext(s,t) &\eqdef&
      (\ntc s t \land
           \forall \gamma \ldotp \ntc s\gamma \limplies \nrtc t\gamma) \lor
             (t = \mnull \land \forall \gamma \ldotp \lnot\ntc s\gamma) \\
% Allocation
  \wlp{x := \NEW}Q &\eqdef&
      \forall \alpha \ldotp \bigl(\Land_{p\in\Pvar\cup\{\mnull\}} \neg \nrtc p\alpha \bigr) \limplies
         \substitute{Q}{x}{\alpha} \\
\end{array}\\
\hline
 \end{array}\]
 \caption{\tablabel{Wprules}Rules for computing weakest liberal preconditions for procedures in IMP$^R$.
$Q$ is a post-condition expressed as a first-order formula.
The top frame shows the standard $\wlpSym$ rules for IMP, the bottom frame contains
our additions for heap updates, dereference, and memory allocation.
We assume that  the program nullifies a field before modifying it, i.e.,
every command of the form $x.\nextf := y$ is preceded by a command $x.\nextf := \NULL$.}
\end{table}


%To construct a transition system using $\wlpSym$, we define the \emph{identity} relation on states $\Identity$,
%such that $\cmodel\Identity\cmodel' \Leftrightarrow \cmodel=\cmodel'$. Formally, $\Identity$ is defined by
%a two-vocabulary formula
%\begin{equation}\label{Eq:identity}
%  \Identity ~\eqdef~ \Land_{c \in \textit{constants}} c = c' \quad\land \Land_{R \in \textit{predicates}} \forall \overline{\alpha}.~ R(\overline{\alpha}) \liff R'(\overline{\alpha})
%\end{equation}
%%
%where $\overline{\alpha}$ is a list of variables according to the arity of the predicate symbol $R$.
%The vocabulary $\vocabulary$ corresponds to the structure $\cmodel$, and $\vocabulary'$ corresponds to $\cmodel'$.
%Take notice that we casually identify a binary relation on states with the two-vocabulary formula
%defining it in that manner. Which one is referred to should be clear from the context.

%As described in \Cref{sec:programs-to-vp}, the transition relation $\Trans$
%is defined via the formula $\Trans \eqdef \kCond \land \wlp{\kCmd}\Identity$,
%with $\kCond$ being the condition and $\kCmd$ being the body of the $\WHILE$ loop.

Given the $\wlpSym$ definition, the transition relation $\Trans$
is defined as in \Cref{Eq:rho} (see \Cref{sec:programs-to-vp}).
It is important to notice that, since $\Pnext$ is a universal formula occurring in a
negative context in $\wlp{x := y.\nextf}Q$, which is itself defined by a universal formula,
the resulting formula $\Trans$ will have a quantifier prefix $\forall^*\exists^*$. We would like
to get an $\EAR$ formula for our purposes; we achieve this by changing the quantified rules
slightly, resulting in the variants in \Cref{Ta:wlp-exists}. All other rules remain unchanged.
From here on we switch to the definition of the transition relation as%~---
\begin{equation}\label{Eq:transition}
  \Trans ~\eqdef~ \kCond \land \wlpE{\kCmd}\Identity
\end{equation}

Notice that the equivalence of semantics relies on the fact that for every location
$s\in\universe$ which is different from $\mnull$
there is exactly one location $t$ such that $\Pnext(s,t)$, as follows from the definition.

%the validity of the
%sentence $\forall s.\,\exists!t.\, \Pnext(s,t)$, following from
%the definition.
%Informally, for every location $s\in\universe$ different from $\mnull$
%there is exactly one $t$ such that $\Pnext(s,t)$.
%
% Notice that the equivalence of semantics relies on the fact that for every location
% $i\in\Locs\setminus\{\mnull\}$ there is exactly one $j\in\Locs$ such that $\Pnext(i,j)$,
% as follows from the definition.

\begin{table}
\[
 \renewcommand\arraystretch{1.3}
 \begin{array}{|r@{~~~}c@{~~~}l|}
  \hline
  % Traversal
  \wlpE{x := y.\nextf}Q &\eqdef&
      \exists \alpha \ldotp \Pnext(y,\alpha) \land \substitute{Q}{x}{\alpha}\\
  % Allocation
  \wlpE{x := \NEW}Q &\eqdef&
      \exists \alpha \ldotp \bigl(\Land_{p\in\Pvar\cup\{\mnull\}} \neg \nrtc p\alpha \bigr) \land
         \substitute{Q}{x}{\alpha} \\
  \hline
 \end{array}
\]
\caption{\label{Ta:wlp-exists}
  Leading-existential variant of $\wlpSym$ rules.}
\end{table}

\begin{example}\label[example]{Ex:Unchain}
Since the transition relation obtained for \code{filter()} is large, we demonstrate the construction of $\rho$ on a simpler program,
where the loop body consists of the following command:
\[
\kCmd ~=~ k:=i.\nextf\,;~ i.\nextf := \NULL\,;~ i:=k
\]
%For the program $i:=h ~;~ \WHILE ~ i\neq\NULL ~ ( ~ k:=i.\nextf ~;~ i.\nextf := \NULL ~;~ i:=k ~ )$,
We then have
\begin{align*}
  \wlpE{\kCmd}Q & = \wlpE{k:=i.\nextf\,;~ i.\nextf := \NULL\,;~ i:=k}Q \\
                & = \wlpE{k:=i.\nextf}{\bigl(\wlpE{i.\nextf := \NULL}{\bigl(\wlpE{i:=k}Q\bigr)}\bigr)} \\
                & = \exists \alpha \ldotp \Pnext(i,\alpha) \land
    \substitute{\bigl(\wlpE{i.\nextf := \NULL}{(\substitute{Q}{i}{k})}\bigr)}{k}{\alpha} \\
                & = \exists \alpha \ldotp \Pnext(i,\alpha) \land
    \substitute{\bigl(\substitute{(\substitute{Q}{i}{k})}{\nrtc\alpha\beta}{\nrtc\alpha\beta\land(\lnot\nrtc\alpha i\lor\nrtc\beta i)}\bigr)}{k}{\alpha}
\end{align*}
% \[\renewcommand\arraystretch{1.5}
% \begin{array}{@{}l@{}}
%   \wlpE{\kCmd}Q = \wlpE{k:=i.\nextf\,;~ i.\nextf := \NULL\,;~ i:=k}Q = {} \\
%   \quad = \wlpE{k:=i.\nextf}{\Bigl(\wlpE{i.\nextf := \NULL}{\bigl(\wlpE{i:=k}Q\bigr)}\Bigr)} = {} \\
%   \quad = \exists \alpha.\, \Pnext(i,\alpha) \land
%     \substitute{\Big(\wlpE{i.\nextf := \NULL}{\big(\substitute{Q}{i}{k}\big)}\Big)}{k}{\alpha} = {} \\
%   \quad = \exists \alpha.\, \Pnext(i,\alpha) \land
%     \substitute{\Big(\substitute{\big(\substitute{Q}{i}{k}\big)}{\nrtc\alpha\beta}{\nrtc\alpha\beta\land(\lnot\nrtc\alpha i\lor\nrtc\beta i)}\Big)}{k}{\alpha}
% \end{array}
% \]

The transition relation is then constructed as follows:
\begin{align*}
  \Trans & = i\neq\mnull \land \wlpE{\kCmd}{\Identity} \\
         & = i\neq\mnull \land \wlpE{\kCmd}{(i=i'\land k=k' \land \forall \alpha,\beta \ldotp \nrtc\alpha\beta\liff\xnrtc\alpha\beta)} \\
         & = i\neq\mnull \land {} \\
         &\quad\; \exists \alpha \ldotp \Pnext(i,\alpha) \land
     \bigl(\alpha=i'\land \alpha=k' \land \forall \alpha,\beta \ldotp \bigl(\nrtc\alpha\beta\land(\lnot\nrtc\alpha i\lor\nrtc\beta i)\bigr)\liff\xnrtc\alpha\beta\bigr)
\end{align*}
% \[\renewcommand\arraystretch{1.5}
% \begin{array}{@{}r@{}c@{}l@{}}
%   \Trans & {}={} & i\neq\mnull \land \wlpE{\kCmd}\Identity = {} \\
%          & {}={} & i\neq\mnull \land \wlpE{\kCmd}{\big(i=i'\land k=k' \land \forall \alpha\beta.~ \nrtc\alpha\beta\liff\xnrtc\alpha\beta\big)} = {} \\
%          & {}={} &  i\neq\mnull \land {} \\ && \exists \alpha.\, \Pnext(i,\alpha) \land
%      \big(\alpha=i'\land \alpha=k' \land \forall \alpha\beta.\, \big(\nrtc\alpha\beta\land(\lnot\nrtc\alpha i\lor\nrtc\beta i)\big)\liff\xnrtc\alpha\beta\big) \\
% \end{array}
% \]
\end{example}

%\begin{example}\label[example]{Ex:Unchain}
%For the program $i:=h ~;~ \WHILE ~ i\neq\NULL ~ ( ~ k:=i.\nextf ~;~ i.\nextf := \NULL ~;~ i:=k ~ )$,
%we have---
%\[\renewcommand\arraystretch{1.5}
%\begin{array}{@{}l@{}}
%  \wlpE{\kCmd}Q = \wlpE{k:=i.\nextf ~;~ i.\nextf := \NULL ~;~ i:=k}Q = {} \\
%  \quad = \wlpE{k:=i.\nextf}{\Big(\wlpE{i.\nextf := \NULL}{\big(\wlpE{i:=k}Q\big)}\Big)} = {} \\
%  \quad = \exists \alpha.~ \Pnext(i,\alpha) \land \substitute{\Big(\wlpE{i.\nextf := \NULL}{\big(\substitute{Q}{i}{k})}\Big)}{k}{\alpha} = {} \\
%  \quad = \exists \alpha.~ \Pnext(i,\alpha) \land
%    \substitute{\Big(\substitute{\big(\substitute{Q}{i}{k}\big)}{\nrtc\alpha\beta}{\nrtc\alpha\beta\land(\lnot\nrtc\alpha i\lor\nrtc\beta i)}\Big)}{k}{\alpha}
%\end{array}
%\]
%
%The transition system is then constructed as follows:
%\[\renewcommand\arraystretch{1.5}
%\begin{array}{@{}r@{}c@{}l@{}}
%  \Trans & {}={} & i\neq\mnull \land \wlpE{\kCmd}\Identity = {} \\
%         & {}={} & i\neq\mnull \land \wlpE{\kCmd}{\big(i=i'\land k=k' \land \forall \alpha\beta.~ \nrtc\alpha\beta\liff\xnrtc\alpha\beta\big)} = {} \\
%         & {}={} &  i\neq\mnull \land {} \\ && \exists \alpha.~ \Pnext(i,\alpha) \land
%     \big(\alpha=i'\land \alpha=k' \land \forall \alpha\beta.~ \big(\nrtc\alpha\beta\land(\lnot\nrtc\alpha i\lor\nrtc\beta i)\big)\liff\xnrtc\alpha\beta\big) \\
%\end{array}
%\]
%\end{example}
%\todo{refer to the benchmarks section for more details?}

\begin{proposition}
$\EAR$ is closed under $\wlpE{\kCmd}{}$; that is,
if $Q\in\EAR$ then $\wlpE{\kCmd}Q\in\EAR$.
In particular, $\Trans\in\EAR$ (as defined by \Cref{Eq:transition}).
\end{proposition}



\para{Initial and bad states}
We express properties of
list-manipulating programs, e.g., their pre- and post-conditions, $\Pre$ and $\Post$,
respectively,
using assertions written in $\EAR$
over the above vocabulary. % fragment of first-order logic with transitive closure.
$\minit$ and $\mbad$ are defined based on these assertions, as shown in \Cref{Eq:init-bad} (see \Cref{sec:programs-to-vp}).
%%
%\[\minit ~\eqdef~ \Pre \qquad
%  \Bad   ~\eqdef~ (\lnot\kCond \land \lnot\Post) ~\lor~ \big(\kCond \land \lnot\wlp{\kCmd}\true\big)\]


\begin{example}
In \Cref{Ex:Filter}, we have
$\Pre = i=h \land j = \mnull$ and
$\Post = h \neq \mnull \to \forall z \ldotp \nrtc{h}{z}\to \isOK(z)$.
Note that these refer to the pre- and post-conditions that should hold right before the loop begins and right after it terminates, respectively.
Therefore,
$\minit \eqdef i=h \land j = \mnull$ and
$\mbad  \eqdef i = \mnull \land \neg (h \neq \mnull \to \forall z \ldotp \nrtc{h}{z}\to \isOK(z))$.
Here, a state is bad if $i = \mnull$ (i.e., it occurs when the loop terminates)
and \code{h} points to a non-empty list that contains an element not having
the property $\isOK$. In this example there are no $\ASSERT$ statements in the body, hence
the second disjunct in the definition of $\mbad$ in \Cref{Eq:init-bad},
which captures the semantics of assertion violations, simplifies to $\false$ and is subsumed by the first disjunct.
\end{example}

%\begin{example}
%In \Cref{Ex:Unchain}, let $\Post = \forall\alpha.~\unrtc h\alpha \limplies \Pnext(\alpha,\mnull)$,
%where $\unrtcSym$ capture $\nrtcSym$ at the input state. In that case
%\[
%\begin{array}{r@{}c@{}l}
%  \minit & {}={} & (i = h) \land \forall\alpha\beta.~ \unrtc\alpha\beta \liff \nrtc\alpha\beta  \\
%  \mbad  & {}={} & \big( (i = \mnull) \land \lnot\forall\alpha.~\unrtc h\alpha \limplies \Pnext(\alpha,\mnull) \big) ~\lor~ {} \\
%         &       & \big( (i \neq \mnull) \land \lnot\exists\alpha.~ \Pnext(i,\alpha)\land\true \big)
%\end{array}
%\]
%
%The second disjunct in $\mbad$ captures the semantics of assertion violations.
%In this example there are no $\ASSERT$ statements in the body, so the only violation is
%$\lnot\exists\alpha.~ \Pnext(i,\alpha)$ (which cannot really happen since $i\neq\mnull$; thus the program is safe).
%\end{example}

In our analysis, the $\EAR$ formulae $\minit$, $\mbad$ and $\Trans$
are translated into equisatisfiable $\EPR$ formulae~\cite{CAV:IBINS13}.

\subsection{Modeling of Programs Manipulating Cyclic Linked Lists}

As an extension of previous work~\cite{CAV14:Itzhaky,CAV:KBIRS15}, which targeted acyclic data structures,
we augment the formalism shown above to handle a restricted form of cycles.
The new formalism allows at most one cycle to be present in the
heap at any given time. This is achieved by decomposing the pointer edges,
labeled $n$, into a set of acyclic edges labeled $k$ plus \emph{at most one}
additional edge labeled $m$. This is always possible~--- if the heap contains
(at most) one cycle, then it is enough to remove (at most) one edge to make
it acyclic.

We denote $\nrtcSym$ and $\krtcSym$ the reflexive transitive closures of $n$ and $k$, and $\abra{m_s,m_t}$
the source and destination of the edge labeled $m$, if it is present
(if $m$ is not present, $m_s=m_t=\mnull$). The following relationship holds
between $\nrtcSym$ and $\krtcSym,m_s,m_t$:
%
\begin{equation}
  \forall\alpha,\beta \ldotp
    \nrtc\alpha\beta ~\Leftrightarrow~
    \krtc\alpha\beta \lor (m_s\neq\mnull \land \krtc\alpha{m_s} \land \krtc{m_t}\beta)
\end{equation}

Therefore $k^*,m_s,m_t$ fully characterize the heap reachability.
The axioms in \Cref{Ta:axioms-linord} now hold for $\krtcSym$ instead of $\nrtcSym$.
In addition, we require that if $m$ is present, then $m_s$ has no $k$-successor,
and the edge $m$ closes the cycle;
that is, $m_s\neq\mnull\limplies \krtc{m_t}{m_s} \land \lnot\exists\alpha.\,\ktc {m_s}\alpha$.
We now modify the $\wlpSym$ formulae from \Cref{Ta:Wprules} to reflect the
new situation. The new semantics for $x.\nextf := \NULL$, $x.\nextf := y$,
and $x := y.\nextf$ are shown in \Cref{Ta:Wprules-cycles}.
The multiple substitutions in the brackets are done in parallel.
The operator $\ite(p,a,b)$ denotes a term that is equal to $a$ if $p$ is $\true$,
and $b$ otherwise.\footnote{%
Any formula containing $\ite$ can be translated to an equivalent first-order
formula using only standard connectives; however, SMT-LIB-compliant solvers
natively support $\ite$~\cite{BarST-SMTLIB} so this translation is not needed.
}
$\Rkm(s,t)$ is a formula expressing a path between $s$ and $t$ utilizing
the special edge $m$.
$\Rkm(u,u)$ means that $u$ lies on the cycle.

\begin{table}
%\resizebox{\textwidth}{!}{
\[
 \renewcommand\arraystretch{1.3}
 \begin{array}{|r@{~~~}c@{~~~}l|}
  \hline
  % Destructive Updates
  \wlp{x.\nextf := \NULL}Q &\eqdef&
      \substitutethreeVert{Q}{m_s}{\ite(\Rkm(x,x),\mnull,m_s)}
                             {m_t}{\ite(\Rkm(x,x),\mnull,m_t)}
                             {\krtc\alpha\beta}{\krtc\alpha\beta \land (\lnot \krtc\alpha x \lor \krtc\beta x) \lor{} \\
                             \quad (\Rkm(\alpha,\beta)\land \lnot\krtc\alpha x \land \krtc\beta x \land x \neq m_s)} \\
  \textit{where ~}
  \Rkm(s,t) &\eqdef& m_s\neq\mnull \land \krtc s{m_s} \land \krtc{m_t}t \\
  \wlp{x.\nextf := y}Q &\eqdef& (\krtc yx \limplies m_s=\mnull) \land{} \\
   && \substitutethreeVert{Q}{m_s}{\ite(\krtc y x,x,m_s)}
                             {m_t}{\ite(\krtc y x,y,m_t)}
                             {\krtc\alpha\beta}{\krtc\alpha\beta \lor {} \\
                               \quad (y\neq\mnull \land \lnot\krtc yx \land \krtc\alpha x \land \krtc y\beta)} \\
  % Traversal
  \wlp{x := y.\nextf}Q &\eqdef&
      \forall \alpha \ldotp \Pnext(y,\alpha) \limplies \substitute{Q}{x}{\alpha} \\
  \textit{where ~}
  \Pnext(s,t) &\eqdef&
      \begin{cases}
        t = m_t & s = m_s  \\
        (\ktc s t \land \forall \gamma \ldotp \ktc s\gamma \limplies \krtc t\gamma) \lor {} \\
        \quad (t=\mnull \land \forall \gamma \ldotp \lnot\ktc s\gamma) & \textit{otherwise} \\
      \end{cases} \\
  \hline
 \end{array}
\]
%}
\caption{\label{Ta:Wprules-cycles}
  Modified $\wlpSym$ rules for handling (restricted) cyclic lists.}
\end{table}

The semantics maintains the edge $m$ by creating it when a cycle is closed as
a result of an assignment of the form $x.\nextf := y$, and removing it or replacing
it with a $k$ edge when the cycle is broken by $x.\nextf := \NULL$.
Notice that $\wlp{x := y.\nextf}{}$ remains as in \Cref{Ta:Wprules}, except
that the definition of $\Pnext$ is changed. The adjustment in \Cref{Ta:wlp-exists}
for $\wlpESym$ is suitable in this case as well.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
