%!TEX root = ./paper.tex

\section{Introduction}
\label{Se:Introduction}

We present \emph{Universal Property Directed Reachability} (\UPDR),
a procedure for automatic inference of  quantified
inductive invariants, and its application for the analysis of
programs that manipulate unbounded data structures such as
singly-linked and doubly-linked list data structures.
%\footnote{Technically, an assertion is inductive if it is preserved by every step of the program. See \Cref{Def:Invariant}.}
For a correct program, the inductive invariant generated
ensures that the program satisfies its
specification.
For an erroneous program, \UPDR\ produces a concrete counterexample.
Historically, this has been addressed by abstract interpretation~\cite{kn:CC77} algorithms,
which automatically infer sound inductive invariants,
%e.g.,~\cite{TOPLAS:SRW02,JACM:Calcagno11},
and bounded model checking algorithms,
which explore a limited number of loop iterations in order to systematically look for bugs~\cite{TACAS99:Biere,DAC:Clarke03}.
We continue the line of recent works~\cite{CAV14:Itzhaky,ESOP:C15} which simultaneously search for invariants and counterexamples.
We follow %the original idea of
Bradley's PDR/IC3 algorithm~\cite{VMCAI:Bradley11} by repeatedly
strengthening a candidate invariant until it either becomes inductive,
or a counterexample is found. % or an inductive invariant is identified.

In our experience, the correctness of many programs can be proven using universal invariants.
Hence, we simplify matters by focusing on inferring universal first-order invariants.
When \UPDR\ terminates,
it yields one of the following outcomes:
(i)~a universal inductive invariant strong enough to show that the program respects the property,
(ii)~a concrete counterexample which shows that the program violates the desired safety property,
or
(iii)~a \emph{proof that the program cannot be proven correct using a universal invariant}
in a given vocabulary.


\para{Diagram Based Abstraction}
Unlike previous work \cite{CAV14:Itzhaky,ESOP:C15},  we neither assume that the
predicates which constitute the invariants are known, nor apriori bound the number of
universal quantifiers.
Instead, we rely on first-order theories with a \emph{finite model property}:
for such theories, SMT-based tools are able to either return \texttt{UNSAT},
indicating that the negation of a formula~$\varphi$ is valid,
or construct a \emph{finite} model~$\cmodel$ of~$\varphi$.
We then translate $\cmodel$ into a \emph{diagram}~\cite{chang1990model}---a formula
describing the set of  models that extend $\cmodel$---and use the diagram to construct
a \emph{universal} clause to strengthen a candidate invariant.
%\notepr[SH]{I would say strengthen}

\input{split-filter}


\para{Property-Directed Invariant Inference}
Similarly to IC3, \UPDR\ iteratively constructs an increasing
sequence of candidate inductive invariants $\aframe{0}\til\aframe{N}$.
Every $\aframe{i}$ over-approximates the set $\Reach_i$ of states that can be reached by up to $i$ execution steps from
a given set of \emph{initial} states.
In every iteration, \UPDR\
uses SMT to check whether one of the candidate invariants became inductive.
If so, then %an inductive invariant is discovered and hence
% we are assured that this is indeed the case and hence the
the program respects the desired property.
%If the result is \texttt{UNSAT}, then it discovered an inductive invariant and hence the
%% we are assured that this is indeed the case and hence the
%program respects the desired property.
If not, \UPDR\ iteratively strengthens the candidate invariants and adds new ones,
guided by the considered property.
Specifically, it checks %satisfiability of a formula that expresses
%the existence of a successor state of $\aframe{i}$ that is not included in $\aframe{i}$.
if there exists a \emph{bad} state $\cmodel$ which satisfies  $\aframe{N}$ but not the  property.
%If the result is \texttt{SAT},
%
%Otherwise, there exists a \emph{bad} state $\cmodel$ which satisfies  $\aframe{N}$ but not the  property.
%\notepr[SH]{there is another case. We probably don't want to get into the details so need to change previous sentence}
If so, we use SMT again to check whether there is a state $\cmodel_a$ in $\aframe{N-1}$ that can lead to a state in the \emph{diagram}  $\varphi$ of $\cmodel$ in one execution step.
If no such state exists,  the candidate  invariant $\aframe{N}$ can be strengthened by conjoining it with the negation of $\varphi$.
Otherwise, we recursively strengthen $\aframe{i-1}$ to exclude $\cmodel_a$ from its over-approximation of $\Reach_{i-1}$.
If the recursive process tries to strengthen $\aframe{0}$,
we stop and use a bounded model checker to look for a counterexample of length $N$.
If no counterexample is found,
\emph{\UPDR\ determines
that no universal invariant strong enough to prove the desired property exists} (see \Cref{lem:diagram}).
We note that \UPDR\ is not guaranteed to terminate, although in our experience it often does.
%
\vspace{-1ex}
%
\begin{example}\label{Ex:Split}
Procedure \code{split()},
shown in \Cref{Fi:Split}(a),
moves the elements not satisfying
the condition $C$ from the list pointed to by \code{h} to the list pointed by \code{g}.
\UPDR\ can infer tricky inductive invariants strong enough to prove several
interesting properties:
(i)~memory safety, i.e., no null dereference and no memory leaks;
(ii)~all the elements satisfying $C$ are kept in $h$;
(iii)~all the elements which do not satisfy $C$ are moved to $g$;
(iv)~no new elements are introduced; and
(v)~stability, i.e., the reachability order between the elements satisfying $C$ is not changed.
Our implementation verified that \code{split()} satisfies all the above properties
fully automatically by inferring an inductive loop invariant
consisting of $36$ clauses (among them $19$ are universal formulae) in $206$~sec.
%For space reasons, we cannot show the invariant.
\end{example}
%
\vspace{-3ex}
%
\begin{example}\label{Ex:Filter}
Procedure \code{filter()}, shown in \Cref{Fi:Filter}(b), removes and deallocates
the elements not satisfying
the condition $C$ from the list pointed to by \code{h}.
The figure also shows the loop invariant inferred by \UPDR\ when it was asked to verify
a simplified version of property (iii):
%stating that 
all the elements which do not satisfy $C$ are removed from \code{h}.
The invariant highlights certain interesting properties of \code{filter()}.
For example, clause $L_4$ says that if the head element of the list was processed and
kept in the list (this is the only way $i \neq h$ can hold), then $j$ becomes an immediate predecessor of $i$.
Clause $L_7$ says that all the elements $x_3$ reachable from $h$ and not
satisfying $C$ must occur after $j$.
\end{example}

%
\vspace{-0.8ex}
%
\para{Experimental Evaluation}
We implemented $\UPDR$ on top of the decision procedure of~\cite{CAV14:Itzhaky},
and applied it to a collection of procedures that
manipulate  (possibly sorted) singly linked lists, doubly-linked lists, and
multi-linked lists.
Our analysis successfully verified interesting specifications, detected bugs in incorrect programs,
and established the absence of
universal invariants for certain correct programs.

\para{Main Contributions}
The main contributions of this work can be summarized as follows.
\begin{compactitem}[\textbullet]
\item We present \UPDR, a pleasantly simple, yet surprisingly powerful, combination of
PDR~\cite{VMCAI:Bradley11} with a  strengthening
technique based on diagrams~\cite{chang1990model}. \UPDR\ enjoys a high-degree of automation because it does \emph{not} require
pre-defined abstraction predicates.
\item The diagram-based abstraction is particularly interesting as it is determined ``on-the-fly''
according to the  structural properties of the %(arbitrarily chosen)
bad states discovered in PDR's
traversal of the state space.
\item We prove that the diagram-based abstraction is precise in the sense that
if \UPDR\ finds a spurious counterexample then the program cannot  be proven correct using a universal invariant.
We believe that this is a unique feature of our approach.
\item We implemented \UPDR\ on top of a decision procedure for logic $\AER$~\cite{CAV:IBINS13},
and applied it successfully to verify a collection of list-manipulating programs,
detect bug, and prove the absence of universal invariants.
We show that our technique outperforms  an existing state-of-the-art less-automatic
PDR-based verification technique~\cite{CAV14:Itzhaky} which uses the same decision procedure.
\end{compactitem}


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Discards %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TACAS99:Biere,DAC:Clarke03}.
% We continue the line of recent work~\cite{
% %
% % \subsection{Outline}\label{Se:Outline}
% The following is the suggested abstract + initial work distribution.
%
%
%
% \begin{enumerate}
% \item Introduction [Mooly]
% \item Running Example (simple filter) [Noam]
% \item Algorithm
%     \begin{enumerate}
%     \item Pseudo code of PDR (Reuse text from CAV'14) [Noam]
%     \item Figure with the facts discovered for the running example [Noam]
%     \end{enumerate}
% \item Discussion
%     \begin{enumerate}
%     \item Theorem 1: Correctness of PDR [Sharon + Aleks]
%     \item Theorem 2: Monotonicity/Precision: Using diagrams does not remove ``unwanted'' states   [Sharon + Aleks]
%     \item Theorem 3: Abstract counter examples $\Rightarrow$ No universal invarinat. [Noam + Sharon + Aleks]
%     \item Theorem 4: Termination [Noam + Sharon + Aleks]
%     \end{enumerate}
% \item Evaluation [Aleks]
%     \begin{enumerate}
%     \item Setting: EPR + wlp for lists
%     \item Case studies (highlights of interesting verification problems): complicated filter + insertion sort
%     \item Benchmarks + Comparison with CAV'14
%     \end{enumerate}
% \item Related work [Nikolaj]
% \item Conclusion
% \end{enumerate}
%
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CAV'14 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \Omit{
% % \vspace{-1.0ex}
% % 1. Describe the problem
% The goal of our work is to automatically generate quantified
% invariants for programs that manipulate
% singly-linked and doubly-linked list
% data structures.  For a correct program, the invariant generated
% ensures that the program has no memory-safety violations,
% such as null-pointer dereferences, and that
% data-structure invariants are preserved.  For a program in which it is
% possible to have a memory-safety violation or for a data-structure
% invariant to be violated, the algorithm produces a concrete
% counterexample.
% % NSB: Technically, theorem 1 says something weaker.
% Although in this paper we mainly discuss
% memory-safety properties and data-structure invariants, the technique
% can be easily extended to other correctness properties (see
% \secref{EmpiricalEvaluation}).
%
% To the best of our knowledge, our method represents the first
% shape-analysis algorithm that is capable of (i)~reporting concrete
% counterexamples, or alternatively (ii)~establishing that the abstraction
% in use is not capable of proving the property in question.
% This result is achieved by combining several existing ideas in a new
% way:
% \begin{itemize}
%   \item
%     The algorithm uses a predicate-abstraction domain \cite{CAV:GS97}
%     in which
%     quantified
%     predicates express properties of singly and doubly linked
%     lists.
%     In contrast to most recent work, which uses restricted forms of
%     predicate abstraction---such as Cartesian abstraction
%     \cite{TACAS:BPR01}---our algorithm uses full predicate abstraction
%     (i.e., the abstraction uses arbitrary Boolean combinations of predicates).
%   \item
%     The abstraction predicates and language semantics are expressed in
%     recently developed \emph{reachability logics}, $\AFR$ and $\EAR$, respectively, which
%     are decidable using a reduction to SAT \cite{CAV:IBINS13}.
%     % NSB: Standard SAT solvers can't solve EPR formulae, so it is
%     % wrong to say 'reduction to standard SAT solvers'
%   \item
%     The algorithm is property-directed---i.e., its choices
%     are driven by the memory-safety properties to be proven.
%     In particular, the algorithm is based on IC3 \cite{VMCAI:Bradley11},
%     which we here refer to as \emph{property-directed reachability} (PDR).
%     %NSB: removed self-reference because we don't do Horn clauses here.
% \end{itemize}
%
% PDR integrates well with full predicate abstraction:
% in effect, the analysis obtains the same precision as
% the best abstract transformer for full predicate abstraction,
% without ever constructing the transformers explicitly.
% In particular, we cast PDR as a \emph{framework} that is parameterized on
% \begin{itemize}
% \item the logic $\LogicLang$ in which the semantics of
%     program statements are expressed, and
%   \item the finite set of predicates that define the abstract domain
%     $\AbsDomain$ in which invariants can be expressed. An element of
%     $\AbsDomain$ is an arbitrary Boolean combination of the
%     predicates.
% \end{itemize}
% Furthermore, our PDR framework is \emph{relatively complete with respect
% to the given abstraction}. That is, the analysis
% is guaranteed to terminate and
% either
% (i)~verifies the given property, (ii)~generates a concrete
% counterexample to the given property, or (iii)~reports
% that the abstract domain is not expressive enough to establish
% the proof.
% %NSB: this is repeated too many times in the paper.
% % It got to the point of being over-repetitious to me.
% % Why not highlight Theorem 1 early on and refer to it?
% Outcome (ii) is possible because the ``frame'' structure
% maintained during PDR can be used to build a trace formula;
% if the formula is satisfiable, the model can be presented to the
% user as a concrete counterexample. Moreover, if the analysis
% fails to prove the property or find a concrete counterexample (outcome (iii)),
% then there is no way to express an inductive invariant that
% establishes the property in question using a Boolean combination of
% the abstraction predicates.
% Note that outcome (iii) is a much stronger guarantee than what other
% approaches provide in such cases when they neither succeed nor give a concrete
% counterexample.
%
% Key to instantiating the PDR framework for shape analysis was a
% recent development of the $\AFR$ and $\EAR$ logics for expressing properties of
% linked lists \cite{CAV:IBINS13}. %In the PDR framework,
% $\AFR$ is used to define abstraction predicates,
% and $\EAR$ is used to express the language semantics.
% $\AF$ is a decidable, alternation-free fragment of first-order logic
% with transitive closure ($\FOTC$).  When applied to list-manipulation
% programs, atomic formulae of $\AF$ can denote reachability relations
% between memory locations pointed to by pointer variables, where
% reachability corresponds to repeated dereferences of $\nextf$ or
% $\prevf$ fields.  One advantage of $\AF$ is that it does not require
% any special-purpose reasoning machinery:
% an $\AF$ formula can be
% converted to a formula in ``effectively propositional'' logic,
% which can be reduced to SAT solving.
% That is, in
% contrast to much previous work on shape analysis, our method makes use
% of \emph{a general purpose SMT solver}, Z3~\cite{TACAS:deMB08}
%  (rather than specialized
% tools developed for reasoning about linked data structures, e.g.,
% \cite{TOPLAS:SRW02,TACAS:DOY06,CAV:BCCDOWY07,SAS:GMP13}).
%
%
% The main restriction in $\AF$ is that it allows the use of a relation
% symbol $f^*$ that denotes the transitive closure of a function symbol
% $f$, but only limited use of $f$ itself. Although this restriction can
% be somewhat awkward, it is mainly a concern for the analysis
% designer (and the details have already been worked out in
% \cite{CAV:IBINS13}).  As a language for expressing invariants, $\AF$
% provides a fairly natural abstraction, which means that analysis
% \emph{results} should be understandable by non-experts (see
% \secref{Overview}).\footnote{ By a
%   ``non-expert'', we mean someone who has no knowledge of either the
%   analysis algorithm,
% % NSB: I bet the only users ever of this will in fact understand the algorithm.
% or the abstraction techniques used inside the
%   algorithm.  }
%
% % 2. State your contributions
% Our work represents the first algorithm for shape analysis that either
% (i)~succeeds, (ii)~returns a concrete counterexample, or
% (iii)~returns an abstract trace showing that the abstraction in use is
%  not capable of proving the property in question.
% The specific contributions of our work include
% \begin{itemize}
%   \item
%     A framework, based on the PDR algorithm, for finding an inductive
%     invariant in a certain logic fragment (abstract domain) that
%     allows one to prove that a given pre-/post-condition holds or find
%     a concrete counter-example to the property, or, in the case of a
%     negative result, the information that there is no inductive
%     invariant expressible in the abstract domain
%     (\secref{PropertyDirectedReachability}).
%   \item
%     An instantiation of the framework for finding invariants of
%     programs that manipulate singly-linked or doubly-linked lists.
%     This instantiation uses $\AF$ to define a simple
%     predicate-abstraction domain, and is the first application of PDR
%     to establish quantified invariants of programs that manipulate
%     linked lists (\secref{AF}).
%   \item
%     An empirical evaluation showing the efficacy of the PDR framework
%     for a set of linked-list programs (\secref{EmpiricalEvaluation}).
% \end{itemize}
%
% }
%
%
% %%% Local Variables:
% %%% mode: latex
% %%% TeX-master: "paper"
% %%% End:
