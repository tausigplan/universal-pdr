%!TEX root = ./paper.tex

\section{Motivating Example}
\label{Se:Overview}

We describe the details of the $\UPDR$ algorithm in \secref{Updr}.
Here we illustrate it informally using procedure $\mathtt{filter()}$, shown in \figref{Filter} example.
Procedure $\mathtt{filter()}$ removes all the elements not satisfying property $C(\cdot)$ from a singly-linked list pointed to by \lstinline{h}.
Our analysis can verify that the elements that have property $C$ and were reachable from $h$ when $\mathtt{filter}$ was invoked, are reachable from $h$ when the procedure terminates and
that the relative order between them is maintained. For brevity, we consider here a weaker specification: 
%The pre- and the post-condition of procedure $\mathtt{filter}$ are given in \figref{Filter}.
%They say that  
%Regardless of the initial state, if
When $\mathtt{filter}$ terminates, the list pointed to by $\mathtt{h}$ contains only elements which have property $C$.
% does not contain
% then all the elements reachable from the head have property $C(\cdot)$.\footnote{Our analysis is capable of verifying stronger properties,
% e.g., that all
% For simplicity, we illustrate the analysis using a simpler specification.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\lstset{
  language=C,
  columns=fullflexible,
  basicstyle=\footnotesize\ttfamily,
  numbers=none,
  morekeywords={then},
  numberstyle=\scriptsize,
  stepnumber=1,
  breaklines=true,
}
\begin{figure}[t]
\centering
\begin{minipage}{0.8\textwidth}
% $\fname{pre}\colon \mtrue$
% $\fname{post}\colon \forall v.\, \unrtc{h}{v} \implies C(z)$
\begin{lstlisting}[mathescape]
void filter(List h) {
  i = h; j = null;
  while ($i \neq \tnull$){
    if $\neg C(i)$ then  
      if $i = h$ then h := i.n else j.n := i.n;
    else j := i; 
    i := i.n }}
\end{lstlisting}
\end{minipage}
{\small\caption{\label{Fi:Filter}%
  A procedure that removes all the elements not satisfying $C(\cdot)$
  from the list pointed by \lstinline{h}.
}}
\end{figure}

%
% Given an input procedure,
% annotated with a pre-condition $\Pre$ and post-condition $\Post$ (expressed as
% formulas over the same vocabulary of predicates);
% the goal of the analysis is to compute
%  an invariant for the head of each loop
%
% expressed as
% a CNF formula over the predicates given in \tabref{Pred} (and
% their negations).


The goal of the analysis is to infer an invariant for the head of each loop.  
Note that the user of the analysis is required  to provide the vocabulary, but not any \emph{abstraction predicates}~\cite{}.
These are automatically inferred by the analysis.
The inferred invariants are conjunctions of \emph{clauses}, where each clause is a universal formulae.
For example, when applied to the running example, the analysis inferred invariant $I$, shown below:
\begingroup
%\allowdisplaybreaks
\[
\begin{array}{l}% \label{Eq:Inv}
I  = \ulemma_1 \land \ulemma_2 \land \ulemma_3 \land \ulemma_4 \land \ulemma_5 \land \ulemma_6 \land \ulemma_7, \quad \text{where} \\
%L_1 & = & i = h \lor \unrtc{j}{i} \lor i = h \\
~~\begin{array}{rcl}% \label{Eq:Inv}
L_1 & = & i \neq h \land i \neq \mnull \to \unrtc{j}{i} \\
L_2 & = & C(h) \lor h = i \\
%L_2 & = & h \neq i \to C(h) \\
L_3 & = & \unrtc{h}{j} \lor i \neq j \\
%L_4 & = & \forall x_2.\, \unrtc{i}{x_2} \lor x_2 = j \lor i = h \lor \neg\unrtc{j}{x_2} C(x_2) \\
L_4 & = & \forall x_2.\, i \neq h \land \unrtc{j}{x_2} \land x_2 \neq j \to \unrtc{i}{x_2} \\
L_5 & = & C(j) \lor h = i \\
L_6 & = & \forall x_3.\, z = h \lor j = \mnull \lor \neg \unrtc{h}{x_3} \lor \unrtc{h}{j} \lor \neg C(j) \\
%L_7 & = & \forall x_4.\, j = \mnull \lor \neg \unrtc{h}{x_4} \lor x_4 = h \lor C(x_4) \lor \unrtc{j}{x_4} \\
L_7 & = & \forall x_4.\, j \neq \mnull \land \unrtc{h}{x_4} \land x_4 \neq h \land \neg C(x_4) \to \unrtc{j}{x_4} \\
\end{array}
\end{array}
\]
\endgroup
The invariant highlights certain interesting properties of \texttt{filter()}.
For example, clause $L_4$ says that if the head element of the list was processed and
kept in the list, then $j$ becomes an immediate predecessor of $i$.
Clause $L_7$ says that all the elements $u$ reachable from $h$ and not
satisfying $C$ must occur after $j$.
We note that while the invariant is strong enough to establish the post condition, the inferred clauses might be not minimal.
For example, the clause $L_6$ contains redundant disjuncts and can be replaced
by a stronger clause $j = \mnull \lor \unrtc{h}{j}$.

%Also note that the number of potential invariants is unbounded.


For the running example, our analysis proves that $\mathtt{filter()}$ respects
the given specification by inferring the loop invariant $I$.
Our implementation discovered the invariant by refining a sequence of four
assertions $\uframe_1,\ldots,\uframe_4 = I$, and proving inductiveness of $\uframe_4$.
In total, the algorithm performed $17$ refinement steps (i.e., $17$
counterexamples were used) and $102$ calls to the theorem prover in $8.94$
seconds.

After the refinement, $\uframe_4$ contains $11$ clauses.
The formula below is the content of $\uframe_4$ after elimination of $4$ redundant
clauses.
Note that $3$ of the clauses are properly universal.

% R[3] =
% Not(And(i != null, Not(n*(j, i)), i != h))
% Not(And(C(h), h != i))
% Not(And(Not(n*(h, j)), i == j))
% ForAll(x!V!73,
%        Not(And(Not(n*(i, x!V!73)),
%                x!V!73 != j,
%                i != h,
%                n*(j, x!V!73))))
% Not(And(C(j), h != i))
% ForAll(x!V!36,
%        Not(And(x!V!36 != h,
%                j != null,
%                n*(h, x!V!36),
%                Not(n*(h, j)),
%                Not(C(j)))))
% ForAll(x!V!49,
%        Not(And(j != null,
%                n*(h, x!V!49),
%                x!V!49 != h,
%                C(x!V!49),
%                Not(n*(j, x!V!49)))))


%%%%%%%%%%%%%%%%%%%%%%%%%
%%% The previous version
%%%%%%%%%%%%%%%%%%%%%%%%%
% R[3] =
% And(ForAll(x!V!70,
%            Not(And(Not(n*(i, x!V!70)),
%                    Not(n*(x!V!70, j)),
%                    n*(j, x!V!70),
%                    i != h))),
%     Not(And(Not(n*(h, j)), i == j)),
%     Not(And(h != i, null == j)),
%     Not(And(C(j), h != i)),
%     ForAll([x!V!52, x!V!53],
%            Not(And(C(x!V!52),
%                    n*(h, x!V!52),
%                    n*(h, x!V!53),
%                    Not(n*(x!V!53, i)),
%                    Not(n*(i, x!V!53))))),
%     ForAll(x!V!48,
%            Not(And(Not(C(j)),
%                    C(x!V!48),
%                    n*(x!V!48, j),
%                    n*(h, x!V!48)))),
%     Not(And(C(h), h != i)),
%     ForAll(x!V!36,
%            Not(And(C(x!V!36),
%                    h != null,
%                    n*(h, x!V!36),
%                    Not(n*(h, j)),
%                    j != null,
%                    Not(n*(j, h))))))
%\begin{eqnarray*} \label{Eq:Inv}
%  I & = & \ulemma_1 \land \ulemma_2 \land \ulemma_3 \land \ulemma_4 \land \ulemma_5 \land \ulemma_6 \land \ulemma_7 \land \ulemma_8, \quad\text{where} \\
%L_1 & = & \forall x_1.\, \unrtc{i}{x_1} \lor \unrtc{x_1}{j} \lor
%                         \neg\unrtc{j}{x_1} \lor i = h \\
%L_2 & = & \unrtc{h}{j} \lor i \neq j \\
%L_3 & = & h = i \lor \mnull \neq j \\
%L_4 & = & C(j) \lor h = i \\
%L_5 & = & \forall x_2, x_3.\, C(x_2) \lor \neg \unrtc{h}{x_2} \lor \neg \unrtc{h}{x_3} \lor \unrtc{x_3}{i} \lor \unrtc{i}{x_3} \\
%L_6 & = & \forall x_4.\, \neg C(j) \lor C(x_4) \lor \neg \unrtc{x_4}{j} \lor \neg \unrtc{h}{x_4} \\
%L_7 & = & C(h) \lor h = i \\
%L_8 & = & \forall x_5.\, C(x_5) \lor h = \mnull \lor \neg\unrtc{h}{x_5} \lor \unrtc{h}{j} \lor j = \mnull \lor \unrtc{j}{h}
%\end{eqnarray*}


% \begin{figure}[t]
% \centering
% \begin{tabular}{ccc}
% \mbox{
% \xymatrix @-1pc {
% h\ar[d] & j\ar[rd] & i\ar[d] & null\ar[dl]\\
% *++[o][F-]{v_0} \ar[r]^n
% & *++[o][F-]{v_1}
% & *++[o][F-]{v_2}
% }}
% &
% \mbox{
% \xymatrix @-1pc {
% h\ar[d] & j\ar[ld] & i\ar[d] & null\ar[dl]\\
%  *++[o][F-]{v_0} \ar[r]^n
% & *++[o][F-]{v_1}
% & *++[o][F-]{v_2}
% }}
% &
% \mbox{
% \xymatrix @-1pc {
% C\ar[rd] & h\ar[d] & j\ar[ld] & i\ar[d] & null\ar[dl]\\
% & *++[o][F-]{v_0} \ar[r]^n
% & *++[o][F-]{v_1}
% & *++[o][F-]{v_2}
% }}
% \\
% \end{tabular}
% %&*++[o][F=]{3} \ar ‘ur^l[lll]‘^dr[lll]^b [lll]\ar ‘dr_l[ll] ‘_ur[ll] [ll] }
% {\small\caption{\label{Fi:BadmodelRefined}A depiction of $\cmodel_b'$, a bad witness model $R_1^1$.}}
% \end{figure}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
