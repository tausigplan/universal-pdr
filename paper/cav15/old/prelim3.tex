%!TEX root = ./paper.tex

\newcommand{\addprime}[1]{(#1)'}
\newcommand{\removeprime}[1]{(#1)^{\hat{'}}}

\section{Preliminaries}\label{Se:Prel}

In this section we introduce our assumptions regarding the analyzed programs and the terminology used in the {\UPDR} algorithm.

\para{Programs} Our analysis assumes that the program is comprised of a single loop.
Hence, it has the form \texttt{while}~$\kCond$~\texttt{do}~$\kCmd$.
We handle multiple loops and code sequences outside of loops
by rewriting the program and expressing its control structure using boolean variables.
%We handle code sequences outside of loops by computing their effect on the pre- and post-conditions.
In our examples, however, we will ignore these restrictions and use standard structured code.
% ir control structure  to
% This is not a real limitation, as the  To simplify the presentation, we describe the analysis assuming that the program contains a single
% loop \footnote{Technically, we can use boolean variables to rewrite any program which contains nested loops to one which has only one.}
% To simplify the presentation, we assume that the given pre- and post-conditions should hold right before the loop begins and right after it terminates, respectively.

%We use formulae written in a first-order logic %$\logic$ %with transitive closure
%to describe programs.

\para{Assertions}
We express properties of list-manipulation programs, e.g., their pre- and post-conditions, $\Pre$ and $\Post$, respectively,
using assertions written in a fragment of first-order logic with transitive closure.
These assertions are translated into equi-satisfiable first order logic formulae~\cite{CAV:IBINS13}.

The vocabulary used for expressing these formulae consists of constants and relation symbols.
For example, to  represent memory states of list manipulating programs,
%constants in $\vocabulary$ can be used to represent program variables, and binary relations in $\vocabulary$ can represent pointer fields.
we associate  every  program variable $\mathtt{x}$ with a constant $x$,
every boolean field $\mathtt{C}$ with a unary predicate $C(\cdot)$,
and every pointer field \texttt{n} with a binary predicate $n^*(\cdot,\cdot)$ which represents its reflexive transitive closure
($n$ is then defined by a formula over $n^*$, but is not included in the vocabulary).
We use a special constant $\mnull$ to denote the $\tnull$ value.
%that records the values of $\mathtt{n}$-pointer fields.
The vocabulary used to prove the \texttt{filter()}\ program is shown in \Cref{Ta:Voc}.
If we also wish to express the property that there is no memory leakage, we can add a unary predicate $alloc(\cdot)$.
\Omit{We note that the $\UPDR$ algorithm is parametric in the vocabulary, but requires that every satisfiable formula has a finite model.}


%\para{Vocabulary} The vocabulary $\vocabulary$ over which the transition system is defined %formulae are written
%is determined by the program and the pre- and post-conditions.
%Bad witness model that arise when refining $R_1$ of our running example.

In the following, we assume a fixed, but arbitrary, vocabulary $\vocabulary$.
We note that the $\UPDR$ algorithm is parametric in the vocabulary.

\paragraph{From Programs to Transition Systems.}
The semantics of a program is described by a \emph{transition system}, which consists of a set of states
and transitions between states. The pre- and post-conditions define a set of \emph{initial} states
and a set of \emph{bad} states, respectively.

\para{Programs States} We consider the states of the program at the beginning of each iteration of the loop.
A program state is represented by a first order model
$\cmodel= (\domain, \interpretation)$ over $\vocabulary$,
where $\domain$ is the \emph{universe} of the model,
and $\interpretation$ is the interpretation function of the constant and relation symbols in $\vocabulary$.
%Formulae describe both sets of program states and the transition relation of the program:
%Sets of states are described by formulae over $\vocabulary$.

We depict memory states as directed graphs (see \figref{Badmodel}).
Individuals in $\domain$, representing heap locations, are depicted as circles labeled by their name.
We draw an edge from the name of constant $x$ and of a unary predicate $C$ to an individual $v$
if $\cmodel \models x=v$ or $\cmodel \models C(v)$, respectively.
We draw an $n$-annotated edge between $v$ and $u$ if $\cmodel\models n(v,u)$.
%Note that the $\mnull$ value is represented by an individual.
%Note that $\cmodel_b$ is a bad witness model when $R_1=\mtrue$.

%\para{Transition Systems} The semantics of a program is described by a transition system.
%We use formulae written in a first-order logic over vocabulary $\vocabulary$ %$\logic$ %with transitive closure
%to describe transition systems.


%The transition relation is described by a two-vocabulary formula over vocabulary $\dvoc{\vocabulary}$.
%= \vocabulary \uplus \vocabulary'$,
%where vocabulary $\vocabulary' = \{ \letter' \mid \letter \in \vocabulary\}$
%is used to describe the states resulting from applying the transition relation.


\para{Transition Relation}
The transition relation of a program is described by a formula $\rho$ over a double vocabulary $\dvoc{\vocabulary}= \vocabulary \uplus \vocabulary'$,
where vocabulary $\vocabulary' = \{ \letter' \mid \letter \in \vocabulary\}$
is used to describe the target state of the transition.
Each transition of the program describes one execution of the loop body, $\kCmd$. i.e.
it relates a state at the beginning of an iteration
of the loop to the state at the end of the iteration. %, after the execution of the loop body $\kCmd$.
This is captured by defining $\rho \eqdef \kCond \land \wlp(\kCmd, \Identity)$, where $\wlp(\Cmd, \Identity)$ denotes the weakest
liberal precondition of the loop body (see \cite{CAV:IBINS13} for more details).
%A transition consists of a pair of states.
%We represent transitions (pairs of states) by models over a double vocabulary
%$\dvoc{\vocabulary} = \vocabulary \uplus \vocabulary'$,
%where vocabulary $\vocabulary' = \{ \letter' \mid \letter \in \vocabulary\}$
%is used to describe the target state of the transition.
%A model $\cmodel$ for the double vocabulary $\dvoc{\vocabulary}$ describes a pair of states (i.e., a transition),

Technically, a transition $(\cmodel_1,\cmodel_2)$ is
represented by a model $\cmodel \models \rho$ over the double vocabulary $\dvoc{\vocabulary}$
whose universe is the union of the universes of $\cmodel_1$ and $\cmodel_2$, and whose
interpretation of $\vocabulary$ is taken from $\cmodel_1$,
and the interpretation of $\vocabulary'$ is taken from $\cmodel_2$. By
abuse of notation, we write $(\cmodel_1,\cmodel_2) \models \rho$ to denote that $\cmodel \models \rho$.
Conversely, a model $\cmodel \models \rho$ over $\dvoc{\vocabulary}$ describes a transition whose source state
is the \emph{reduct} of $\cmodel$ to vocabulary $\vocabulary$,
and whose target state is the (unprimed version) of the reduct to $\vocabulary'$:
%Specifically, the source state of the transition described by model $\cmodel$ over $\dvoc{\vocabulary}$
%is the \emph{reduct} of $\cmodel$ to vocabulary $\vocabulary$,
%and the target state is the (unprimed version) of the reduct to $\vocabulary'$:
\begin{definition}[Reduct]
Let $\cmodel = (\domain,\interpretation)$ be a model over $\dvoc{\vocabulary}$, and let $\Sigma
\subseteq \dvoc{\vocabulary}$.
The \emph{reduct of $\cmodel$ to $\Sigma$} is the model
$\reduct{\cmodel}{\Sigma} = (\domain,\interpretation_i)$ where for every symbol $v \in \Sigma$,
$\interpretation_i(v) = \interpretation(v)$.
\end{definition}

%The transition relation of a program is described by a formula $\rho$ over $\dvoc{\vocabulary}$.
%%$\rho$ is used to describe the execution of the loop body, i.e. each transition $(\cmodel_1,\cmodel_2) \models \rho$
%%relates a state at the beginning of an iteration of the loop with a state at the end of the iteration.
%Technically, $\rho \eqdef \kCond \land \wlp(\kCmd, \Identity)$, where $\wlp(\Cmd, \Identity)$ denotes the weakest
%liberal precondition of the loop body (see \cite{CAV:IBINS13} for more details).

We say that a state $\cmodel_2$ is a successor of $\cmodel_1$
if $(\cmodel_1,\cmodel_2) \models \rho$. %, is a model of the two-vocabulary formula $\Trans$.
In this case we also refer to $\cmodel_1$ as a predecessor of $\cmodel_2$.
(We note that a state may have any finite number of successors but an infinite  number of predecessors.)

Given a formula $\varphi$ in the unprimed vocabulary, we use $(\varphi)'$ to denote the formula
obtained by replacing every constant and relation symbol in $\varphi$ with its primed version.

\para{Initial and Bad States}
Sets of states are described by formulae over $\vocabulary$.
The sets of \emph{initial} and \emph{bad} states are defined by the formulae
$\minit \eqdef \Pre$ and $\Bad \eqdef \neg \kCond \land \neg \Post$ respectively. That is, a state is initial if it satisfies the pre-condition, and
it is bad if it satisfies
the negation of the loop condition (which indicates termination of the loop) conjoined with the negation of the post-condition.
%The set of \emph{initial} states, $\minit$, is defined by the pre-condition of the program. Namely, $\minit \eqdef \Pre$.
%The set of \emph{bad} states is defined by $\Bad \eqdef \neg \kCond \land \neg \Post$.
This captures the requirement that when the loop
terminates the post-condition needs to hold.

In the \texttt{filter()}\ example, $\minit\eqdef i=h \land j = \mnull$ and
$\mbad \eqdef (i = \mnull) \land \neg((h \neq \mnull) \to (\forall v.\, \unrtc{h}{v}\to C(v)))$.
%$\mbad \eqdef \neg ((i=\mnull \land h \neq \mnull) \to (\forall v.\, \unrtc{h}{v}\to C(v)))$.
Note that these refer to the pre- and post-conditions that should hold right before the loop begins and right after it terminates, respectively.
%our running example, to simplify the presentation we allowed sequential code around the loop.
%We therefore transform the pre- and post-conditions of the procedure (and accordingly $\minit$ and $\mbad$) to conditions
%that should hold right before the loop begins and right after it terminates, respectively, using strongest postcondition and weakest precondition computations.
%We then get $\minit\eqdef i=h \land j = \mnull$ and $\mbad \eqdef \neg ((i=\mnull \land h \neq \mnull) \to (\forall v.\, \unrtc{h}{v}\to C(v)))$.
Here a state is bad if $i = \mnull$ (i.e., it occurs when the loop terminates) and \texttt{h} points to a non-empty list which contains an element which does not have property $C$.

%\TODO{To simplify the presentation, we transform the pre- and post-conditions of the procedure to conditions that should hold right before the loop begins and right after it terminates, respectively, using strongest postcondition and weakest precondition computations.
%We denote the loop pre-condition by $\minit$ and the negation of the loop condition (which indicates termination of the loop) conjoined with the negation of the loop post-condition by $\mbad$.
%We say that a state is an \emph{initial} state if it satisfies $\minit$ and \emph{bad} if it satisfies $\mbad$.
%In our running example, $\minit\eqdef i=h \land j = \mnull$ and $\mbad \eqdef \neg ((i=\mnull \land h \neq \mnull) \to (\forall v.\, \unrtc{h}{v}\to C(v)))$.
%Here a state is bad if it occurs when the loop terminates (i.e., the loop condition does not hold) and \texttt{h} points to a non-empty list which contains an element which does not have property $C$.
%}

\paragraph{Verification Problem.}
Let $\TS = (\minit,\rho)$ be a \emph{transition system} over vocabulary $\vocabulary$,
where $\minit$ is a formula over $\vocabulary$ used to denote
the initial states, and $\rho$ is a formula over $\dvoc{\vocabulary}$, used to denote the transition relation.
Further, let $\Prop \eqdef \neg \Bad$ be a \emph{safety property}
describing a set of states that should include all reachable states.

We say that a state is reachable by $i$ steps of $\rho$ (or $i$-reachable for short)
if it can be reached by $i$ applications of $\rho$ starting from some initial state.
We denote the set of states that are reachable by at most $i$ steps of $\rho$ by $\Reach_i$.

We say that $\TS$ \emph{satisfies} $\Prop$ if no state that satisfies $\Bad$ is reachable from $\Init$ via any number of steps of $\rho$.

\begin{definition}[Invariant]
Let $\TS = (\Init, \Trans)$ be a transition system and $\Prop$ a safety
property over $\vocabulary$.
A formula $\IndInv$ is a \emph{safety inductive invariant} (invariant, in
short) for $\TS$ and $\Prop$ if
\begin{inparaenum}[(i)]
\item $\Init \implies \IndInv$, and
\item $\IndInv \wedge \Trans \implies \IndInv'$, and
\item $\IndInv \implies \Prop$.
\end{inparaenum}
\end{definition}
If there exists an invariant for $\TS$ and $\Prop$, then $\TS$ satisfies $\Prop$.
An invariant is \emph{universal} if it contains no existential quantifier.




\def\arraystretch{1.5}%  1 is the default, change whatever you need
\begin{figure}[t]
\centering
\begin{tabular}{|cc|cc|}
\hline
~($\cmodel_b$)
&
\mbox{\xymatrix @-1pc {
h\ar[d] & j\ar[d] & i\ar[d] & null\ar[dl]\\
*++[o][F-]{v_0} \ar[r]^n
& *++[o][F-]{v_1}
& *++[o][F-]{v_2}
}}
&
~($\cmodel_b'$)
&
\mbox{
\xymatrix @-1pc {
h\ar[d] & j\ar[rd] & i\ar[d] & null\ar[dl]\\
*++[o][F-]{v_0} \ar[r]^n
& *++[o][F-]{v_1}
& *++[o][F-]{v_2}
}}
\\[8Ex]
\hline
~($\cmodel_b''$)
&
\mbox{
\xymatrix @-1pc {
h\ar[d] & j\ar[ld] & i\ar[d] & null\ar[dl]\\
 *++[o][F-]{v_0} \ar[r]^n
& *++[o][F-]{v_1}
& *++[o][F-]{v_2}
}}
&
~($\cmodel_b'''$)
&
\mbox{
\xymatrix @-1pc {
C\ar[rd] & h\ar[d] & j\ar[ld] & i\ar[d] & null\ar[dl]\\
& *++[o][F-]{v_0} \ar[r]^n
& *++[o][F-]{v_1}
& *++[o][F-]{v_2}
}}\\[8Ex]
\hline
\end{tabular}
%&*++[o][F=]{3} \ar ‘ur^l[lll]‘^dr[lll]^b [lll]\ar ‘dr_l[ll] ‘_ur[ll] [ll] }
{\small\caption{\label{Fi:Badmodel}Graphical depiction of models found during the analysis of the running example}}
\vspace{-2ex}
\end{figure}
\def\arraystretch{1}%  1 is the default, change whatever you need



%\subsection{EPR}


\begin{table}[t]
\centering
{\small\caption{\label{Ta:Voc}%
  The vocabulary of formulae  used to describe properties of singly linked lists.
  We reason about list-manipulating programs using logic $\EAR$~\cite{CAV14:Itzhaky},
  Hence, the values of pointer fields is recorded indirectly using their transitive closure.
}}
$
\begin{array}{l|l||l|l}
%\scriptstyle
\textbf{Constants} & \textbf{Description} &
\textbf{Relations} & \textbf{Description} \\
\hhline{--||--}
   x     & \text{value of variable $\mathtt{x}$}
 & C(v)    & \text{element $v$ has property $C$} \\
 \mnull  & \text{the $\tnull$ value}
 & \unrtc{u}{v} & \text{a (possibly empty) path of $n$-fields from $u$ to $v$} \\
% \hline
% \text{Shorthands}
%  & \nfld{u}{v} & \text{element $v$ is the $n$-successor of element $v$} \\
\end{array}
$
\end{table}
