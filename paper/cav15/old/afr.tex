\section{Property-Directed  Reachability for Linked-List Programs}
\label{Se:AF}
\vspace{-1.0ex}

In this section, we describe how PDR$_\AbsDomain(\Init,
\rho, \Bad)$ described in \algref{PDR} can be instantiated for
verifying linked-list programs.
The key insight is the use of the
recently developed reachability logics for expressing properties of linked
lists \cite{CAV:IBINS13}.


\subsection{Reachability Logics}
We use two related logics for expressing properties of linked data structures:
\begin{itemize}
  \item
    $\AFR$ is a decidable fragment of first-order logic with transitive
    closure ($\FOTC$), which is an alternation-free quantified logic.
    This logic is used to express the abstraction predicates $\AbsPreds$,
    and pre- and post-conditions.
   It is closed under negation, and decidable for both satisfiability and validity.
  \item
    $\EAR$ allows there to be universal quantifiers inside of existential ones.
    It is used to define the transition formulas of statements that allocate new nodes
    and dereference pointers.
    This logic is not closed under negation, and is only decidable for satisfiability.
    We count on the fact that transition formulas are only used in a positive form 
    in the satisfiability queries in \algref{PDR}.
\end{itemize}
Although $\AFR$ is used as the language for defining the predicates in
$\AbsPreds$, the $\wlp$ rules go slightly outside of $\AFR$,
producing $\EAR$ formulas (see \tabref{Wprules} below).

\begin{definition}\label{de:EAR}\textbf{($\EAR$)}
A \textbf{term}, $t$, is a variable or constant symbol.
An \textbf{atomic formula} is one of the following:
(i)~$t_1 = t_2$;
(ii)~$r(t_1, t_2, \ldots, t_a)$ where $r$ is a relation symbol of arity $a$
(iii)~A \textbf{reachability constraint} $t_1 \B{f^*} t_2$, where $f$ is a function symbol.
A \textbf{quantifier-free formula} ($\QFR$) is a boolean combination of atomic formulas.
A \textbf{universal formula} begins with zero or more universal
quantifiers followed by a quantifier-free formula.
An \textbf{alternation-free formula} ($\AFR$) is a boolean combination of universal formulas.
\textbf{$\EAR$} consists of formulas with quantifier-prefix $\exists^* \forall^*$.

In particular, $\QFR \subset \AF \subset \EAR$.
\qed
\end{definition}


Technically, $\EAR$ forbids \emph{any}
use of an individual function symbol $f$; however, when $f$
defines an acyclic linkage chain---as in acyclic singly linked and
doubly linked lists---$f$ can be defined in terms of $f^*$
by using universal quantification to express that an element is
the closest in the chain to another element.
This idea is formalized by showing that for all $\alpha$ and $\beta$,
$f(\alpha) = \beta \liff E_f(\alpha, \beta)$ where $E_f$ is defined
as follows:
\begin{equation}
  \label{Eq:fViafStar}
  E_f(\alpha, \beta) \eqdef \alpha \B{f^+} \beta  \land \forall \gamma: \alpha \B{f^+} \gamma \implies \beta \B{f^*} \gamma,
\end{equation}
where $\alpha \B{f^+} \beta ~\eqdef~ \alpha \B{f^*} \beta \land \alpha \neq \beta$.
However, because of the quantifier in \equref{fViafStar},
the right-hand side of \equref{fViafStar} can only be used
in a context that does not introduce a quantifier alternation
(so that the formula remains in a decidable fragment of $\FOTC$).


\begin{table}[t]
\resizebox{\textwidth}{!}{
$\begin{array}{||l|l||}
\hhline{|t:==:t|}
\textbf{Name} & \textbf{Formula}\\
\hline
\field{f}{x}{y} & E_f(x, y)\\
\ls{f}{x}{y}  & \forall \alpha, \beta:
    \frtcBetween{x}{\alpha}{y} \land \frtc{\beta}{\alpha}
    \implies (\frtc{\beta}{x} \lor \frtc{x}{\beta}) \\
\hline
\stable{f}{h} & \forall \alpha: \frtc{h}{\alpha} \implies \alloc(\alpha) \\
\hline
\reverse{f}{b}{x}{y}  & \forall \alpha,\beta:
    \left(\begin{array}{c@{\hspace{.75ex}}l}
            & \alpha\neq\tnull \land \beta\neq\tnull \\
      \land & \frtcBetween{x}{\alpha}{y} \land \frtcBetween{x}{\beta}{y}
    \end{array}\right)
    \limplies \big(\frtc\alpha\beta \liff \brtc\beta\alpha\big)\\
\sorted{f}{x}{y}  & \forall \alpha,\beta:
    \left(\begin{array}{c@{\hspace{.75ex}}l}
            & \alpha\neq\tnull \land \beta\neq\tnull \\
      \land & \reachField{f}{x}{\alpha} \land  \reachField{f}{\alpha}{\beta} \land \reachField{f}{\beta}{y}
    \end{array}\right)
    \limplies \dle(\alpha,\beta) \\
\hhline{|b:==:b|}
\end{array}$
}
\vspace{.75ex}
{\small\caption{\label{Ta:PredFormulas}%
\AFR\ formulas for the derived predicates shown in \tabref{Pred}.
$f$ and $b$ denote pointer fields.
$\dle$ is an uninterpreted predicate that denotes a total order on the data values.
The intention is that $\dle(\alpha, \beta)$ holds whenever $\alpha\texttt{->}d \leq \beta\texttt{->}d$, where $d$ is the data field.
We assume that the semantics of $\dle$ are enforced by an appropriate total-order background theory.
}}
\vspace{-2ex}
\end{table}

\subsubsection*{A Predicate Abstraction Domain that uses $\AFR$.}
The abstraction predicates used for verifying properties of linked list programs
were introduced informally in \tabref{Pred}.
\tabref{PredFormulas} gives the corresponding formal definition of
the predicates as $\AF$ formulas.
Note that all four predicates defined in \tabref{PredFormulas} are quantified.
(The quantified formula for $E_f$ is given in \equref{fViafStar}.)
In essence, we use a template-based approach for obtaining quantified
invariants: the discovered invariants have a quantifier-free
structure, but the atomic formulas can be quantified $\AF$ formulas.


%\subsection{$\EAR$ Satisfies the Requirements of PDR algorithm}

We now show that the $\EAR$ logic satisfies requirements \ref{It:Decidable} and
\ref{It:ConcreteSemantics} for the PDR algorithm stated in \secref{PropertyDirectedReachability}.


\subsubsection*{Decidability of $\EAR$.}
To satisfy requirement \ref{It:Decidable} stated in
\secref{PropertyDirectedReachability}, we have to show that $\EAR$ is decidable for satisfiability.

$\EAR$ is decidable for satisfiability because any formula in this logic can
be translated into the ``effectively propositional'' decidable logic
of $\exists^*\forall^*$ formulas described by Piskac et al.\ \cite{JAR:PdMB10}.
$\EAR$ includes relations of the form $f^*$ (the reflexive transitive
closure of a function symbol $f$),
but only allows limited use of $f$ itself.

Every $\EAR$
formula can be translated into an $\exists^{*}\forall^{*}$ formula
using the following steps~\cite{CAV:IBINS13}: (i)~add a new
uninterpreted relation $R_f$, which is intended to represent reflexive
transitive reachability via $f$; (ii)~add the consistency rule
$\Tlinord$ shown in \tabref{Rtnext}, which asserts that $R_f$ is a
partial order, i.e., reflexive, transitive, acyclic, and
linear;\footnote{Note that the order is a partial order and not a
  total order, because not every pair of elements must be ordered.  }
and (iii)~replace all occurrences of $t_1 \B{f^*} t_2$ by $R_f(t_1,
t_2)$.  (By means of this translation step, acyclicity is built into
the logic.)

\begin{table}[t]
\[
\begin{array}{||cl|r||}
\hhline{|t:===:t|}
        & \forall \alpha: R_f(\alpha,\alpha)       & \mbox{reflexivity} \\
  \land & \forall \alpha, \beta, \gamma: R_f(\alpha, \beta) \land R_f(\beta, \gamma) \to
            R_f(\alpha, \gamma)                    & \mbox{transitivity} \\
  \land & \forall \alpha,\beta: R_f(\alpha,\beta) \land R_f(\beta,\alpha)\to \alpha = \beta & \mbox{acyclicity} \\
  \land & \forall \alpha, \beta, \gamma: R_f(\alpha, \beta) \land R_f(\alpha, \gamma) \to  (R_f(\beta, \gamma) \lor R_f(\gamma, \beta)) & \mbox{linearity}\\
\hhline{|b:===:b|}
\end{array}
\]
{\small\caption{\label{Ta:Rtnext}%
A universal formula, $\Tlinord$, which asserts that all points reachable from a given
point are linearly ordered.
}}
\vspace{-2ex}
\end{table}

%The following proposition is proven in \cite[Proposition 3, Appendix A.1]{TR:IBINS13}
\begin{proposition}[Simulation of \EAR]
 \label{pr:simulation}
Consider \EAR\ formula $\varphi$ over vocabulary $\voc=\B{\constants, \functions, \relations}$.
Let $\varphi' \eqdef \varphi[R_f(t_1, t_2)/ t_1 \B{f^*} t_2]$.
Then $\varphi'$ is a first-order formula over vocabulary
$\voc'=\B{\constants, \emptyset, \relations \cup \{R_f \colon f \in \functions}$,
and $\Tlinord \land \varphi'$ is satisfiable if and
only if the original formula $\varphi$ is satisfiable.
\end{proposition}
This proposition is the dual of \cite[Proposition 3, Appendix A.1]{TR:IBINS13} for validity of $\forall^{*} \exists^{*}$ formulas.




%\subsection{Transition Formulas}
%
%It is also possible to generate \AER formula which expresses the effect of arbitrary loop free code.
%These formulas employ several vocabularies to denote the state before and after the code.
%\tabref{TransitionFormulas} defines transition formulas for standard atomic commands.
%Each of the commands change some of the constants and reachability constrains.
%\begin{table}
%\[
%\begin{array}{||l|l|l||}
%\hline\hline
%\textbf{Command} & \textbf{Change} & \textbf{Transition Formula}\\
%\hline
%\texttt{assume}~\varphi & \emptyset & \varphi\\
%\texttt{x := null} & \{x\} & x' = \tnull\\
%\texttt{x := malloc()} & \{x, \alloc\} &  x' \neq \tnull  \land \neg \alloc (x') \land \alloc' (x') \\
%\texttt{x := y} & \{x\} & x' = y\\
%\texttt{x := y.f} & \{x\} & y \neq \tnull \land \alloc(y) \land E_f(y, x')\\
%\texttt{x.f := y} &  \{f\} & x \neq \tnull \land \alloc(x) \land \forall \alpha, \beta : \reachField{f'}{\alpha}{\beta} \leftrightarrow\left \{
%                    \begin{array}{lr}
%                    \reachField{f}{\alpha}{\beta} &  \neg (\reachField{f}{\alpha}{x} \land x\B{f^+}\beta)\\
%                    \reachField{f}{\alpha}{x}  \land  \reachField{f}{x}{\beta}  & \mbox{otherwise} \end{array} \right .\\
%\texttt{free(x)} & \{\alloc\} &\alloc(x) \land \alloc'(x)\\
%\hline\hline
%\end{array}
%\]
%{\small\caption{\label{Ta:TransitionFormulas}%
%Transition Formulas for imperative statements.
%$\alloc$ stands for a memory location which has allocated and not-freed.
%$E_f(y, x)$ is the universal formula defined in\equref{fViafStar}.
%}}
%\end{table}
%
%The following Lemma establishes the soundness and completeness of the transition formulas.
%\begin{lemma}
%For every structure $\sigma$ and $\sigma'$ and atomic Command, $\Cmd$ of the form shown in
%\tabref{TransitionFormulas},
%the execution of $\Cmd$ on $\sigma$ may yield a state $\sigma'$ if and only if
%$\sigma, \sigma' \models \rho$ where $\rho$ is the corresponding transition formula.
%\end{lemma}
%
%We assume that transition formulas of compound statements $C_1 ; C_2$ and $C_1 | C_2$ are defined in the standard  way, i.e.,
%$\rho(C_1 ; C_2) = \rho(c_1) \land (\rho(c_2))'$,
% where $'$ denotes renaming the input and output vocabularies of $\rho(C_2)$
%so that $\rho(C_1 ; C_2) = \rho(C_1) \land (\rho(C_2))'$he output vocabulary of $C_1$ matches the input vocabulary of $C_2$.
%Also, $\rho(C_1 | C_2) = \rho(C_1) \lor \rho(C_2)'$.
%Finally, for a while command, $C= \texttt{while~ \Cond~ do~ \Cmd'}$, we set $\rho(C) = \rho(\texttt{assume}~\Cond ~; ~\rho(\Cmd))$
%TODO NESTED LOOPS.


\begin{table}[t]
\resizebox{\textwidth}{!}{
$\begin{array}{||l|l||}
\hhline{|t:==:t|}
\textbf{Command~C} & \wlp(C, Q)\\
\hline
\texttt{assume}~\varphi  & \varphi \implies Q\\
%\texttt{assert}~\varphi  & \varphi \land Q\\
\texttt{x = y} & \substitute{Q}{x}{y}\\
\texttt{x = y->f} & y \neq \tnull \land \exists \alpha:
                            (E_f(y, \alpha) \land \substitute{Q}{x}{\alpha})\\
\texttt{x->f = null} &   x \neq \tnull \land \substitute{Q}{\alpha \B{f^*} \beta}{\alpha \B{f^*}\beta \land (\lnot \alpha \B{f^*}x \lor \beta \B{f^*}x)}\\
\texttt{x->f = y}  &  x \neq \tnull \land \substitute{Q}{\alpha \B{f^*} \beta}{\alpha \B{f^*}\beta \lor (\alpha \B{f^*}x \land y \B{f^*}\beta)}\\
\hline
\texttt{x = malloc()} & \exists \alpha: \neg \alloc(\alpha)  \land \substitute{Q}{\alloc(\beta))}{(\alloc(\beta) \lor (\beta = \alpha \land \beta = x))}\\
\texttt{free(x)} &  \alloc(x) \land \substitute{Q}{\alloc(\beta))}{(\alloc(\beta) \land \beta \neq x)}\\
\hhline{|b:==:b|}
\end{array}$
}
\vspace{.75ex}
{\small\caption{\label{Ta:Wprules}%
Rules for $\wlp$ for atomic commands.
$\alloc$ stands for a memory location that has been allocated
and not subsequently freed.
$E_f(y, \alpha)$ is the universal formula defined in \equref{fViafStar}.
$\substitute{Q}{x}{y}$ denotes $Q$ with all occurrences of atomic formula $x$
replaced by $y$.
}}
\vspace{-2ex}
\end{table}

\subsubsection*{Axiomatic specification of concrete semantics in $\EAR$.}
To satisfy requirement \ref{It:ConcreteSemantics} stated in
\secref{PropertyDirectedReachability}, we have to show that the transition relation for each
statement $\Cmd$ of the programming language can be expressed as a
two-vocabulary formula $\rho \in \EAR$.
Let $\wlp(\Cmd, Q)$ be the
weakest liberal precondition of command $\Cmd$ with respect 
$Q \in \EAR$. 
Then, the transition formula for command $\Cmd$ is $\wlp(\Cmd,
\Identity)$, where $\Identity$ is a two-vocabulary formula that
specifies that the input and the output states are identical, i.e.,
\begin{equation*}
  \Identity \eqdef \Land_{c \in \constants} c = c' \land \Land_{f \in \functions} \forall \alpha, \beta: \alpha \B{f^*} \beta \impliesBothWays  \alpha \B{f'^*} \beta .
\end{equation*}
\vspace{-0.5em}

To show that the concrete semantics of linked list programs
can be expressed in $\EAR$, we have to prove that $\EAR$ is closed under
$\wlp$; that is, for all commands $\Cmd$ and $Q \in \EAR$, $\wlp(\Cmd,
Q) \in \EAR$.

\tabref{Wprules} shows rules for computing $\wlp$ for atomic commands.
Note that pointer-related rules in \tabref{Wprules} each include a
memory-safety condition to detect \tnull-dereferences.
For instance, the rule for ``\lstinline{x->f = y}'' includes
the conjunct ``$x \neq \tnull$'';
if, in addition, we wish to detect accesses to unallocated memory,
the rule would be extended with the conjunct ``$\alloc(x)$''.

The following lemma establishes the soundness and completeness of the
$\wlp$ rules.
%\textbf{We need to say something about the proof of the next lemma. Perhaps
%cite the correct theorem from \cite{CAV:IBINS13}?}
\begin{lemma}
Consider a command $C$ of the form defined in \tabref{Wprules}
and postcondition $Q$.
Then, $\sigma \models \wlp(C, Q)$ if and only if the execution of $C$
on $\sigma$ can yield a state $\sigma'$ such that $\sigma' \models Q$.
\end{lemma}
This lemma is the dual of \cite[Prop.\ 1, App.\ A.1]{TR:IBINS13} for
validity of $\forall^{*} \exists^{*}$ formulas.

Weakest liberal preconditions of compound commands $C_1 ; C_2$
(sequencing) and $C_1 | C_2$ (nondeterministic choice) are defined
in the standard way, i.e.,

{\small
\noindent
\vspace{-1ex}
\begin{equation*}
\begin{array}{c@{\hspace{5.5ex}}c}
  \wlp(C_1 ; C_2, Q) \eqdef \wlp(C_1, \wlp(C_2, Q))
  &
  \wlp(C_1 | C_2, Q) \eqdef \wlp(C_1, Q) \land \wlp(C_2, Q)
\end{array}
\vspace{-3ex}
\end{equation*}
}

Consider a program with a single loop ``\texttt{while}~\Cond~\texttt{do}~\Cmd''.
\algref{PDR} can be used to prove whether or not a precondition $\Pre
\in \AF$ before the loop implies that a postcondition $\Post \in \AF$
holds after the loop, if the loop terminates: we supply \algref{PDR} with $\Init
\eqdef \Pre$, $\rho \eqdef \Cond \land \wlp(\Cmd, \Identity)$ and
$\Bad \eqdef \neg \Cond \land \neg \Post$.
Furthermore, memory safety can be enforced on the loop body by setting
$\Bad \eqdef (\lnot \Cond \land \neg \Post) \lor (\Cond \land \neg
\wlp(\Cmd, \true))$.  
\Omit{
Note that the $\Pre$ and $\Post$ formulas are
defined using $\AF$ in \secref{EmpiricalEvaluation} for each of the
linked-list programs.
}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
