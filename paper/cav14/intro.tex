\section{Introduction}
\label{Se:Introduction}

\vspace{-1.0ex}
% 1. Describe the problem
The goal of our work is to automatically generate quantified
invariants for programs that manipulate
singly-linked and doubly-linked list
data structures.  For a correct program, the invariant generated
ensures that the program has no memory-safety violations,
such as null-pointer dereferences, and that
data-structure invariants are preserved.  For a program in which it is
possible to have a memory-safety violation or for a data-structure
invariant to be violated, the algorithm produces a concrete
counterexample.
% NSB: Technically, theorem 1 says something weaker.
Although in this paper we mainly discuss
memory-safety properties and data-structure invariants, the technique
can be easily extended to other correctness properties (see
\secref{EmpiricalEvaluation}).

To the best of our knowledge, our method represents the first
shape-analysis algorithm that is capable of (i)~reporting concrete
counterexamples, or alternatively (ii)~establishing that the abstraction
in use is not capable of proving the property in question.
This result is achieved by combining several existing ideas in a new
way:
\begin{itemize}
  \item
    The algorithm uses a predicate-abstraction domain \cite{CAV:GS97}
    in which
    quantified
    predicates express properties of singly and doubly linked
    lists.
    In contrast to most recent work, which uses restricted forms of
    predicate abstraction---such as Cartesian abstraction
    \cite{TACAS:BPR01}---our algorithm uses full predicate abstraction
    (i.e., the abstraction uses arbitrary Boolean combinations of predicates).
  \item
    The abstraction predicates and language semantics are expressed in
    recently developed \emph{reachability logics}, $\AFR$ and $\EAR$, respectively, which
    are decidable using a reduction to SAT \cite{CAV:IBINS13}.
    % NSB: Standard SAT solvers can't solve EPR formulas, so it is
    % wrong to say 'reduction to standard SAT solvers'
  \item
    The algorithm is property-directed---i.e., its choices
    are driven by the memory-safety properties to be proven.
    In particular, the algorithm is based on IC3 \cite{VMCAI:Bradley11},
    which we here refer to as \emph{property-directed reachability} (PDR).
    %NSB: removed self-reference because we don't do Horn clauses here.
\end{itemize}

PDR integrates well with full predicate abstraction:
in effect, the analysis obtains the same precision as
the best abstract transformer for full predicate abstraction,
without ever constructing the transformers explicitly.
In particular, we cast PDR as a \emph{framework} that is parameterized on
\begin{itemize}
\item the logic $\LogicLang$ in which the semantics of
    program statements are expressed, and
  \item the finite set of predicates that define the abstract domain
    $\AbsDomain$ in which invariants can be expressed. An element of
    $\AbsDomain$ is an arbitrary Boolean combination of the
    predicates.
\end{itemize}
Furthermore, our PDR framework is \emph{relatively complete with respect
to the given abstraction}. That is, the analysis
is guaranteed to terminate and
either
(i)~verifies the given property, (ii)~generates a concrete
counterexample to the given property, or (iii)~reports
that the abstract domain is not expressive enough to establish
the proof.
%NSB: this is repeated too many times in the paper.
% It got to the point of being over-repetitious to me.
% Why not highlight Theorem 1 early on and refer to it?
Outcome (ii) is possible because the ``frame'' structure
maintained during PDR can be used to build a trace formula;
if the formula is satisfiable, the model can be presented to the
user as a concrete counterexample. Moreover, if the analysis
fails to prove the property or find a concrete counterexample (outcome (iii)),
then there is no way to express an inductive invariant that
establishes the property in question using a Boolean combination of
the abstraction predicates.  
Note that outcome (iii) is a much stronger guarantee than what other
approaches provide in such cases when they neither succeed nor give a concrete
counterexample.

Key to instantiating the PDR framework for shape analysis was a
recent development of the $\AFR$ and $\EAR$ logics for expressing properties of
linked lists \cite{CAV:IBINS13}. %In the PDR framework,
$\AFR$ is used to define abstraction predicates,
and $\EAR$ is used to express the language semantics.
$\AF$ is a decidable, alternation-free fragment of first-order logic
with transitive closure ($\FOTC$).  When applied to list-manipulation
programs, atomic formulas of $\AF$ can denote reachability relations
between memory locations pointed to by pointer variables, where
reachability corresponds to repeated dereferences of $\nextf$ or
$\prevf$ fields.  One advantage of $\AF$ is that it does not require
any special-purpose reasoning machinery:
an $\AF$ formula can be
converted to a formula in ``effectively propositional'' logic,
which can be reduced to SAT solving.
That is, in
contrast to much previous work on shape analysis, our method makes use
of \emph{a general purpose SMT solver}, Z3~\cite{TACAS:deMB08}
 (rather than specialized
tools developed for reasoning about linked data structures, e.g.,
\cite{TOPLAS:SRW02,TACAS:DOY06,CAV:BCCDOWY07,SAS:GMP13}).


The main restriction in $\AF$ is that it allows the use of a relation
symbol $f^*$ that denotes the transitive closure of a function symbol
$f$, but only limited use of $f$ itself. Although this restriction can
be somewhat awkward, it is mainly a concern for the analysis
designer (and the details have already been worked out in
\cite{CAV:IBINS13}).  As a language for expressing invariants, $\AF$
provides a fairly natural abstraction, which means that analysis
\emph{results} should be understandable by non-experts (see
\secref{Overview}).\footnote{ By a
  ``non-expert'', we mean someone who has no knowledge of either the
  analysis algorithm,
% NSB: I bet the only users ever of this will in fact understand the algorithm.
or the abstraction techniques used inside the
  algorithm.  }

% 2. State your contributions
Our work represents the first algorithm for shape analysis that either
(i)~succeeds, (ii)~returns a concrete counterexample, or
(iii)~returns an abstract trace showing that the abstraction in use is
 not capable of proving the property in question.
The specific contributions of our work include
\begin{itemize}
  \item
    A framework, based on the PDR algorithm, for finding an inductive
    invariant in a certain logic fragment (abstract domain) that
    allows one to prove that a given pre-/post-condition holds or find
    a concrete counter-example to the property, or, in the case of a
    negative result, the information that there is no inductive
    invariant expressible in the abstract domain
    (\secref{PropertyDirectedReachability}).
  \item
    An instantiation of the framework for finding invariants of
    programs that manipulate singly-linked or doubly-linked lists.
    This instantiation uses $\AF$ to define a simple
    predicate-abstraction domain, and is the first application of PDR
    to establish quantified invariants of programs that manipulate
    linked lists (\secref{AF}).
  \item
    An empirical evaluation showing the efficacy of the PDR framework
    for a set of linked-list programs (\secref{EmpiricalEvaluation}).
\end{itemize}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
