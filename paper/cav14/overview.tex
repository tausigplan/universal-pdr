\section{A Motivating Example}
\label{Se:Overview}

\lstset{
  language=C,
  columns=fullflexible,
  basicstyle=\footnotesize\ttfamily,
  numbers=none,
  numberstyle=\scriptsize,
  stepnumber=1,
  breaklines=true,
}

%\begin{figure}[t]
%\centering
%\begin{tabular}{l}
%\begin{lstlisting}[mathescape]
%void reverse(List h) {
%  $\textbf{Requires:~} \nrtc{h}{\tnull}$
%  $\textbf{Ensures:~} \nrtc{q}{\tnull}$
%  p = h;
%  q = null;
%  while (p != null) {
%    t = p->n; p->n = q; q = p; p = t;
%  }
%}
%\end{lstlisting}
%\end{tabular}
%\vspace{-2.0ex}
%{\small\caption{\label{Fi:Reverse}%
%  A procedure to reverse a singly-linked list pointed by \lstinline{h},
%  where the field \lstinline{n} is the next-pointer.
%}}
%\vspace{-1.0ex}
%\end{figure}

\begin{table}[t]
\centering
\resizebox{.75\textwidth}{!}{
$\begin{array}{||l|l|c||}
\hhline{|t:===:t|}
\textbf{Name} & \textbf{Description} & \textbf{Mnemonic}\\
\hhline{||---||}
x = y   & \text{equality} & \\
\field{f}{x}{y} & x\texttt{->}f = y & \\
\reachField{f}{x}{y} & \text{an}~f~\text{path from}~x~\text{to}~y & \\
%\preachField{f}{x}{y} & \text{An}~f~\text{non-empty path from}~x~\text{to}~y & \\
\ls{f}{x}{y}  & \text{unshared}~f~\text{linked-list segment between}~x~\text{and}~y & \\
\hhline{||---||}
\alloc(x)   & x~\text{points to an allocated element} & St \\
\stable{f}{h}  & \text{any $f$-path from $h$ leads to an allocated element}& St \\
\hhline{||---||}
\reverse{f}{b}{x}{y}  & \text{reversed}~f/b~\text{linked-list segment between}~x~\text{and}~y & R \\
\hhline{||---||}
\sorted{f}{x}{y}  & \text{sorted}~f~\text{list segment between}~x~\text{and}~y  & S \\
\hhline{|b:===:b|}
\end{array}$
}
\vspace{.75ex}
{\small\caption{\label{Ta:Pred}%
  Predicates for expressing various properties of linked lists whose
  elements hold data values.
  $x$ and $y$ denote program variables that point to list elements or \tnull.
  $f$ and $b$ are parameters that denote pointer fields.
  (The mnemonics are referred to in \tabref{Experiments}.)
}}
\vspace{-3ex}
\end{table}

\vspace{-2.0ex}
%%\subsubsubsection{Analysis goals.}
To illustrate the analysis, we use the procedure \lstinline{insert}, shown in \figref{Insert}, that
inserts a new element pointed to by
\lstinline{e} into the non-empty, singly-linked list pointed to by \lstinline{h}.
\lstinline{insert} is annotated with a pre-condition and a post-condition.
\begin{figure}[t]
\begin{lstlisting}[mathescape]
void insert(List e, List h, List x) {
  $\textbf{Requires:~} h \neq \tnull \land \ntc{h}{x} \land \nrtc{x}{\tnull}  \land e \neq \tnull \land \nsingl{e}{\tnull} \land \neg \nrtc{h}{e}$
  $\textbf{Ensures:~} h \neq \tnull \land \nrtc{h}{e} \land \nsingl{e}{x} \land \nrtc{x}{\tnull}$
  p = h;
  q = null;
  while (p != x && p != null) {
    q = p;
    p = p->n;
  }
  q->n = e;
  e->n = p;
}
\end{lstlisting}
\vspace{-2ex}
{\small\caption{\label{Fi:Insert}%
  A procedure to insert the element pointed to by \lstinline{e} into
  the non-empty, singly-linked list pointed by \lstinline{h}.
  %The while-loop uses the trailing-pointer idiom: \lstinline{q} is one
  %step behind \lstinline{p}.
}}
%\vspace{-.5ex}
\end{figure}

%Because the while-loop in \lstinline{insert} uses the trailing-pointer
%idiom---i.e., \lstinline{q} is one step behind
%\lstinline{p}.
%\lstinline{reverse},
%shown in \figref{Reverse}, which uses destructive updating to reverse
%the direction of the edges in a singly-linked list.
%At any moment, \lstinline{q} points to a list segment that has already
%been reversed, and \lstinline{p} points to the remaining tail.
%In this section, our goal is to establish that \lstinline{reverse}
%has no memory-safety violations and that acyclic-linked-list
%data-structure invariants are preserved.

\tabref{Pred} shows a set of predicates for expressing properties of
linked lists whose elements hold data values.
The predicates above the horizontal line in \tabref{Pred} are inspired
by earlier work on shape analysis~\cite{kn:Hendren} and separation
logic \cite{LICS:Reynolds02}.

%\lstinline{Reverse} is annotated with a pre-condition and a
%post-condition, but not a loop invariant.
Given an input procedure, 
 optionally
annotated with a pre-condition $\Pre$ and post-condition $\Post$ (expressed as
formulas over the same vocabulary of predicates);
the goal of the analysis is to compute 
 an invariant for the head of each loop 
\footnote{The current
  implementation supports procedures with only a single loop; however,
  this restriction is not an essential limitation of our technique.
}
expressed as
a CNF formula over the predicates given in \tabref{Pred} (and
their negations).

The task is not trivial because
(i) a loop invariant may be more complex than a program's
pre-condition or post-condition, and
(ii) it is infeasible to enumerate all the potential invariants
expressible as CNF formulas over the predicates shown in \tabref{Pred}.
For instance, there are $6$ variables in \lstinline{insert}
(including \lstinline{null}), and hence $2^{6 \times 6 \times 6}$ clauses
can be created from the $36$ possible instantiations of each of the $6$
binary predicates in \tabref{Pred}.
Therefore, the number of candidate invariants that can be formulated
with these predicates is more than $2^{2^{6 \times 6 \times 6}}$.
It would be infeasible to investigate them all explicitly.
%NSB: in light of that SLAM (and BLAST?) already address this problem
%by reducing to Boolean programs over an abstraction domain, this
%analysis not a strong argument for our approach. E.g., why not use SLAM or
%change Yogi/Synergy works for EPR?

%Intuitively, an invariant for the while-loop in \lstinline{reverse}
%must assert that the \lstinline{p}-list and the \lstinline{q}-list are
%disjoint (i.e., the two lists do not have a shared common tail).
%Consequently, we expect the loop invariant to look something like the
%following:
%
%\noindent
%\vspace{-1ex}
%\begin{equation}
%  \label{Eq:Reverse:ExpectedInvariant}
%  \forall \alpha : \alpha\neq\tnull \implies
%    \lnot \big(\nrtc{p}{\alpha} \land \nrtc{q}{\alpha}\big).
%\end{equation}
%\vspace{-2ex}

%%\subsubsubsection{Analysis overview.}
Our analysis algorithm is based on 
\emph{property-directed reachability} \cite{VMCAI:Bradley11}.
It starts with the trivial invariant $\true$, which is repeatedly
refined until it becomes inductive.\footnote{An invariant $I$ is
  inductive at the entry to a loop if whenever the code of the loop
  body is executed on an arbitrary state that satisfies both $I$ and the
  loop condition, the result is a state that satisfies $I$.
}
On each iteration, a concrete counterexample to inductiveness is used
to refine the invariant by excluding predicates that are implied by
that counterexample.

\Omit{
When the algorithm terminates, it either 
(i)~Produces an appropriate inductive invariant, or
(ii)~Generates a counterexample, either concrete or abstract
(we explain abstract traces later).}

\begin{comment}
We rely on both the invariants and the transition relation of the loop
being expressed in a recently developed logic for expressing properties
of linked lists, called $\AF$ (\secref{AF}).
$\AF$ is a fragment of so-called ``effectively propositional logic'',
for which SAT solvers are complete.
In our implementation, we use Z3~\cite{TACAS:deMB08} for
(i)~proving that an invariant is inductive,
(ii)~strengthening an invariant when it is non-inductive,
(iii)~producing concrete counterexamples when the postcondition is
violated, and
(iv)~producing a trace that shows that the given predicates cannot be
used to prove the postcondition.

The analysis algorithm is 

guaranteed to terminate. It is also complete in the following sense:
\begin{enumerate}
  \item
    If an inductive invariant that proves the specification is
    expressible as a Boolean combination of the predicates shown in
    \tabref{Pred}, then our algorithm is guaranteed to find
    it.
  \item
    Otherwise, the algorithm either reports a concrete
    counterexample---indicating a bug---or an abstract trace that shows
    that an inductive invariant cannot be expressed as a Boolean
    combination of the predicates shown in \tabref{Pred}.
\end{enumerate}
\end{comment}

When applied to the procedure in \figref{Insert},
our analysis algorithm terminated in about 24 seconds,
and inferred the following 13-clause loop invariant:
{\small
\noindent
\vspace{-1ex}
\begin{equation}
  \label{Eq:Insert:ObtainedInvariant}
  \begin{array}{c@{\hspace{1.0ex}}l c l c l}
          & q\neq e &
    \land & (\nrtc h x \land p=x \limplies \nrtc h q)&
    \land & (p = x \to \nsingl q p) \\
    \land & (\lnot\nsingl e e) &
    \land & (\nrtc q p \limplies \nsingl q p) &
    \land & (\nrtc h p \lor p=\vnull) \\
    \land & (\nsingl e \vnull) &
    \land & (x=\vnull \lor \nrtc p x) &
    \land & (q\neq x \lor p\neq \vnull) \\
    \land & \lnot\nrtc h e &
    \land & (p=\vnull \limplies \nrtc h q) &
    \land & (p = q \lor \nsingl q p) \\
    & &
    \land & (\nrtc h q \land \nrtc h x \limplies \nsingl h q \lor \nrtc q x) &
  \end{array}
\vspace{-1ex}
\end{equation}
}
\noindent
This loop invariant also guarantees that the code is memory safe.  
It is also possible to apply the analysis to infer sufficient conditions
for memory safety using true post-conditions.


%\noindent
%The first three conjuncts are the same as in \equref{Insert:ExpectedInvariant}.
%The next four conjuncts concern the relative position of
%\lstinline{x}, relative to \lstinline{h}, \lstinline{q}, and \lstinline{p}.
%The last four conjuncts concern the relative positions of
%\lstinline{h}, \lstinline{q}, and \lstinline{p}.


%For purposes of human consumption, however, the algorithm did not
%identify a ``pithy'' characterization of the trailing-pointer idiom
%(i.e., characterizing \lstinline{q}'s position relative to
%\lstinline{p}---cf.\ the final conjunct of
%\equref{Insert:ExpectedInvariant}).

\begin{figure}[t]
\def\cw{4mm} \def\dw{5mm} \centering
\begin{tabular}{c@{\hspace{6.0ex}}c}
   \begin{comment}   % For later perhaps
   \begin{tikzpicture}[label distance=3mm, baseline=(h)]
   \matrix[column sep=\dw, row sep=\dw, font=\tiny] {
       \node(nul) {\small null}; &
       \node(v1)[listnode,label={[name=h]left:\small$h$},label={[name=p]above:\small$p$}] {1}; &
       \node(v2)[listnode,label={[name=x]above:\small$x$}] {2}; &
       \node(v3)[listnode] {3}; &
       \node(v4)[listnode] {4}; &
       \node(v5)[listnode] {5}; &
       \node(v6)[listnode] {6}; &
       \node(v7)[listnode] {7}; \\
   };
   \path(nul) [late options={label={[name=e]95:\small$e$},label={[name=q]85:\small$q$}}] ;
   \path[->] (h) edge (v1) (p) edge (v1) (x) edge (v2) (e) edge (nul) (q) edge (nul)
       (v1) edge (v2) (v2) edge (v3) (v3) edge (v4) (v4) edge (v5) (v5) edge (v6) (v6) edge (v7);
   \end{tikzpicture}
   \end{comment}
 \begin{tabular}{l}
     \includegraphics[width=2.4in]{figures/counterexample-before.eps}
 \end{tabular}
  & (a)
 \\
   \begin{comment}   % For later perhaps
   \begin{tikzpicture}[yshift=-5cm, label distance=3mm, baseline=(h)]
   \matrix[column sep=\dw, row sep=\dw, font=\tiny] {
       \node(nul) {\small null}; &
       \node(v1)[listnode,label={[name=h]left:\small$h$},label={[name=q]above:\small$q$}] {1}; &
       \node(v2)[listnode] {2}; &
       \node(v3)[listnode] {3}; &
       \node(v4)[listnode] {4}; &
       \node(v5)[listnode] {5}; &
       \node(v6)[listnode] {6}; &
       \node(v7)[listnode] {7}; \\
   };
   \path(nul) [late options={label={[name=e]90:\small$e$}}] ;
   \path(v2) [late options={label={[name=x]95:\small$x$},label={[name=p]85:\small$p$}}] ;
   \path[->] (h) edge (v1) (q) edge (v1) (x) edge (v2) (p) edge (v2) (e) edge (nul)
       (v1) edge (v2) (v2) edge (v3) (v3) edge (v4) (v4) edge (v5) (v5) edge (v6) (v6) edge (v7);
   \end{tikzpicture}
   \end{comment}
 \begin{tabular}{l}
   \includegraphics[width=2.4in]{figures/counterexample-after.eps}
 \end{tabular}
 & (b)
\end{tabular}
%\vspace{-2ex}
{\small\caption{\label{Fi:BuggyInsertCounterexample}A two-state counterexample
 trace obtained from the algorithm when it is applied to a version of
 \figref{Insert} in which the conjunct $x \neq h$ was added to the
 precondition and $e \neq \tnull$ was removed.
 (a) First state at the loop head; (b) second state at the loop head,
 at which point the loop exits, and a null-dereference violation
 subsequently occurs.
}}
\vspace{-2ex}
\end{figure}

%%\subsubsubsection{Bug finding.}
Our analysis is also capable of finding concrete counterexamples when the procedure violates the specification.
For example, when the conjunct ``$x \neq h$'' is added to the precondition
in \figref{Insert} and ``$e \neq \tnull$'' is removed, the algorithm
returns the counterexample trace shown in \figref{BuggyInsertCounterexample}.
Not surprisingly, $e$ is $\tnull$ in the first state at the loop head
(\figref{Insert}(a)).
The loop body executes once, at which point we reach the loop head
in the state shown in \figref{Insert}(b).
The loop then exits, and there is a null-dereference violation on
\lstinline{e} in the statement \lstinline{e->next = p}.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
