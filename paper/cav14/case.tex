\section{Case Studies}
\label{Se:CaseStudies}

\Omit{
\vspace{-1.0ex}
\subsubsubsection{Procedure \lstinline{reverse}.}
The details of the \lstinline{reverse} example have already been
discussed in \secref{Overview}.
As described in \secref{Overview}, the generated loop invariant for
\lstinline{reverse} (\figref{Reverse}) turned out to be
simpler---i.e., quantifier-free---than the universally quantified
invariant that we had written by hand.
}

\begin{figure}[t]
\begin{lstlisting}[mathescape]
void insert(List e, List h, List x) {
  $\textbf{Requires:~} h \neq \tnull \land \nrtc{h}{x} \land \nrtc{x}{\tnull}  \land e \neq \tnull \land \nsingl{e}{\tnull} \land \neg \nrtc{h}{e}$
  $\textbf{Ensures:~} h \neq \tnull \land \nrtc{h}{e} \land \nsingl{e}{x} \land \nrtc{x}{\tnull}$
  p = h;
  q = null;
  while (p != x && p != null) {
    q = p;
    p = p->n;
  }
  q->n = e;
  e->n = p;
}
\end{lstlisting}
\vspace{-2ex}
{\small\caption{\label{Fi:Insert}%
  A procedure to insert the element pointed to by \lstinline{e} into
  the non-empty, singly-linked list pointed by \lstinline{h}.
  The while-loop uses the trailing-pointer idiom: \lstinline{q} is one
  step behind \lstinline{p}.
}}
%\vspace{-.5ex}
\end{figure}

\vspace{-1.0ex}
\subsubsubsection{Procedure \lstinline{insert}.}
The procedure \lstinline{insert}, shown in \figref{Insert},
inserts a new element pointed to by
\lstinline{e} into the non-empty, singly-linked list pointed to by \lstinline{h}.
\lstinline{Insert} is annotated with a pre-condition and a
post-condition, but not a loop invariant.

Because the while-loop in \lstinline{insert} uses the trailing-pointer
idiom---i.e., \lstinline{q} is one step behind
\lstinline{p}---intuitively, we expect the loop invariant to look
something like the following:\footnote{We used the tool discussed in
  \cite[\S7]{CAV:IBINS13} to check that \equref{Insert:ExpectedInvariant}
  is indeed an invariant.
  Our first attempt---\equref{Insert:ExpectedInvariant} without
  the fourth conjunct---failed, which serves to emphasize
  that a plausible-looking invariant may not actually be an invariant.
}

{\small
\noindent
\vspace{-2ex}
\begin{equation}
\label{Eq:Insert:ExpectedInvariant}
\begin{array}{c@{\hspace{.75ex}}l@{\hspace{1.25ex}}l}
        & e\neq\tnull \land \nsingl{e}{\tnull} \land \lnot\nrtc{h}{e} & \text{// $e$ is an independent element.} \\
  \land & \lnot\nrtc{q}{e}              & \text{// $e$ is not reachable from $q$.} \\
  \land & \nrtc{h}{p} \land \nrtc{p}{x} & \text{// $p$ is between $h$ and $x$.} \\
  \land & (q\neq \tnull \limplies \nsingl{q}{p}) & \text{// Except for the first iteration,} \\
        &                                        & \text{// $q$ is one step behind $p$.}
\end{array}
\vspace{-1ex}
\end{equation}
}

\subsubsubsection{The invariant obtained for \lstinline{insert}.}
The application of our analysis procedure to \figref{Insert}
took about $27$ seconds, and inferred the following $11$-clause loop invariant:

{\small
\noindent
\vspace{-1ex}
\begin{equation}
  \label{Eq:Insert:ObtainedInvariant}
  \begin{array}{c@{\hspace{1.0ex}}l c l c l}
          & e \neq \texttt{null} &
    \land & \nsingl{e}{\texttt{null}} &
    \land & \neg \nrtc{h}{e} \\
    \land & \neg \nrtc{x}{h} &
    \land & (x \neq \texttt{null}) \implies \nrtc{h}{x} &
    \land & \nrtc{h}{x} \implies \nrtc{p}{x} \\
    \land & (\nrtc{h}{x} \land p \neq h) \implies \nrtc{q}{x} &
    \land & (q = x) \implies (p = h) &
    \land & \neg \nrtc{h}{q} \implies (p = h) \\
    \land & (p = \texttt{null}) \implies \nsingl{q}{\texttt{null}} &
    \land & (p \neq \texttt{null}) \implies \nrtc{h}{p}
  \end{array}
\vspace{-1ex}
\end{equation}
}

\noindent
The first three conjuncts are the same as in \equref{Insert:ExpectedInvariant}.
The next four conjuncts concern the relative position of
\lstinline{x}, relative to \lstinline{h}, \lstinline{q}, and \lstinline{p}.
The last four conjuncts concern the relative positions of
\lstinline{h}, \lstinline{q}, and \lstinline{p}.

The algorithm was able to find an invariant sufficient to prove
memory-safety.
For purposes of human consumption, however, the algorithm did not
identify a ``pithy'' characterization of the trailing-pointer idiom
(i.e., characterizing \lstinline{q}'s position relative to
\lstinline{p}---cf.\ the final conjunct of
\equref{Insert:ExpectedInvariant}).

\begin{figure}[t]
\def\cw{4mm}
\def\dw{5mm}
\centering
\begin{tabular}{c@{\hspace{6.0ex}}c}
    \begin{comment}   % For later perhaps
    \begin{tikzpicture}[label distance=3mm, baseline=(h)]
	\matrix[column sep=\dw, row sep=\dw, font=\tiny] {
		\node(nul) {\small null}; &
		\node(v1)[listnode,label={[name=h]left:\small$h$},label={[name=p]above:\small$p$}] {1}; & 
		\node(v2)[listnode,label={[name=x]above:\small$x$}] {2}; & 
		\node(v3)[listnode] {3}; & 
		\node(v4)[listnode] {4}; & 
		\node(v5)[listnode] {5}; &
		\node(v6)[listnode] {6}; &
		\node(v7)[listnode] {7}; \\
	};
	\path(nul) [late options={label={[name=e]95:\small$e$},label={[name=q]85:\small$q$}}] ;
	\path[->] (h) edge (v1) (p) edge (v1) (x) edge (v2) (e) edge (nul) (q) edge (nul)
	    (v1) edge (v2) (v2) edge (v3) (v3) edge (v4) (v4) edge (v5) (v5) edge (v6) (v6) edge (v7);
	\end{tikzpicture}
	\end{comment}
  \begin{tabular}{l}
      \includegraphics[width=2.4in]{figures/counterexample-before.eps}
  \end{tabular}
   & (a)
  \\
    \begin{comment}   % For later perhaps
	\begin{tikzpicture}[yshift=-5cm, label distance=3mm, baseline=(h)]
	\matrix[column sep=\dw, row sep=\dw, font=\tiny] {
		\node(nul) {\small null}; &
		\node(v1)[listnode,label={[name=h]left:\small$h$},label={[name=q]above:\small$q$}] {1}; & 
		\node(v2)[listnode] {2}; & 
		\node(v3)[listnode] {3}; & 
		\node(v4)[listnode] {4}; & 
		\node(v5)[listnode] {5}; &
		\node(v6)[listnode] {6}; &
		\node(v7)[listnode] {7}; \\
	};
	\path(nul) [late options={label={[name=e]90:\small$e$}}] ;
	\path(v2) [late options={label={[name=x]95:\small$x$},label={[name=p]85:\small$p$}}] ;
	\path[->] (h) edge (v1) (q) edge (v1) (x) edge (v2) (p) edge (v2) (e) edge (nul)
	    (v1) edge (v2) (v2) edge (v3) (v3) edge (v4) (v4) edge (v5) (v5) edge (v6) (v6) edge (v7);
	\end{tikzpicture}
	\end{comment}
  \begin{tabular}{l}
    \includegraphics[width=2.4in]{figures/counterexample-after.eps}
  \end{tabular} 
  & (b)
\end{tabular}
%\vspace{-2ex}
{\small\caption{\label{Fi:BuggyInsertCounterexample}A two-state counterexample
  trace obtained from the algorithm when it is applied to a version of
  \figref{Insert} in which the conjunct $x \neq h$ was added to the
  precondition and $e \neq \tnull$ was removed.
  (a) First state at the loop head; (b) second state at the loop head,
  at which point the loop exits, and a null-dereference violation
  subsequently occurs.
}}
\vspace{-2ex}
\end{figure}

\subsubsubsection{The counterexample obtained for a variant of \lstinline{insert}.}
When the conjunct ``$x \neq h$'' is added to the precondition
in \figref{Insert} and ``$e \neq \tnull$'' is removed, the algorithm
returns the counterexample trace shown in \figref{BuggyInsertCounterexample}.
Not surprisingly, $e$ is $\tnull$ in the first state at the loop head
(\figref{Insert}(a)).
The loop body executes once, at which point we reach the loop head
in the state shown in \figref{Insert}(b).
The loop then exits, and there is a null-dereference violation on
\lstinline{e} in the statement \lstinline{e->next = p}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "paper"
%%% End: 
