\section{\PDR}

\subsection{Logic and Transition relations}

\paragraph{Language}
%
\emph{Vocabulary} $\Sigma$ is a fixed finite set of constant symbols $\{c_i\}$
and predicate symbols $\{p_i^{n_i} \}$, where $n_i$ is an arity of $p_i$.
By $\Sigma'$, we denote a vocabulary consisting of primed symbols of $\Sigma$.

As we have no functional symbols \alex{Should we add them?},
\emph{terms} are either constant symbols $c$ or variables $x$.
Should we consider a multisorted language (SDN)?

By \emph{atomic formulas} (\emph{atoms}) over $\Sigma$, we mean formulas of a
kind $t_1 = t_2$ and $p(t_1,\ldots,t_{n})$, where $n$ is an arity of $p$, and
each $t_i$ is a term.
A \emph{literal} is an atom or its negation.
A \emph{cube} is a conjunction of literals.
A \emph{(universal) clause} is a disjunction of literals (with a prefix of
universal quantifiers).
%Constant symbols may represent variables of the program.

\paragraph{Safety transition system}
%
Transition system is given by a triple of (first-order) formulas:
\begin{itemize}
\item $\Init$ is a formula over $\Sigma$ for the set of initial states;
\item Let $X$ be a set of uninterpreted symbols (variables), which may be use
  to represent input parameters of the transition system.
  $\Trans$ is a two-vocabulary formula over $(\Sigma \cup X) \cup \Sigma'$ for the
  state transition relation.
\item $\Bad$ is a formula over $\Sigma$ for the set of \emph{unsafe} states.
\end{itemize}

\subsection{The algorithm}
%
Let $\Bad$ be a formula for the set of bad states.
The algorithm maintains a finite growing sequence of frames $F_0, F_1, \dots$,
where each $F_i$ overapproximates the set of concrete states reachable
from the set of initial states $\Init$ in at most $i$ steps done according
to the transition relation $\Trans$.
The integer $N$ stores an index of the last frame, which is called a \emph{frontier}.

The frames satisfy the following properties
\alex{improve: they are loop invariants for the main loop and for $\Block$}
\begin{enumerate}
  \item $\Init \implies F_0$
  \item $F_i \implies F_{i+1}$, for all $0 \leq i < N$
  \item $F_i \land \Trans \implies F'_{i+1}$, for all $0 \leq i < k$
  \item $F_i \implies \neg \Bad$
\end{enumerate}

\begin{algorithm}[t]
  \begin{algorithmic}
    \State $N \asgn 0$
    \While \True
      \If{$F_i = F_{i+1}$ for some $0 \leq i < N$}
      \Comment the sequence of frames has stabilized
        \State \Return $F_i$
      \EndIf
      \State $(r, s) \asgn \CheckSat(\Bad, F_N)$
      \If{$r = \unsat$}
      \Comment no counterexample for the frontier
        \State $N \asgn N+1$
        \Comment move the frontier forwards
        \State $F_N \asgn \emptyset$
      \Else
        \State $\Block(N, \kDiag(s))$
      \EndIf
    \EndWhile
  \end{algorithmic}
  \caption{PDR, Main loop}
\end{algorithm}

\begin{algorithm}[t]
  \begin{algorithmic}
    \State $(r, \wild) \asgn \CheckSat(\Init, \phi)$
    \If{$r = \sat$}
      \State \Return \text{cex}
      \Comment {check if the cex is spurious by means of BMC}
    \EndIf
    \While \True
    \State $(r, A) \asgn \CheckSat(F_{j-1} \land \Trans, (\phi)')$
    \If{$r = \unsat$}
    \kBreak
    \Else
      \State $\Block(j-1, \kDiag(A))$
    \EndIf
    \EndWhile
    \State $B \asgn \muc(F_{j-1} \land \Trans, (\phi)')$
    \Comment \alex{Is it just an optimization?}
    \For{$i = 0 \til j$}
    \State $F_i \asgn F_i \land \neg B$
    \EndFor
  \end{algorithmic}
  \caption{$\Block(j, \phi)$}
\end{algorithm}

The function $\CheckSat(P, F)$ checks if $P$ and $F$ are satisfiable.
If $P$ and $F$ are not satisfiable, $\CheckSat(P, F)$ returns $(\unsat, \wild)$.
Otherwise, there exists a finite model $s \models P \land F$.
If both $P$ and $F$ are over same vocabulary, $s$ is returned.
Otherwise, if $P$ and $F$ are over $\Sigma \cup \Sigma'$, the \emph{reduct} of $s$ to $\Sigma$
is returned, i.e., interpretations of primed symbols are omitted,
but the universe of $s$ is not changed.

\paragraph{\bf Diagram}

Given a \emph{finite} model $\Mfrak$ for a language $\La$, the \emph{diagram} of $\Mfrak$, denoted by $\kDiag(\Mfrak)$, is constructed as follows.
For every element $e \in M$, we introduce a fresh variable $x_e$.
We form a set $D_{\Mfrak}$ of atomic formulas as follows.
For every $i$, for every tuple $\bar{e}$ of length $n_i$ of elements of $\Mfrak$,
we include in $D$ the atomic formula
$p_i(\bar{x_e})$ if $s \models p(\bar{e})$, and
$\neg p_i(\bar{x_e})$ otherwise.
\alex{This definition actually appears wrong. By the original definition (see below) of a diagram includes all the atomic \emph{sentences}, i.e., atomic formulas without free variables.
This means that we should include \emph{all} atomic formulas that have free variables from the set $X = \{ x_{e_1} \til x_{e_{\card{M}}} \}$ only.
This is crucial to do if we have functional symbols in $\La$.}
We put $\kDiag(\Mfrak) \asgn \exists x_{e_1} \ldots \exists x_{e_{\card{M}}}. \bigwedge D_{\Mfrak}$.
\alex{Note that if $\La$ contains a functional symbol then the set $\kDiag(\Mfrak)$ of atomic formulas over $X$ may be infinite even if $\Mfrak$ is finite.
However, it still might be fine if we work in a sorted logic and if it is not possible to construct infinitely many terms by function applications.
For instance, having two functional symbols $f : S_1 \to S_2$ and $g : S_2 \to S_3$ is ok for any distinct sorts $S_1, S_2, S_3$.
This means that the approach works in the \Vericon's setting where we had functional symbols $\textit{port}$, $\textit{src}$, $\textit{dst}$.
See~\cite{DBLP:conf/asm/NelsonDFK12} for \EPR.
}
\alex{Actually, I now think that the problem of infinity of the set $D_{\Mfrak}$ can be avoided by including formulas describing graphs of functions, for every functional symbol, as I did in the implementation.
I am not sure though.}

\begin{lemma}
\label{lem:diag_embed}
For every finite model $\Bfrak$ such that $\Bfrak \models \kDiag(\Afrak)$,
$\Afrak$ can be isomorphically embedded into $\Bfrak$.
\end{lemma}
%
\begin{proof}
\todo{}
\end{proof}

%The function $\kDiag(s)$ returns a \emph{diagram} of $s$, i.e., a formula
%in $\La$ such that if a model $M$ satisfies $M \models \kDiag(s)$ then
%$s \hookrightarrow M$, i.e., there exists a submodel $s'$ of $M$
%such that $s$ and $s'$ are isomorphic.


\paragraph{\bf Auxiliary statements}
\begin{lemma}
Let $\phi$ be a closed existential first-order formula.
If $s \models \phi$ then for every model $M$ such that
$M \models \kDiag(s)$, $M \models \varphi$ holds.
\end{lemma}
%
\begin{proof}
%It is sufficient to prove the statement for
%$F = \exists \xb. A_1\land\ldots\land A_k$ where each $A_i$ is a literal.
Since $\varphi$ is existential, it is sufficient to show (by Proposition 1 in blue comments below) that $s$ can be isomorphically embedded into $M$.
This is is true by \Cref{lem:diag_embed}.
\end{proof}

\alex{The standard model-theoretic definition of a diagram of the model is
as below (all definitions and propositions are taken from~\cite{chang1990model}).
I actually don't want to use extensions of $\La$ as it may make our explanations less readable.
Let $\Afrak$ be a model for the language $\La$, $A = \abs{\Afrak}$.
We expand $\La$ to a new language $\La_{A} = \La \cup \{ c_a \mid a \in A \}$
by adding a new constant symbol $c_a$ for each $a\in A$.
We expand them $\Afrak$ to the model $\Afrak_A = (\Afrak, c_a)_{a\in A}$
for the language $\La_A$ by interpreting each $c_a$ by $a$.
The \emph{diagram} of model $\Afrak$, denoted $\kDiag(\Afrak)$, is the set of all atomic sentences and negations of atomic sentences of the language $\La_A$ that hold in the model $\Afrak_A$.}

\alex{%
The following propositions are true.
1.~\cite[Exercise 1.3.5]{chang1990model}. Let $f$ be an existential sentence.
If $\Afrak$ is a submodel of $\Bfrak$ and $\Afrak \models f$,
then $\Bfrak \models f$.
2.~\cite[Proposition 2.1.8]{chang1990model}.
    Let $A$ and $B$ be models for $\La$ and let $f : A \to B$.
    Then the following are equivalent:
    \begin{enumerate}
    \item $f$ is an isomorphic embedding of $\Afrak$ into $\Bfrak$
    \item There is an extension $\Cfrak \supset \Afrak$ and an isomorphism
    $g : \Cfrak \cong \Bfrak$ such that $g \supset f$
    \item $(\Bfrak, f a)_{a \in A}$ is a model of the diagram of $\Afrak$.
    \end{enumerate}
%$A$ can be isomorphically embedded into $B$ iff model $B$ has an extension that is a model of the diagram of $A$.
}

%Proving that $\kDiag(s)$ is unreachable, blocks all the models that extend $s$.

%\subsection{Specification of \PDR}
%Below is the specification of \PDR.

\paragraph{\bf In our method}
Each frame except $F_0$ is a set of universally quantified clauses ($\forall$-clause) of the form
\begin{equation*}
\forall \xb.\, A_1 \lor \dots \lor A_k
\end{equation*}
where every $A_i$ is a literal, i.e., an atomic formula or its negation.
\alex{The class of atomic formulas includes formulas of the form
$t_1 \equiv t_2$.
Do we consider them and their negations as literals?}

For all $i > 0$, $F_{i+1}$ is a subset of $F_i$ syntactically.
Fixpoint is reached when $F_i = F_{i+1}$ for some $i \geq 0$.

\paragraph{\bf Transition relation}

We define a formula
$E = \bigwedge_{c} c = c' \land
     \bigwedge_{P\in \Pred} (\forall \xb.\, P \Longleftrightarrow P')$.
Then $\Trans = \wlp(\kCmd, E)$.
\alex{
  What can we say about $\Trans$?
  If universal fragment of FO is in $\La$ and $\wlp$ is logic preserving then
$\Trans$ is in $\La$.}
\alex{Should $\Trans$ include $\kCond$ conjunct?
$\Trans = \kCond \land \wlp(\kCmd, E)$?}

\paragraph{\bf Termination proof}
$C(\La) = \{ [c]_{\equiv} \mid c \text{ is a $\forall$-clause} \}$.
Define
\begin{equation*}
C(I_0) = \{ [c]_{\equiv} \mid I_0 \implies c \text{ and } c \text{ is a $\forall$-clause} \}
\subseteq C(\La)
\end{equation*}
%$C(F_i)$ is the set of clauses in $F_i$, that is
%$F_i = \bigwedge_{c \in C(F_i)} c$

\begin{itemize}
%\item Claim 1. $C(I_0)$ is finite.
\item Claim 1. $C(\La)$ is finite.\alex{Wrong!} (This implies that $C(I_0)$ is finite. \alex{This is wrong too!})
% \newline
% Proof. We can enumerate all combinations of tuples of variables and constants, and thus enumerate all atomic propostions built from predicates of the language $\La$.
%\item Claim 2. For every $c \in C(F_i)$, $[c] \in C(I_0)$.
%\item Claim 2. For every $c \in F_i$, $[c] \in C(\La)$.
\item Claim 3. For every $c_1, c_2 \in C(F_i)$, $c_1 \not\equiv c_2$.
(Not sure yet about it)
\end{itemize}

Do we terminate?

\begin{definition}
For a program $P$ in the form~(\ref{eq:program}), we say that the first-order formula $I$ is a \emph{safety loop-invariant} of $P$ if
\begin{enumerate}
\item $\Pre \implies I$, and
\item $I \land \kCond \land \Trans \land \neg (I)'$ is unsatisfiable
    (equivalently, $I \land \kCond \land \Trans \implies (I)'$ is valid)
    \alex{Should it be $I \land \Trans \land \neg (I)'$?}
\end{enumerate}
\alex{$I$ must be a loop-invariant of the \emph{annotated} program.
That means $I$ must be strong enough to prove correctness of the program.}
\begin{enumerate}[resume]
\item $I \land \neg \kCond \implies \Post$
\end{enumerate}
\end{definition}

\begin{theorem}
Assume that $\CheckSat$ returns minimal models.
1) Given a simple program $P$,
if there exists a universal safety loop-invariant $I$ (over the set of \emph{let's say chosen predicates}) for $P$
then \PDR when applied to $P$ terminates and returns a safety loop-invariant of $P$.
\newline
2) If there is no such universal safety loop invariant, then the algorithm will either diverge or return an abstract counterexample (which might be spurious).
\sharon{This should be wrong: If there exists a u.s.l.i. but not over the set of chosen predicates, the algorithm terminates and returns a spurious cex.}
\end{theorem}
%
\begin{proof}
Assume that there exists a universal loop invariant $I_0$.
W.l.o.g., we can assume $I_0$ in a conjunctive normal form
\begin{equation*}
  I_0 = (\forall \xb.\, A_0) \land \cdots \land (\forall \xb.\,A_k)
\end{equation*}
where each $A_i$ is a clause, i.e., a disjunction of literals.
\alex{We need to define formally our language $\La$}
Since $I_0$ is a loop-invariant, is satisfies
%\begin{compactitem}
\begin{itemize}
\item $\Pre \implies I_0$
\item $I_0 \land \kCond \land \Trans \implies (I_0)'$
\item\label{inv_prop_safe} $I_0 \land \neg \kCond \implies \Post$
\end{itemize}
%\end{compactitem}

For safety property,
\begin{equation*}
\Bad = \neg \kCond \land \neg \Post
\end{equation*}

For memory-safety property,
\begin{align*}
\Bad^{m} & = \neg \ite (\kCond, \wlp(\kCmd, \True), \Post) \\
         & \equiv (\kCond \land \neg\wlp(\kCmd, \True)) \lor
                  (\neg \kCond \land \neg \Post)
\end{align*}

\begin{itemize}
\item Claim 1. $I_0 \land \Bad$ is unsatisfiable.
    It follows from \ref{inv_prop_safe}.
\item Claim 2. $I_0 \land \Bad^{m}$ is unsatisfiable.
    It follows from \ref{inv_prop_safe} and the property
    $I_0 \land \kCond \implies \wlp(\kCmd, \True)$.
    The latter is true since $\wlp$ is monotonic.
\item Claim 3. The previous two claims imply that the counterexample $s_0$
    obtained in $\CheckSat(\Bad, F_N)$ satisfies $s_0 \models \neg I_0$.
\item Claim 4. For every model $s$, if $s \models \neg I_0$ then
    $\kDiag(s) \models \neg I_0$
    (i.e., for every model $\Mfrak$ if $\Mfrak \models \kDiag(s)$ then $\Mfrak \models \neg I_0$).
    This is true since $\neg I_0$ is an existential sentence.
%\item Claim 5. Since $\neg I_0$ is existential, $\kDiag(s) \models I_0$.
\item Claim 5. Let $A_2$ be a model obtained from
$\CheckSat(F_{j-1} \land \Trans, (\phi)')$.
Then $A_2 \models \neg I_0$ whenever $\phi \implies \neg I_0$ holds.
Proof.
We have: since $I_0$ is a loop-invariant,
$I_0 \land \kCond \land \Trans \land (\neg I_0)'$ is unsatisfiable.
Let $\phi \implies \neg I_0$.
There exists a finite model $B$ expanding $A_2$ to primed symbols,
such that
$B \models F_{j-1} \land \Trans \land (\phi)'$.
%$A_2' \models I_0 \land \kCond \land \Trans \land (\neg I_0)'$.
%$A_2' \models (\neg I_0)'$.
Towards contradiction, assume that $A_2 \models I_0$.
Then $B \models I_0 \land \kCond \land \Trans \land (\neg I_0)'$.
Contradiction. \alex{$\kCond$ is lost here. Need to be fixed.}
\item Claim 6. If $I_0$ exists then the algorithm never returns a counterexample $t$.
Such a counterexample is a trace that reaches $s_k\in \Bad$.
Let $s_{k-1}$ be the state in $t$ that precedes $s_k$.
Then we know that the pair
$(s_k,s_{k-1}) \models F_{k-1} \land \Trans \land \Bad$.
By claim~5, we know that $s_{k-1} \models \neg I_0$.
By induction on the length of $t$ \todo{.}
\item Claim 7. Since $\CheckSat$ returns a minimal model,
every model $A_2$ has a number of elements in it limited by
the quantifier depth in $I_0$.
Here we should use an inductive argument.
We assume that $\Trans$ is a \emph{universal} formula in 2-vocabulary.
Let $A_2$ be a model returned by $\CheckSat(F_{j-1} \land \Trans, (\phi)')$.
There exists a model $B$ that is an expansion of $A_2$ such that
$B \models F_{j-1} \land \Trans \land (\phi)'$.

We claim that there exists a model of size bounded by the sum of the number of existential quantifiers and the number of constant symbols in $\La$.

$B$ contains interpretations of primed and unprimed constant symbols
and predicate symbols.
Assume $\phi$ is an existential sentence in $\La$.
Let $M = \abs{A_2}$.
Let $N$ be a set of all the elements of $M$ that make $(\phi)'$ valid (these are the elements for existential prefix and primed constants) as well as the elements interpreting unprimed constant symbols.
(Assume for now that we have no functional symbols.)
Construct a model $\Afrak = A_2 \restriction N$.


\item Claim 8. For the bound $d$, the number of non-equivalent universal
clauses over a fixed set $\{x_1 \til x_d\}$ of variables in the language
$\La$ is finite, say $U_d$.
Every new clause $\psi$ generated in the frame $F^t_{j-1}$ for $t$-th call to $\Block$ is not a consequence of $F^t_{j-1}$.
1) Every ``big'' iteration is finite (immediate from the previous sentence).
2) The number of ``big'' iterations is bounded by $U_d + 1$ since we cannot have a sequence of $U_d$ set of universal clauses
$F_0 \supset F_1 \supset \ldots \supset F_{U_d}$.

%Indeed, if $A_2$ is a model of $\neg I_0$ (Claim~5) then there exists a subformula
\end{itemize}

%For every clause $\forall xb.\,A_i$, $i\in [1,k]$,

We denote $F_i^t$ a frame after iteration $t$  ($t$-th call to $\Block$).
For every $t$, all $F_i^t$ satisfy invariants 1--4.
\alex{Except the invariant 4 that maybe not true for the last frame.}
\alex{We should rewrite the algorithm such that it maintains a queue of goals
to block.
If the counterexample $\CheckSat(\Bad, F_N)$ exists, we put it into the queue.
If $\CheckSat(F_{j-1} \land \Trans, (A)')$ is not blocked, we put the obtained
counterexample into the queue.
}

Take some $(j, A)$ from the queue.
We try to block it.
For that, we call $\CheckSat(F_{j-1} \land \Trans, (A)')$.
\begin{itemize}
  \item 
If the result is $\sat$, we obtain a new ``abstract'' $A_2$, and put
$(j-1, A_2)$ into the queue.
The invariants 1--4 for $F_i^t$ are obviously preserved.
Increase $t$.
\item
If the result is $\unsat$, the abstract model $A$ is provably unreachable.
\end{itemize}

\ldots



\qed
\end{proof}
